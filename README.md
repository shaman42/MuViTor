# SimpleVideoEditor
A simple video editor written in java, tailored for creating music videos.

For a compiled version, see the release page.

Features:
 - Import any video format that ffmpeg supports
 - Import any audio as .wav or .ogg files
 - Rendering backend handled by the jMonkeyEngine resulting in realtime previews
 - Precise placement of objects:
   - Pixel-wise positioning
   - Scaling around a user-specified anchor
   - Cropping
 - Layout manager:
   - split the screen horizontally and vertically using custom split planes, infinite recurseable
   - place videos in the layout, zoom and move them around
   - Specify the border size to the neighboring videos
   - No need to calculate the positions and sizes manually anymore!
 - User-friendly timeline:
   - Move objects around in time, specify the start frame, skipped frames at the beginning, and the duration
   - Display audio waveform or frames
   - Split objects at the current time
   - Disable objects
   - Three channel modes: off, on, and solo that act on all objects in that channel
 - Full undo-redo support
 
Screenshot:
This screenshot shows how I used this program to create the video for the [Michael Jackson Medley](http://www.sebastian-weiss.eu/music/sax-ensemble/michael-jackson-medley/)

![Editing the Michael Jackson Medley](http://sebastian-weiss.eu/wp-content/uploads/2017/04/VideoEditor_v2.0.png)
 
It requires at least Java 8 and ffmpeg (the binary) to run.
Libraries used:
 - Apache Commons (https://commons.apache.org/)
 - SwingX
 - Simple XML (http://simple.sourceforge.net/)
 - jMonkeyEngine3 (http://jmonkeyengine.org/)
 - Humble-Video (https://github.com/artclarke/humble-video)
All libraries are bundled with this project.

In detail, when a video is imported, ffmpeg is used to extract the audio file. Then humble-video is used to extract thumbnails of each frame for a realtime preview.
Limitations: because it is very difficult to record audio inside jME, only one active audio source is supported. It is directly copied into the final movie. For music videos, this is enough.

For more information, see http://www.sebastian-weiss.eu/programming/simple-video-editor/.
