/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.layout;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.MainPanel;
import org.shaman.muvitor.Project;
import org.shaman.muvitor.SerializationUtils;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineChannel;
import org.simpleframework.xml.Serializer;

/**
 *
 * @author Sebastian
 */
public class LayoutManagerPanel extends javax.swing.JPanel implements PropertyChangeListener {
	private static final Logger LOG = Logger.getLogger(LayoutManagerPanel.class.getName());
	private static final String PRESET_FOLDER = "presets/layouts";
	
	private Project project;
	private final LayoutPanel panel;
	private UndoableEditSupport undoSupport;
	private Player player;
	
	private Layout currentLayout;
	private LayoutNode selectedNode;

	/**
	 * Creates new form LayoutManagerFrame
	 */
	public LayoutManagerPanel() {
		panel = new LayoutPanel();
		initComponents();
		panel.addPropertyChangeListener(this);
	}
	
	public void setProject(Project project) {
		this.project = project;
		project.addPropertyChangeListener(this);
		checkTime();
	}
	public void setPlayer(Player player) {
		this.player = player;
		panel.initPlayer(player);
	}
	
	public void setMainPanel(MainPanel mainPanel) {
		panel.initMainPanel(mainPanel);
	}
	
	public void initMenu(JMenuBar menu, JToolBar toolbar) {
		
	}
	
	public void setUndoSupport(UndoableEditSupport undoSupport) {
		this.undoSupport = undoSupport;
	}
	
	private boolean isPlaying() {
		return player!=null && player.isPlaying();
	}
	
	/**
	 * Checks if there is a layout at the current time
	 */
	private void checkTime() {
		if (currentLayout != null) {
			currentLayout.removePropertyChangeListener(this);
		}
		
		FrameTime time = project.getTime();
		currentLayout = null;
		FrameTime[] currentInterval = null;
		
		for (Iterator<Layout> it = project.getLayouts().iterator(); it.hasNext();) {
			Layout layout = it.next();
			layout.fireNodeChanged(null);
			FrameTime[] interval = layout.getInfluencingTime();
			if (interval[0] == null) {
				//empty layout, delete
				if (currentLayout != layout) {
					it.remove();
					LOG.info("deleted empty layout");
					continue;
				}
			}
			if (time.compareTo(interval[0])>=0 && time.compareTo(interval[1])<0) {
				currentLayout = layout;
				currentInterval = interval;
				break;
			}
		}
		
		checkButtons();
	}
	private void checkButtons() {
		if (currentLayout == null) {
			FrameTime time = project.getTime();
			infoTextLabel.setText("No layout found at the current time ("+time+")");
			createLayoutButton.setEnabled(true);
			loadPresetButton.setEnabled(true);
			deleteLayoutButton.setEnabled(false);
			savePresetButton.setEnabled(false);
			panel.setLayout(project, null);
			innerBorderSpinner.setEnabled(false);
			outerBorderSpinner.setEnabled(false);
		} else {
			FrameTime[] interval = currentLayout.getInfluencingTime();
			infoTextLabel.setText("Layout ranges from "+interval[0]+" to "+interval[1]);
			createLayoutButton.setEnabled(false);
			loadPresetButton.setEnabled(false);
			deleteLayoutButton.setEnabled(true);
			savePresetButton.setEnabled(true);
			panel.setLayout(project, currentLayout);
			innerBorderSpinner.setEnabled(true);
			outerBorderSpinner.setEnabled(true);
			innerBorderSpinner.setValue(currentLayout.getInnerBorder());
			outerBorderSpinner.setValue(currentLayout.getOuterBorder());
			currentLayout.addPropertyChangeListener(this);
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (pce.getSource() == panel 
				&& pce.getPropertyName().equals(LayoutPanel.PROP_SELECTION_CHANGED)) {
			selectedNode = (LayoutNode) pce.getNewValue();
			checkSelectedNode();
		}
		else if (pce.getSource() == currentLayout) {
			checkSelectedNode();
		}
		else if (pce.getSource() == project) {
			switch (pce.getPropertyName()) {
				case Project.PROP_TIME:
					if (!isPlaying()) {
						checkTime();
					}
					break;
				case Project.PROP_TIMELINE_CHANNELS_CHANGED:
					//Check all layouts if all timeline objects still exist
					for (Layout l : project.getLayouts()) {
						l.checkLinkedObjects();
					}
			}
		}
	}
	
	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        loadPresetButton = new javax.swing.JButton();
        savePresetButton = new javax.swing.JButton();
        splitVerticallyButton = new javax.swing.JButton();
        splitHorizontallyButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        splitPointSpinner = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        innerBorderSpinner = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        outerBorderSpinner = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        zoomSpinner = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        translationXSpinner = new javax.swing.JSpinner();
        translationYSpinner = new javax.swing.JSpinner();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        mergeButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        infoTextLabel = new javax.swing.JLabel();
        createLayoutButton = new javax.swing.JButton();
        deleteLayoutButton = new javax.swing.JButton();
        selectParentButton = new javax.swing.JButton();

        setMinimumSize(new java.awt.Dimension(0, 0));

        loadPresetButton.setText("load preset");
        loadPresetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadPresetEvent(evt);
            }
        });

        savePresetButton.setText("save preset");
        savePresetButton.setEnabled(false);
        savePresetButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savePresetEvent(evt);
            }
        });

        splitVerticallyButton.setText("split vertically");
        splitVerticallyButton.setEnabled(false);
        splitVerticallyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                splitVerticallyButtonEvent(evt);
            }
        });

        splitHorizontallyButton.setText("split horizontally");
        splitHorizontallyButton.setEnabled(false);
        splitHorizontallyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                splitHorizontallyButtonEvent(evt);
            }
        });

        jLabel1.setLabelFor(splitPointSpinner);
        jLabel1.setText("split point:");

        splitPointSpinner.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.5f), Float.valueOf(0.0f), Float.valueOf(1.0f), Float.valueOf(0.01f)));
        splitPointSpinner.setEnabled(false);
        splitPointSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                splitPointChangedEvent(evt);
            }
        });

        jLabel2.setLabelFor(innerBorderSpinner);
        jLabel2.setText("inner border:");
        jLabel2.setToolTipText("the size of the inner border in pixels");

        innerBorderSpinner.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        innerBorderSpinner.setToolTipText("the size of the inner border in pixels");
        innerBorderSpinner.setEnabled(false);
        innerBorderSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                innerBorderChangedEvent(evt);
            }
        });

        jLabel3.setLabelFor(innerBorderSpinner);
        jLabel3.setText("outer border:");
        jLabel3.setToolTipText("the size of the outer border in pixels");

        outerBorderSpinner.setModel(new javax.swing.SpinnerNumberModel(10, 0, null, 1));
        outerBorderSpinner.setToolTipText("the size of the outer border in pixels");
        outerBorderSpinner.setEnabled(false);
        outerBorderSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                outerBorderChangedEvent(evt);
            }
        });

        jLabel4.setLabelFor(zoomSpinner);
        jLabel4.setText("zoom:");
        jLabel4.setToolTipText("the zoom of the selected timeline object");

        zoomSpinner.setModel(new javax.swing.SpinnerNumberModel(1.0f, 1.0f, null, 0.01f));
        zoomSpinner.setToolTipText("the zoom of the selected timeline object");
        zoomSpinner.setEnabled(false);
        zoomSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                zoomChangedEvent(evt);
            }
        });

        jLabel5.setText("translation:");
        jLabel5.setToolTipText("zoom anchor");

        translationXSpinner.setModel(new javax.swing.SpinnerNumberModel());
        translationXSpinner.setToolTipText("zoom anchor x");
        translationXSpinner.setEnabled(false);
        translationXSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                translationXChangedEvent(evt);
            }
        });

        translationYSpinner.setModel(new javax.swing.SpinnerNumberModel());
        translationYSpinner.setToolTipText("zoom anchor y");
        translationYSpinner.setEnabled(false);
        translationYSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                translationYChangedEvent(evt);
            }
        });

        mergeButton.setText("merge");
        mergeButton.setToolTipText("Turns the selected inner layout node in a leaf node");
        mergeButton.setEnabled(false);
        mergeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mergeButtonEvent(evt);
            }
        });

        infoTextLabel.setText("no project loaded");

        createLayoutButton.setText("create new layout");
        createLayoutButton.setToolTipText("Creates a new layout at the current time");
        createLayoutButton.setEnabled(false);
        createLayoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newLayoutButtonEvent(evt);
            }
        });

        deleteLayoutButton.setText("delete");
        deleteLayoutButton.setToolTipText("deletes the current layout");
        deleteLayoutButton.setEnabled(false);
        deleteLayoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteLayoutEvent(evt);
            }
        });

        selectParentButton.setText("select parent");
        selectParentButton.setEnabled(false);
        selectParentButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectParentButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(infoTextLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jSeparator5)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(zoomSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(innerBorderSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(outerBorderSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(splitVerticallyButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(splitHorizontallyButton))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(mergeButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(selectParentButton))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel1)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(splitPointSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jSeparator1)
                                .addComponent(jSeparator4)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(loadPresetButton)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(savePresetButton))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(translationXSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(translationYSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(createLayoutButton, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(deleteLayoutButton)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(infoTextLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loadPresetButton)
                    .addComponent(savePresetButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(createLayoutButton)
                    .addComponent(deleteLayoutButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(innerBorderSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(outerBorderSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(splitVerticallyButton)
                    .addComponent(splitHorizontallyButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mergeButton)
                    .addComponent(selectParentButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(splitPointSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(zoomSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(translationXSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(translationYSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void splitPointChangedEvent(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_splitPointChangedEvent
        selectedNode.setSplitPoint((float) splitPointSpinner.getValue());
    }//GEN-LAST:event_splitPointChangedEvent

    private void innerBorderChangedEvent(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_innerBorderChangedEvent
        currentLayout.setInnerBorder((int) innerBorderSpinner.getValue());
    }//GEN-LAST:event_innerBorderChangedEvent

    private void outerBorderChangedEvent(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_outerBorderChangedEvent
        currentLayout.setOuterBorder((int) outerBorderSpinner.getValue());
    }//GEN-LAST:event_outerBorderChangedEvent

    private void zoomChangedEvent(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_zoomChangedEvent
		selectedNode.setZoomFactor((float) zoomSpinner.getValue());
    }//GEN-LAST:event_zoomChangedEvent

    private void translationXChangedEvent(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_translationXChangedEvent
		selectedNode.setTranslationX((int) translationXSpinner.getValue());
    }//GEN-LAST:event_translationXChangedEvent

    private void translationYChangedEvent(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_translationYChangedEvent
        selectedNode.setTranslationY((int) translationYSpinner.getValue());
    }//GEN-LAST:event_translationYChangedEvent

    private void splitVerticallyButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_splitVerticallyButtonEvent
        selectedNode.split(LayoutNode.SplitDirection.VERTICALLY);
		checkSelectedNode();
    }//GEN-LAST:event_splitVerticallyButtonEvent

    private void splitHorizontallyButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_splitHorizontallyButtonEvent
        selectedNode.split(LayoutNode.SplitDirection.HORIZONTALLY);
		checkSelectedNode();
    }//GEN-LAST:event_splitHorizontallyButtonEvent

    private void mergeButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mergeButtonEvent
        selectedNode.merge();
		checkSelectedNode();
    }//GEN-LAST:event_mergeButtonEvent

	private ImageTimelineObject findActiveObject() {
		FrameTime time = project.getTime();
		ImageTimelineObject obj = null;
		findActiveObject:
		for (int i=project.getTimelineChannels().size()-1; i>=0; --i) {
			TimelineChannel c = project.getTimelineChannels().get(i);
			if (!c.isVisible()) continue;
			for (ResourceTimelineObject robj : c.getObjects()) {
				if (!robj.isEnabled()) continue;
				if (!(robj instanceof ImageTimelineObject)) continue;
				if (robj.getOffset().add(robj.getStart()).compareTo(time) >= 0) continue;
				if (robj.getOffset().add(robj.getDuration()).compareTo(time) < 0) continue;
				obj = (ImageTimelineObject) robj;
				break findActiveObject;
			}
		}
		return obj;
	}
	
    private void newLayoutButtonEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newLayoutButtonEvent
		//find active image timeline object
		ImageTimelineObject obj = findActiveObject();
		
		if (obj == null) {
			JOptionPane.showMessageDialog(this, "No image timeline object is active at the current time", "Unable to create layout", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		//TODO: this will trigger many undo events
		FrameTime time = project.getTime();
		LOG.log(Level.INFO, "create layout at {0}", time);
		Layout layout = new Layout(project, undoSupport);
		layout.setInnerBorder((int) innerBorderSpinner.getValue());
		layout.setOuterBorder((int) outerBorderSpinner.getValue());
		layout.getRoot().setLeafObject(obj);
		project.getLayouts().add(layout);
		
		//trigger check again, the new layout should now be found
		checkTime();
		panel.disabledAllAvailableObjects();
		project.fireLayoutsChanged();
    }//GEN-LAST:event_newLayoutButtonEvent

	private File getPresetFolder() {
		File projectFolder = project.getFolder();
		File folder = new File(projectFolder, PRESET_FOLDER);
		if (folder.isDirectory()) {
			return folder;
		} else if (!folder.exists()) {
			folder.mkdirs();
			return folder;
		} else {
			throw new IllegalStateException("file "+folder+" should be a folder");
		}
	}
	
    private void loadPresetEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadPresetEvent
        File folder = getPresetFolder();
		JFileChooser fc = new JFileChooser(folder);
		FileNameExtensionFilter ff = new FileNameExtensionFilter("Presets", "xml");
		fc.setFileFilter(ff);
		fc.addChoosableFileFilter(ff);
		fc.setDialogTitle("Load preset");
		int ret = fc.showOpenDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			File file = fc.getSelectedFile();
			LOG.log(Level.INFO, "load preset {0}", file);
			Serializer serializer = SerializationUtils.createSerializer();
			try {
				Layout l = serializer.read(Layout.class, file);
				l.init(project, undoSupport);
				currentLayout = l;
				project.getLayouts().add(l);
				checkButtons();
				project.fireLayoutsChanged();
			} catch (Exception ex) {
				LOG.log(Level.SEVERE, "unable to load layout", ex);
			}
		}
    }//GEN-LAST:event_loadPresetEvent

    private void savePresetEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savePresetEvent
        File folder = getPresetFolder();
		Layout clone = currentLayout.cloneForSaving();
		JFileChooser fc = new JFileChooser(folder);
		FileNameExtensionFilter ff = new FileNameExtensionFilter("Presets", "xml");
		fc.setFileFilter(ff);
		fc.addChoosableFileFilter(ff);
		fc.setDialogTitle("Save layout as preset");
		int ret = fc.showSaveDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			Serializer serializer = SerializationUtils.createSerializer();
			File target = fc.getSelectedFile();
			if (!target.getName().endsWith(".xml")) {
				target = new File(target.getAbsolutePath()+".xml");
			}
			try {
				serializer.write(clone, target);
				LOG.info("layout saved");
			} catch (Exception ex) {
				LOG.log(Level.SEVERE, "unable to save layout", ex);
			}
		}
    }//GEN-LAST:event_savePresetEvent

    private void deleteLayoutEvent(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteLayoutEvent
        assert (currentLayout != null);
		LOG.log(Level.INFO, "delete {0}", currentLayout);
		Layout l = currentLayout;
		project.getLayouts().remove(l);
		checkTime();
		project.fireLayoutsChanged();
		if (undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit(){
				@Override
				public void undo() throws CannotUndoException {
					project.getLayouts().add(l);
					checkTime();
					project.fireLayoutsChanged();
				}

				@Override
				public void redo() throws CannotRedoException {
					project.getLayouts().remove(l);
					checkTime();
					project.fireLayoutsChanged();
				}
				
			});
		}
    }//GEN-LAST:event_deleteLayoutEvent

    private void selectParentButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectParentButtonActionPerformed
        if (selectedNode.getParent() != null)
            panel.setSelectedNode(selectedNode.getParent());
        
    }//GEN-LAST:event_selectParentButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton createLayoutButton;
    private javax.swing.JButton deleteLayoutButton;
    private javax.swing.JLabel infoTextLabel;
    private javax.swing.JSpinner innerBorderSpinner;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JButton loadPresetButton;
    private javax.swing.JButton mergeButton;
    private javax.swing.JSpinner outerBorderSpinner;
    private javax.swing.JButton savePresetButton;
    private javax.swing.JButton selectParentButton;
    private javax.swing.JButton splitHorizontallyButton;
    private javax.swing.JSpinner splitPointSpinner;
    private javax.swing.JButton splitVerticallyButton;
    private javax.swing.JSpinner translationXSpinner;
    private javax.swing.JSpinner translationYSpinner;
    private javax.swing.JSpinner zoomSpinner;
    // End of variables declaration//GEN-END:variables

	private void checkSelectedNode() {
		//update spinners
		boolean inner = (selectedNode != null) && !selectedNode.isLeaf();
		boolean leaf = (selectedNode != null) && selectedNode.isLeaf();
		boolean leafRes = leaf && selectedNode.getLeafObject()!=null;
                boolean hasParent = (selectedNode != null) && selectedNode.getParent()!=null;

		splitHorizontallyButton.setEnabled(leaf);
		splitVerticallyButton.setEnabled(leaf);
		splitPointSpinner.setEnabled(inner);
		mergeButton.setEnabled(inner);
                selectParentButton.setEnabled(hasParent);
		zoomSpinner.setEnabled(leafRes);
		translationXSpinner.setEnabled(leafRes);
		translationYSpinner.setEnabled(leafRes);

		if (inner) {
			splitPointSpinner.setValue(selectedNode.getSplitPoint());
		}
		if (leafRes) {
			zoomSpinner.setValue(selectedNode.getZoomFactor());
			translationXSpinner.setValue(selectedNode.getTranslationX());
			translationYSpinner.setValue(selectedNode.getTranslationY());
		}
	}

}
