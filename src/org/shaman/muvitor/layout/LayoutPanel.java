/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.layout;

import com.jme3.texture.Texture2D;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import jme3tools.converters.ImageToAwt;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.MainPanel;
import org.shaman.muvitor.Project;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.resources.Resource;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineChannel;

/**
 *
 * @author Sebastian
 */
public class LayoutPanel implements 
		MouseListener, MouseMotionListener, MouseWheelListener,
		PropertyChangeListener,	Consumer<Graphics2D> {

	private static final Logger LOG = Logger.getLogger(LayoutPanel.class.getName());
	private static final Color HOVERED_COLOR = Color.BLUE;
	private static final Color SELECTED_COLOR = Color.GREEN;
	private static final int HOVERED_INTEND = 0;
	private static final int SELECTED_INTENT = 3;
	private static final int PICK_EXTRA_DISTANCE = 5;

	public static final String PROP_SELECTION_CHANGED = "selection";
	private transient PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	private JPanel contentPanel;
	private Player player;
	
	private Project project;
	private Layout layout;
	
	private TexturePaint fillPattern;
	private ArrayList<ImageTimelineObject> availableObjects;
	
	//selections
	/**
	 * The node under the mouse.
	 * If it is a leaf, then the mouse is in the inside.
	 * If it is an inner node, then the mouse is over the split point
	 */
	private LayoutNode hoveredNode;
	private LayoutNode selectedNode;
	//for dragging
	private Point oldMousePos;
	private int oldTranslationX;
	private int oldTranslationY;
	private float oldZoomFactor;
	private boolean mousePressed;

	public LayoutPanel() {
		try {
			BufferedImage fillTexture = ImageIO.read(LayoutPanel.class.getClassLoader().getResource("org/shaman/muvitor/icons/empty_pattern.png"));
			fillPattern = new TexturePaint(fillTexture, new Rectangle(fillTexture.getWidth(), fillTexture.getHeight()));
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
		
		availableObjects = new ArrayList<>();
	}
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		propertyChangeSupport.addPropertyChangeListener(l);
	}
	
	public void initMainPanel(MainPanel mainPanel) {
		contentPanel = mainPanel.getContentPanel();
		mainPanel.addExtraDrawCommand(this);
		contentPanel.addMouseListener(this);
		contentPanel.addMouseMotionListener(this);
		contentPanel.addMouseWheelListener(this);
	}
	public void initPlayer(Player player) {
		this.player = player;
	}
	
	public void setLayout(Project project, Layout layout) {
		if (this.layout != null) {
			this.layout.removePropertyChangeListener(this);
		}
		this.project = project;
		this.layout = layout;
		this.selectedNode = null;
		this.hoveredNode = null;
		
		if (layout == null) {
			return;
		}
		
		this.layout.addPropertyChangeListener(this);
		
		//collect image providers that are active at the current time
		availableObjects.clear();
		FrameTime time = project.getTime();
		for (TimelineChannel c : project.getTimelineChannels()) {
			for (ResourceTimelineObject obj : c.getObjects()) {
				if (!(obj instanceof ImageTimelineObject)) {
					continue;
				}
				if (obj.getOffset().toFrames() + obj.getStart().toFrames() <= time.toFrames()
						&& obj.getOffset().toFrames() + obj.getDuration().toFrames() > time.toFrames()) {
					availableObjects.add((ImageTimelineObject) obj);
					break;
				}
			}
		}
		LOG.log(Level.INFO, "available timeline objects: {0}", availableObjects);
		
		//trigger layout
		layout();
	}

	private boolean isEnabled() {
		if (layout == null) {
			return false;
		}
		if (player == null || player.isPlaying()) {
			return false;
		}
		return true;
	}
	
	private float transformScale;
	private int transformOffsetX;
	private int transformOffsetY;
	
	private Rectangle convertToScreenRect(Rectangle rect) {
		Rectangle screenRect = new Rectangle();
		screenRect.x = (int) ((rect.x * transformScale) + transformOffsetX);
		screenRect.y = (int) (((project.getHeight()-rect.y-rect.height) * transformScale) + transformOffsetY);
		screenRect.width = (int) (rect.width * transformScale);
		screenRect.height = (int) (rect.height * transformScale);
		return screenRect;
	}
	
	@Override
	public void accept(Graphics2D gOld) {
		if (!isEnabled()) {
			return;
		}
		
		Graphics2D graphics = (Graphics2D) gOld.create();
		
		//transform graphics to view the whole screen
		int sw = contentPanel.getWidth();
		int sh = contentPanel.getHeight();
		int pw = project.getWidth();
		int ph = project.getHeight();
		transformScale = Math.min(sw / (float) pw, sh / (float) ph);
		pw *= transformScale;
		ph *= transformScale;
		transformOffsetX = (sw - pw) / 2;
		transformOffsetY = (sh - ph) / 2;
		
		//paint tree
		paintNode(layout.getRoot(), graphics);
		
		if (selectedNode != null) {
			//show selected marker
			graphics.setColor(SELECTED_COLOR);
			Rectangle screenRect = convertToScreenRect(selectedNode.layoutedRect);
			int x = screenRect.x;
			int y = screenRect.y;
			int width = screenRect.width;
			int height = screenRect.height;
			graphics.drawRect(x + SELECTED_INTENT, y + SELECTED_INTENT, width - 2*SELECTED_INTENT, height - 2*SELECTED_INTENT);
			if (!selectedNode.isLeaf()) {
				float splitPoint = selectedNode.getSplitPoint();
				LayoutNode.SplitDirection dir = selectedNode.getSplitDirection();
				if (dir == LayoutNode.SplitDirection.HORIZONTALLY) {
					int h = (int) (height * (1-splitPoint));
					graphics.fillRect(x + SELECTED_INTENT, y + h - 2, width - 2*SELECTED_INTENT, 6);
				} else {
					int w = (int) (width * splitPoint);
					graphics.fillRect(x + w - 2, y + SELECTED_INTENT, 6, height - 2*SELECTED_INTENT);
				}
			}
		}
		
		if (hoveredNode != null) {
			//show hovered marker
			graphics.setColor(HOVERED_COLOR);
			Rectangle screenRect = convertToScreenRect(hoveredNode.layoutedRect);
			int x = screenRect.x;
			int y = screenRect.y;
			int width = screenRect.width;
			int height = screenRect.height;
			graphics.drawRect(x + HOVERED_INTEND, y + HOVERED_INTEND, width - 2*HOVERED_INTEND, height - 2*HOVERED_INTEND);
			if (!hoveredNode.isLeaf()) {
				float splitPoint = hoveredNode.getSplitPoint();
				LayoutNode.SplitDirection dir = hoveredNode.getSplitDirection();
				if (dir == LayoutNode.SplitDirection.HORIZONTALLY) {
					int h = (int) (height * (1-splitPoint));
					graphics.fillRect(x + HOVERED_INTEND, y + h, width - 2*HOVERED_INTEND, 2);
				} else {
					int w = (int) (width * splitPoint);
					graphics.fillRect(x + w, y + HOVERED_INTEND, 2, height - 2*HOVERED_INTEND);
				}
			}
		}
		
		graphics.dispose();
	}
	private int BORDER_FLAG_LEFT = 1;
	private int BORDER_FLAG_RIGHT = 2;
	private int BORDER_FLAG_TOP = 4;
	private int BORDER_FLAG_BOTTOM = 8;
	private void paintNode(LayoutNode node, Graphics2D g) {
		
		if (node.isLeaf()) {
			//paint content
			ImageTimelineObject obj = node.getLeafObject();
			if (obj == null) {
				//paint empty pattern
				g.setPaint(fillPattern);
				Rectangle rect = convertToScreenRect(node.layoutedRect);
				g.fillRect(rect.x, rect.y, rect.width, rect.height);
				g.setPaint(null);
			} else {
				//paint content
				//handled by jme3
			}
		} else {
			//paint children
			for (LayoutNode c : node.getChildren()) {
				paintNode(c, g);
			}
		}
	}
	
	/**
	 * Disables all available objects.
	 * This is called by {@link LayoutManagerPanel} after a new layout was
	 * created to initialize that layout
	 */
	public void disabledAllAvailableObjects() {
		for (ImageTimelineObject obj : availableObjects) {
			obj.setEnabledNoUndo(false);
		}
	}
	
	/**
	 * The main method: performs the layouting.
	 * This modifies all linked timeline objects according to the layout tree
	 */
	public void layout() {
		
		//perform the layouting
		layoutNode(layout.getRoot(), layout.getOuterBorder(), layout.getOuterBorder(),
				project.getWidth()-2*layout.getOuterBorder(), project.getHeight()-2*layout.getOuterBorder(), 
				0);
	}
	private void layoutNode(LayoutNode node, int x, int y, int width, int height, int borderFlags) {
		//set bounds	
		if (node.isLeaf()) {
			//paint content
			if ((borderFlags & BORDER_FLAG_LEFT) > 0) {
				x += layout.getInnerBorder();
				width -= layout.getInnerBorder();
			}
			if ((borderFlags & BORDER_FLAG_TOP) > 0) {
				y += layout.getInnerBorder();
				height -= layout.getInnerBorder();
			}
			if ((borderFlags & BORDER_FLAG_RIGHT) > 0) {
				width -= layout.getInnerBorder();
			}
			if ((borderFlags & BORDER_FLAG_BOTTOM) > 0) {
				height -= layout.getInnerBorder();
			}
			
			int tx = node.getTranslationX();
			int ty = node.getTranslationY();
			float zoom = node.getZoomFactor();
			
			ImageTimelineObject img = node.getLeafObject();
			if (img != null) {
				//set transformation so that the image fills the rect
				//adjust cropping to cut of the overlapping parts
				float aspect = width / (float) height;
				if (aspect > img.getAspect()) {
					//wider than the image -> increase height
					img.setXNoUndo((int) (x + tx + (img.getWidth()*(1-zoom)/2)));
					img.setWidthNoUndo((int) (width * zoom));
					img.setHeightNoUndo((int) (width / img.getAspect() * zoom));
					img.setYNoUndo((int) (y + ty - (img.getHeight()-height)/2 + (img.getHeight()*(1-zoom)/2)));
				} else {
					//higher than the image -> increase width
					img.setYNoUndo((int) (y + ty + (img.getHeight()*(1-zoom)/2)));
					img.setHeightNoUndo((int) (height * zoom));
					img.setWidthNoUndo((int) (height * img.getAspect() * zoom));
					img.setXNoUndo((int) (x + tx - (img.getWidth()-width)/2 + (img.getWidth()*(1-zoom)/2)));
				}
				img.setCropLeftNoUndo((x-img.getX())/(float)img.getWidth());
				img.setCropRightNoUndo(1-(x+width-img.getX())/(float)img.getWidth());
				img.setCropBottomNoUndo((y-img.getY())/(float)img.getHeight());
				img.setCropTopNoUndo(1-(y+height-img.getY())/(float)img.getHeight());
				
//				System.out.println("Layout "+img.getResource().getName());
//				System.out.println("  bounds: x="+x+", y="+y+", width="+width+", height="+height);
//				System.out.println("  scene: x="+img.getX()+", y="+img.getY()+", width="+img.getWidth()+", height="+img.getHeight());
//				System.out.println("  cropping: left="+img.getCropLeft()+", right="+img.getCropRight()+", top="+img.getCropTop()+", bottom="+img.getCropBottom());
				
				//set zoom factor
//				img.setScalingFactorNoUndo(node.getZoomFactor());
				img.setScalingFactorNoUndo(1);
				//enable them
				img.setEnabledNoUndo(true);
			}
			
		} else {
			//layout children
			float splitPoint = node.getSplitPoint();
			LayoutNode.SplitDirection dir = node.getSplitDirection();
			if (dir == LayoutNode.SplitDirection.HORIZONTALLY) {
				int h = (int) (height * splitPoint);
				layoutNode(node.getChildren()[0], x, y, width, h, borderFlags | BORDER_FLAG_BOTTOM);
				layoutNode(node.getChildren()[1], x, y + h, width, height - h, borderFlags | BORDER_FLAG_TOP);
			} else {
				int w = (int) (width * splitPoint);
				layoutNode(node.getChildren()[0], x, y, w, height, borderFlags | BORDER_FLAG_RIGHT);
				layoutNode(node.getChildren()[1], x + w, y, width - w, height, borderFlags | BORDER_FLAG_LEFT);
			}
		}
		
		node.layoutedRect.x = x;
		node.layoutedRect.y = y;
		node.layoutedRect.width = width;
		node.layoutedRect.height = height;
	}
	
	@Override
	public void mouseClicked(MouseEvent me) {
		if (!isEnabled()) return;
		if (me.getButton() != MouseEvent.BUTTON3) return;
		if (hoveredNode == null) return;
		//open context menu
		JPopupMenu menu = new JPopupMenu();
		if (hoveredNode.isLeaf()) {
			ImageTimelineObject current = hoveredNode.getLeafObject();
			if (current != null) {
				menu.add(new ClearTimelineObjectAction(current, hoveredNode));
				menu.addSeparator();
			}
			for (ImageTimelineObject obj : availableObjects) {
				if (obj != current) {
					menu.add(new UseTimelineObjectAction(obj, layout, hoveredNode));
				}
			}
		}
		menu.show(contentPanel, me.getX(), me.getY());
	}
	private static class ClearTimelineObjectAction extends AbstractAction {
		private final ImageTimelineObject obj;
		private final LayoutNode node;

		public ClearTimelineObjectAction(ImageTimelineObject obj, LayoutNode node) {
			super("Clear "+obj.getResource().getName());
			this.obj = obj;
			this.node = node;
		}

		@Override
		public void actionPerformed(ActionEvent ae) {
			if (node.getLeafObject() != null) {
				node.getLeafObject().setEnabledNoUndo(false);
			}
			node.setLeafObject(null);
		}
	}
	private static class UseTimelineObjectAction extends AbstractAction {
		private final ImageTimelineObject obj;
		private final Layout layout;
		private final LayoutNode node;

		public UseTimelineObjectAction(ImageTimelineObject obj, Layout layout, LayoutNode node) {
			super(obj.getResource().getName());
			this.obj = obj;
			this.layout = layout;
			this.node = node;
		}
		
		@Override
		public void actionPerformed(ActionEvent ae) {
			//1. find if the object is already in use
			LayoutNode n = findUsage(obj, layout.getRoot());
			if (n != null) {
				n.setLeafObject(null);
			}
			
			//2. disable old one
			if (node.getLeafObject() != null) {
				node.getLeafObject().setEnabledNoUndo(false);
			}
			
			//3. set new timeline object
			node.setLeafObject(obj);
			obj.setEnabledNoUndo(true);
		}
		private LayoutNode findUsage(ImageTimelineObject obj, LayoutNode node) {
			if (node.isLeaf()) {
				if (node.getLeafObject() == obj) {
					return node;
				} else {
					return null;
				}
			} else {
				for (LayoutNode c : node.getChildren()) {
					LayoutNode n = findUsage(obj, c);
					if (n != null) {
						return n;
					}
				}
			}
			return null;
		}
		
	}

        public void setSelectedNode(LayoutNode selection)
        {
            selectedNode = selection;
            propertyChangeSupport.firePropertyChange(PROP_SELECTION_CHANGED, null, selectedNode);
        }
        
	@Override
	public void mousePressed(MouseEvent me) {
		if (!isEnabled()) return;
		selectedNode = hoveredNode;
		propertyChangeSupport.firePropertyChange(PROP_SELECTION_CHANGED, null, selectedNode);
		
		//for dragging
		if (selectedNode == null) return;
		oldMousePos = new Point(me.getPoint());
		if (selectedNode.isLeaf() && selectedNode.getLeafObject()!=null) {
			oldTranslationX = selectedNode.getTranslationX();
			oldTranslationY = selectedNode.getTranslationY();
			oldZoomFactor = selectedNode.getZoomFactor();
			mousePressed = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent me) {
		if (!isEnabled()) return;
		if (selectedNode==null) return;
		
		if (selectedNode.isLeaf() && selectedNode.getLeafObject()!=null) {
			//fire undo-redo events
			selectedNode.fireTranslationXUndoRedo(oldTranslationX, selectedNode.getTranslationX());
			selectedNode.fireTranslationYUndoRedo(oldTranslationY, selectedNode.getTranslationY());
			selectedNode.fireZoomFactorUndoRedo(oldZoomFactor, selectedNode.getZoomFactor());
		}
		mousePressed = false;
	}

	@Override
	public void mouseEntered(MouseEvent me) {
		
	}

	@Override
	public void mouseExited(MouseEvent me) {
		
	}

	@Override
	public void mouseDragged(MouseEvent me) {
		if (selectedNode == null) {
			return;
		}
		if (selectedNode.isLeaf()) {
			ImageTimelineObject obj = selectedNode.getLeafObject();
			if (obj != null) {
				//adjust zoom anchor
				float deltaX = me.getX() - oldMousePos.x;
				float deltaY = me.getY() - oldMousePos.y;
//				Rectangle screenRect = convertToScreenRect(selectedNode.layoutedRect);
//				deltaX *= screenRect.width * selectedNode.getZoomFactor() / (float) selectedNode.layoutedRect.width;
//				deltaY *= screenRect.height * selectedNode.getZoomFactor() / (float) selectedNode.layoutedRect.height;
				deltaX /= transformScale;
				deltaY /= transformScale;
				selectedNode.setTranslationXNoUndo((int) (oldTranslationX+deltaX));
				selectedNode.setTranslationYNoUndo((int) (oldTranslationY-deltaY));
			}
		} else {
			//adjust split point
			
		}
	}

	@Override
	public void mouseMoved(MouseEvent me) {
		if (!isEnabled()) return;
		int mx = me.getX();
		int my = me.getY();
		//go down the hierarchy to find the closest match
		LayoutNode oldHoveredNode = hoveredNode;
		hoveredNode = null;
		pickNode(mx, my, layout.getRoot());
	}
	void pickNode(int mx, int my, LayoutNode node) {
		Rectangle rect = convertToScreenRect(node.layoutedRect);
		if (node.isLeaf()) {
			int dist = PICK_EXTRA_DISTANCE + layout.getInnerBorder();
			rect = new Rectangle(rect.x + dist, rect.y + dist, rect.width - 2*dist, rect.height - 2*dist);
		}
		if (rect.contains(mx, my)) {
			hoveredNode = node;
		}
		if (!node.isLeaf()) {
			for (LayoutNode children : node.getChildren()) {
				pickNode(mx, my, children);
			}
		}
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent mwe) {
		if (selectedNode == null || selectedNode!=hoveredNode || !mousePressed) {
			return;
		}
		if (selectedNode.isLeaf()) {
			ImageTimelineObject obj = selectedNode.getLeafObject();
			if (obj != null) {
				//adjust zoom
				float zoom = (float) (selectedNode.getZoomFactor() - mwe.getPreciseWheelRotation() * 0.05);
				selectedNode.setZoomFactorNoUndo(zoom);
			}
		}
	}

	@Override
	public void propertyChange(PropertyChangeEvent pce) {
		if (!isEnabled()) return;
		if (pce.getSource() == layout) {
			//trigger re-layouting
			layout();
		}
	}
	
}
