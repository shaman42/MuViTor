/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.layout;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.Project;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.shaman.muvitor.timeline.TimelineObject;
import org.simpleframework.xml.Element;

/**
 * Root of the layout manager.
 * It stores the layout tree and also where it is placed in the timeline object.
 * @author Sebastian
 */
public final class Layout {
	private static final Logger LOG = Logger.getLogger(Layout.class.getName());
	
	private Project project;
	private UndoableEditSupport undoSupport;
	private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	@Element
	private LayoutNode root;
	
	@Element
	private int innerBorder = 10;
	public static final String PROP_INNERBORDER = "innerBorder";

	@Element
	private int outerBorder = 10;
	public static final String PROP_OUTERBORDER = "outerBorder";
	
	public static final String PROP_NODE_CHANGED = "nodeChanged";
	
	private FrameTime[] influencingTimeInterval = null;

	/**
	 * serialization
	 */
	public Layout() {
	}

	/**
	 * Creates a new layout at the specified time.
	 * @param project
	 * @param undoSupport
	 */
	public Layout(Project project, UndoableEditSupport undoSupport) {
		root = new LayoutNode();
		init(project, undoSupport);
	}
	
	/**
	 * Clones this layout for storing it in a preset
	 * @return the cloned layout without all references to timeline objects
	 */
	public Layout cloneForSaving() {
		Layout l = new Layout();
		l.innerBorder = innerBorder;
		l.outerBorder = outerBorder;
		l.root = root.cloneForSaving();
		return l;
	}
	
	public void init(Project project, UndoableEditSupport undoSupport) {
		this.project = project;
		this.undoSupport = undoSupport;
		root.init(this, undoSupport);
	}

	public LayoutNode getRoot() {
		return root;
	}
	
	void fireNodeChanged(LayoutNode node) {
		influencingTimeInterval = null;
		propertyChangeSupport.firePropertyChange(PROP_NODE_CHANGED, null, node);
	}
	
	/**
	 * Get the size of the inner border in pixels
	 *
	 * @return the value of innerBorder
	 */
	public int getInnerBorder() {
		return innerBorder;
	}

	/**
	 * Set the size of the inner border in pixels
	 *
	 * @param newInnerBorder new value of innerBorder
	 */
	public void setInnerBorder(int newInnerBorder) {
		if (newInnerBorder < 0) {
			throw new IllegalArgumentException("inner border size must be non-negative");
		}
		int oldInnerBorder = this.innerBorder;
		this.innerBorder = newInnerBorder;
		propertyChangeSupport.firePropertyChange(PROP_INNERBORDER, oldInnerBorder, newInnerBorder);
		if (newInnerBorder != oldInnerBorder && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					innerBorder = oldInnerBorder;
					propertyChangeSupport.firePropertyChange(PROP_INNERBORDER, newInnerBorder, oldInnerBorder);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					innerBorder = newInnerBorder;
					propertyChangeSupport.firePropertyChange(PROP_INNERBORDER, oldInnerBorder, newInnerBorder);
				}
			
			});
		}
	}
	
	/**
	 * Get the size of the outer border in pixels
	 *
	 * @return the value of outerBorder
	 */
	public int getOuterBorder() {
		return outerBorder;
	}

	/**
	 * Set the size of the outer border in pixels
	 *
	 * @param newOuterBorder new value of outerBorder
	 */
	public void setOuterBorder(int newOuterBorder) {
		if (newOuterBorder < 0) {
			throw new IllegalArgumentException("outer border size must be non-negative");
		}
		int oldOuterBorder = this.outerBorder;
		this.outerBorder = newOuterBorder;
		propertyChangeSupport.firePropertyChange(PROP_OUTERBORDER, oldOuterBorder, newOuterBorder);
		if (newOuterBorder != oldOuterBorder && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					outerBorder = oldOuterBorder;
					propertyChangeSupport.firePropertyChange(PROP_OUTERBORDER, newOuterBorder, oldOuterBorder);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					outerBorder = newOuterBorder;
					propertyChangeSupport.firePropertyChange(PROP_OUTERBORDER, oldOuterBorder, newOuterBorder);
				}
			
			});
		}
	}
	
	/**
	 * Add PropertyChangeListener.
	 *
	 * @param listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * Remove PropertyChangeListener.
	 *
	 * @param listener
	 */
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}
	
	/**
	 * Collects all timeline objects that are referenced by {@link LayoutNode} leaves.
	 * @return 
	 */
	public Set<? extends ImageTimelineObject> collectLinkedObjects() {
		HashSet<ImageTimelineObject> set = new HashSet<>();
		fillLinkedObjectsSet(set, root);
		return set;
	}
	private void fillLinkedObjectsSet(Set<ImageTimelineObject> set, LayoutNode node) {
		if (node.isLeaf()) {
			ImageTimelineObject o = node.getLeafObject();
			if (o != null) {
				set.add(o);
			}
		} else {
			for (LayoutNode c : node.getChildren()) {
				fillLinkedObjectsSet(set, c);
			}
		}
	}
	
	/**
	 * Checks if all linked objects are still valid (i.e. exist in a channel)
	 */
	public void checkLinkedObjects() {
		checkLinkedObjects(root);
	}
	private void checkLinkedObjects(LayoutNode node) {
		if (node.isLeaf())  {
			ImageTimelineObject o = node.getLeafObject();
			if (o == null) return;
			if (!o.getChannel().getObjects().contains(o)) {
				//object was removed
				LOG.log(Level.INFO, "timeline object {0} removed from layout", o);
				node.setLeafObjectNoUndo(null);
			}
		} else {
			for (LayoutNode c : node.getChildren()) {
				checkLinkedObjects(c);
			}
		}
	}
	
	/**
	 * Computes the union of the time interval where all linked objects are active.
	 * Returns the lower and upper bound of that interval
	 * @return an array of length 2 with the lower and upper time
	 * @see #collectLinkedObjects() 
	 */
	public FrameTime[] getInfluencingTime() {
		if (influencingTimeInterval == null) {
			Set<? extends ImageTimelineObject> objx = collectLinkedObjects();
			FrameTime start = null;
			FrameTime end = null;
			for (ImageTimelineObject obj : objx) {
				FrameTime s = obj.getOffset().add(obj.getStart());
				FrameTime e = obj.getOffset().add(obj.getDuration());
				if (start == null) {
					start = s;
					end = e;
				} else {
					start = start.min(s);
					end = end.max(e);
				}
			}
			influencingTimeInterval = new FrameTime[]{start, end};
			if (project != null) {
				project.fireLayoutsChanged();
			}
		}
		return influencingTimeInterval;
	}
}
