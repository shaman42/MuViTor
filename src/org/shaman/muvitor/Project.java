/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import com.jme3.scene.Node;
import org.shaman.muvitor.resources.Resource;
import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.layout.Layout;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.timeline.Marker;
import org.shaman.muvitor.timeline.TimelineChannel;
import org.simpleframework.xml.*;

/**
 *
 * @author Sebastian Weiss
 */
@Root
public class Project implements Listenable {

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Attribute
    private int version = 1;

    @Element
    private String name;
    private File folder;
    @Element
    private int framerate;
    @Element
    private int width;
    @Element
    private int height;

    @ElementList
    private ArrayList<Resource> resources = new ArrayList<>();

    @ElementList
    private ArrayList<TimelineChannel> timelineChannels = new ArrayList<>();

    @ElementList
    private ArrayList<Marker> markers = new ArrayList<>();

    @Element
    private Color backgroundColor = new Color(0, 0, 0);
    public static final String PROP_BACKGROUNDCOLOR = "backgroundColor";

    @Element(required = false)
    private DynamicBackground dynamicBackground = new DynamicBackground();
    public static final String PROP_DYNAMICBACKGROUND = "dynamicBackground";

    /**
     * Get the value of dynamicBackground
     *
     * @return the value of dynamicBackground
     */
    public DynamicBackground getDynamicBackground() {
        return dynamicBackground;
    }

    /**
     * Set the value of dynamicBackground
     *
     * @param newDynamicBackground new value of dynamicBackground
     */
    public void setDynamicBackground(DynamicBackground newDynamicBackground) {
        DynamicBackground oldDynamicBackground = this.dynamicBackground;
        this.dynamicBackground = newDynamicBackground;
        propertyChangeSupport.firePropertyChange(PROP_DYNAMICBACKGROUND, oldDynamicBackground, newDynamicBackground);
        if (!Objects.equals(oldDynamicBackground, newDynamicBackground) && undoSupport != null) {
            undoSupport.postEdit(new AbstractUndoableEdit() {

                @Override
                public void undo() throws CannotUndoException {
                    super.undo();
                    dynamicBackground = oldDynamicBackground;
                    propertyChangeSupport.firePropertyChange(PROP_DYNAMICBACKGROUND, newDynamicBackground, oldDynamicBackground);
                }

                @Override
                public void redo() throws CannotRedoException {
                    super.redo();
                    dynamicBackground = newDynamicBackground;
                    propertyChangeSupport.firePropertyChange(PROP_DYNAMICBACKGROUND, oldDynamicBackground, newDynamicBackground);
                }

            });
        }
    }

    @ElementList(required = false)
    private ArrayList<Layout> layouts = new ArrayList<>();
    public static final String PROP_LAYOUTS_CHANGED = "layouts";

    //not persistent
    private FrameTime time;
    public static final String PROP_TIME = "time";
    private FrameTime length;
    public static final String PROP_LENGTH = "length";
    public static final String PROP_TIMELINE_CHANNEL_CHANGED = "tloChanged";
    public static final String PROP_TIMELINE_CHANNELS_CHANGED = "tloxChanged";
    public static final String PROP_TIMELINE_CHANNEL_ADDED = "tlo+";
    public static final String PROP_TIMELINE_CHANNEL_REMOVED = "tlo-";
    public static final String PROP_RESOURCE_ADDED = "res+";
    public static final String PROP_RESOURCE_REMOVED = "res-";
    public static final String PROP_MARKER_ADDED = "marker+";
    public static final String PROP_MARKER_REMOVED = "marker-";
    public static final String PROP_MARKER_CHANGED = "markerChanged";

    //for playback
    private Player player;
    private Node projectNode;

    private UndoableEditSupport undoSupport;

    public Project() {
    }

    public void init(UndoableEditSupport undoSupport) {
        this.undoSupport = undoSupport;
        for (TimelineChannel channel : timelineChannels) {
            channel.init(this, undoSupport);
        }
        for (Marker m : markers) {
            m.init(this, undoSupport);
        }
        for (Layout l : layouts) {
            l.init(this, undoSupport);
        }
        if (dynamicBackground == null)
            dynamicBackground = new DynamicBackground();
        dynamicBackground.setFramerate(framerate);
    }

    public void initPlayer(Player player, Node projectNode) {
        this.player = player;
        this.projectNode = projectNode;
        for (TimelineChannel c : timelineChannels) {
            this.projectNode.attachChild(c.initPlayer(player));
        }
    }

    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public File getFolder() {
        return folder;
    }

    public void setFolder(File folder) {
        this.folder = folder;
    }

    public int getFramerate() {
        return framerate;
    }

    public void setFramerate(int framerate) {
        this.framerate = framerate;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public ArrayList<Resource> getResources() {
        return resources;
    }

    public void setResources(ArrayList<Resource> resources) {
        if (resources == null) {
            resources = new ArrayList<>();
        }
        this.resources = resources;
    }

    public void addResource(Resource res) {
        resources.add(res);
        propertyChangeSupport.firePropertyChange(PROP_RESOURCE_ADDED, null, res);
    }

    public void removeResource(Resource res) {
        resources.remove(res);
        propertyChangeSupport.firePropertyChange(PROP_RESOURCE_REMOVED, res, null);
    }

    public ArrayList<TimelineChannel> getTimelineChannels() {
        return timelineChannels;
    }

    public void setTimelineObjects(ArrayList<TimelineChannel> timelineChannels) {
        if (timelineChannels == null) {
            timelineChannels = new ArrayList<>();
        }
        this.timelineChannels = timelineChannels;
    }

    /**
     * Fires a property changed event that the specified obj is changed
     *
     * @param obj
     */
    public void fireTimelineChannelChanged(TimelineChannel obj) {
        propertyChangeSupport.firePropertyChange(PROP_TIMELINE_CHANNEL_CHANGED, null, obj);
        propertyChangeSupport.firePropertyChange(PROP_TIMELINE_CHANNELS_CHANGED, null, null);
    }

    /**
     * Fires a property changed event that all timeline objects are changed.
     */
    public void fireTimelineChannelsChanged() {
        propertyChangeSupport.firePropertyChange(PROP_TIMELINE_CHANNELS_CHANGED, null, null);
    }

    public void addTimelineChannel(TimelineChannel obj) {
        assert (obj != null);
        timelineChannels.add(obj);
        obj.init(this, undoSupport);
        propertyChangeSupport.firePropertyChange(PROP_TIMELINE_CHANNEL_ADDED, null, obj);
        fireTimelineChannelsChanged();
        if (player != null) {
            player.enqueue(() -> {
                projectNode.attachChild(obj.initPlayer(player));
            });
        }
    }

    public void removeTimelineObject(TimelineChannel obj) {
        timelineChannels.remove(obj);
        propertyChangeSupport.firePropertyChange(PROP_TIMELINE_CHANNEL_REMOVED, obj, null);
        fireTimelineChannelsChanged();
        player.enqueue(() -> {
            projectNode.detachChild(obj.getChannelNode());
        });
    }

    public ArrayList<Marker> getMarkers() {
        return markers;
    }

    public void setMarkers(ArrayList<Marker> markers) {
        if (markers == null) {
            markers = new ArrayList<>();
        }
        this.markers = markers;
    }

    public void fireMarkerChanged(Marker marker) {
        propertyChangeSupport.firePropertyChange(PROP_MARKER_CHANGED, null, marker);
    }

    public void addMarker(Marker marker) {
        markers.add(marker);
        marker.init(this, undoSupport);
        propertyChangeSupport.firePropertyChange(PROP_MARKER_ADDED, null, marker);
        propertyChangeSupport.firePropertyChange(PROP_MARKER_CHANGED, null, marker);
        undoSupport.postEdit(new AbstractUndoableEdit() {
            @Override
            public void undo() throws CannotUndoException {
                super.undo();
                markers.remove(marker);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_REMOVED, marker, null);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_CHANGED, null, marker);
            }

            @Override
            public void redo() throws CannotRedoException {
                super.redo();
                markers.add(marker);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_ADDED, null, marker);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_CHANGED, null, marker);
            }

        });
    }

    public void removeMarker(Marker marker) {
        markers.remove(marker);
        propertyChangeSupport.firePropertyChange(PROP_MARKER_REMOVED, marker, null);
        propertyChangeSupport.firePropertyChange(PROP_MARKER_CHANGED, null, marker);
        undoSupport.postEdit(new AbstractUndoableEdit() {
            @Override
            public void undo() throws CannotUndoException {
                super.undo();
                markers.add(marker);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_ADDED, null, marker);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_CHANGED, null, marker);
            }

            @Override
            public void redo() throws CannotRedoException {
                super.redo();
                markers.remove(marker);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_REMOVED, marker, null);
                propertyChangeSupport.firePropertyChange(PROP_MARKER_CHANGED, null, marker);
            }

        });
    }

    /**
     * Get the value of backgroundColor
     *
     * @return the value of backgroundColor
     */
    public Color getBackgroundColor() {
        return backgroundColor;
    }

    private static int mix(float f, int a, int b)
    {
        f = Math.max(0, Math.min(1, f));
        return (int)(a + f*(b-a));
    }
    public Color getCurrentBackgroundColor(FrameTime time) {
        if (dynamicBackground.getTimes().isEmpty())
            return backgroundColor; //static background
        ArrayList<FrameTime> tx = dynamicBackground.getTimes();
        if (tx.get(0).compareTo(time)>=0)
            return dynamicBackground.getColors().get(0); //first time
        if (tx.get(tx.size()-1).compareTo(time)<=0)
            return dynamicBackground.getColors().get(tx.size()-1); //last time
        //interpolate
        int i;
        for(i=0; i<tx.size()-2; ++i)
            if (tx.get(i+1).compareTo(time)>0)
                break;
        FrameTime tLow = tx.get(i);
        FrameTime tHigh = tx.get(i+1);
        float frac = (time.toFrames() - tLow.toFrames()) / (float)(tHigh.toFrames() - tLow.toFrames());
        Color cLow = dynamicBackground.getColors().get(i);
        Color cHigh = dynamicBackground.getColors().get(i+1);
        return new Color(
                Project.mix(frac, cLow.getRed(), cHigh.getRed()),
                Project.mix(frac, cLow.getGreen(), cHigh.getGreen()),
                Project.mix(frac, cLow.getBlue(), cHigh.getBlue()));
    }

    /**
     * Set the value of backgroundColor
     *
     * @param newBackgroundColor new value of backgroundColor
     */
    public void setBackgroundColor(Color newBackgroundColor) {
        Color oldBackgroundColor = this.backgroundColor;
        this.backgroundColor = newBackgroundColor;
        propertyChangeSupport.firePropertyChange(PROP_BACKGROUNDCOLOR, oldBackgroundColor, newBackgroundColor);
        if (!Objects.equals(oldBackgroundColor, newBackgroundColor) && undoSupport != null) {
            undoSupport.postEdit(new AbstractUndoableEdit() {

                @Override
                public void undo() throws CannotUndoException {
                    super.undo();
                    backgroundColor = oldBackgroundColor;
                    propertyChangeSupport.firePropertyChange(PROP_BACKGROUNDCOLOR, newBackgroundColor, oldBackgroundColor);
                }

                @Override
                public void redo() throws CannotRedoException {
                    super.redo();
                    backgroundColor = newBackgroundColor;
                    propertyChangeSupport.firePropertyChange(PROP_BACKGROUNDCOLOR, oldBackgroundColor, newBackgroundColor);
                }

            });
        }
    }

    /**
     * Get the current playback time
     *
     * @return the value of time
     */
    public FrameTime getTime() {
        return time;
    }

    /**
     * Set the current playback time
     *
     * @param time new value of time
     */
    public void setTime(FrameTime time) {
        FrameTime oldTime = this.time;
        this.time = time;
        propertyChangeSupport.firePropertyChange(PROP_TIME, oldTime, time);
    }

    /**
     * @return the total length of the project in frames.
     */
    public FrameTime getLength() {
        if (length == null) {
            length = computeTotalLength();
        }
        return length;
    }

    /**
     * Sets the total length of the project in msec.
     *
     * @param length new value of length
     */
    public void setLength(FrameTime length) {
        FrameTime oldLength = this.length;
        this.length = length;
        propertyChangeSupport.firePropertyChange(PROP_LENGTH, oldLength, length);
    }

    /**
     * Computes the total length of this project
     */
    public FrameTime computeTotalLength() {
        FrameTime ft = timelineChannels.get(0).computeTotalLength();
        for (int i = 1; i < timelineChannels.size(); ++i) {
            ft = ft.max(timelineChannels.get(i).computeTotalLength());
        }
        return ft;
    }

    /**
     * @return the stored layouts
     */
    public ArrayList<Layout> getLayouts() {
        return layouts;
    }

    public void fireLayoutsChanged() {
        propertyChangeSupport.firePropertyChange(PROP_LAYOUTS_CHANGED, null, layouts);
    }

    @Override
    public String toString() {
        return name;
    }

}
