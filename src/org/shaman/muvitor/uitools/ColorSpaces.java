/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.uitools;

/**
 * A utility class to convert between color spaces.
 * 
 * https://en.wikipedia.org/wiki/YCbCr
 * @author Sebastian Weiss
 */
public class ColorSpaces {
	private ColorSpaces() {}
	
	/**
	 * a utility function to convert colors in RGB into YCbCr.
	 * @param r red color (0-1)
	 * @param g green color (0-1)
	 * @param b blue color (0-1)
	 * @return Y (0 - 1)
	 */
	public static float rgb2y(float r, float g, float b) {
		return 0.299f * r + 0.587f * g + 0.114f * b;
	}
	
	/**
	 * a utility function to convert colors in RGB into YCbCr.
	 * @param r red color (0-1)
	 * @param g green color (0-1)
	 * @param b blue color (0-1)
	 * @return Cb (-0.5 - 0.5)
	 */
	public static float rgb2cb(float r, float g, float b) {
		return -0.168736f * r - 0.331264f * g + 0.5f * b;
	}

	/**
	 * a utility function to convert colors in RGB into YCbCr.
	 * @param r red color (0-1)
	 * @param g green color (0-1)
	 * @param b blue color (0-1)
	 * @return Cr (-0.5 - 0.5)
	 */
	public static float rgb2cr(float r, float g, float b) {
		return 0.5f * r - 0.418688f * g - 0.081312f * b;
	}
	
	/**
	 * a utility function to convert colors from YCbCr into RGB
	 * @param y luminance, (0 - 1)
	 * @param cb cb chroma (-0.5 - 0.5)
	 * @param cr cr chroma (-0.5 - 0.5)
	 * @return red (0 - 1)
	 */
	public static float ycc2r(float y, float cb, float cr) {
		return y - 1.21889e-6f*cb + 1.402f*cr;
	}
	
	/**
	 * a utility function to convert colors from YCbCr into RGB
	 * @param y luminance, (0 - 1)
	 * @param cb cb chroma (-0.5 - 0.5)
	 * @param cr cr chroma (-0.5 - 0.5)
	 * @return red (0 - 1)
	 */
	public static float ycc2g(float y, float cb, float cr) {
		return y - 0.344136f*cb - 0.714136f*cr;
	}
	
	/**
	 * a utility function to convert colors from YCbCr into RGB
	 * @param y luminance, (0 - 1)
	 * @param cb cb chroma (-0.5 - 0.5)
	 * @param cr cr chroma (-0.5 - 0.5)
	 * @return red (0 - 1)
	 */
	public static float ycc2b(float y, float cb, float cr) {
		return y + 1.772f * cb + 4.06298e-7f * cr;
	}
}
