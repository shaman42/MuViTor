/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.uitools;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * A Color picker in the YCbCr-space for constant Y term.
 * @author Sebastian Weiss
 */
public class YCbCrColorPicker extends JPanel implements MouseListener, MouseMotionListener {
	
	private static final float Y = 0.5f;
	private static final int CURSOR_SIZE = 3;
	public static final String PROP_COLOR_DRAGGING = "colorDrag";
	public static final String PROP_COLOR_CHANGED = "colorChanged";

	private BufferedImage background;
	
	private float currentCb;
	private float currentCr;
	private float oldCb, oldCr;
	private boolean dragging;
	private float tolA, tolB;
	
	public YCbCrColorPicker() {
		setMinimumSize(new Dimension(100, 100));
		setPreferredSize(new Dimension(200, 200));
		addMouseListener(this);
		addMouseMotionListener(this);
	}

	public float getCurrentCb() {
		return currentCb;
	}

	public float getCurrentCr() {
		return currentCr;
	}

	public void setCurrentCb(float currentCb) {
		this.currentCb = currentCb;
		repaint();
	}

	public void setCurrentCr(float currentCr) {
		this.currentCr = currentCr;
		repaint();
	}
	
	public void setTolerance(float tolA, float tolB) {
		this.tolA = tolA;
		this.tolB = tolB;
		repaint();
	}
	
	private int rgb2int(float r, float g, float b) {
		r = Math.max(0, Math.min(1, r));
		g = Math.max(0, Math.min(1, g));
		b = Math.max(0, Math.min(1, b));
		int ri = (int) (255*r);
		int gi = (int) (255*g);
		int bi = (int) (255*b);
		return (255 << 24) | (ri << 16) | (gi << 8) | bi;
	}

	private void updateBackground() {
		int w = getWidth();
		int h = getHeight();
		int s = Math.min(w, h);
		if (background!=null && background.getWidth()==s) {
			return; //nothing to do
		}
		background = new BufferedImage(s, s, BufferedImage.TYPE_INT_ARGB);
		for (int x=0; x<s; ++x) {
			float cb = x/(float)s - 0.5f;
			for (int y=0; y<s; ++y) {
				float cr = y/(float)s - 0.5f;
				float r = ColorSpaces.ycc2r(Y, cb, cr);
				float g = ColorSpaces.ycc2g(Y, cb, cr);
				float b = ColorSpaces.ycc2b(Y, cb, cr);
				background.setRGB(x, y, rgb2int(r, g, b));
			}
		}
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		updateBackground();
		
		int w = getWidth();
		int h = getHeight();
		int s = Math.min(w, h);
		int ox = (w-s) / 2;
		int oy = (h-s) / 2;
		
		g.setClip(ox, oy, s+1, s+1);
		
		//background color gradient
		g.drawImage(background, ox, oy, this);
		
		//axis
		g.setColor(Color.WHITE);
		g.drawRect(ox, oy+s/2, s, 1);
		g.drawRect(ox+s/2, oy, 1, s);
		
		//cursor + tolerance
		int cx = (int) ((currentCb + 0.5) * s);
		int cy = (int) ((currentCr + 0.5) * s);
		int tolAS = (int) (tolA * s);
		int tolBS = (int) (tolB * s);
		g.setColor(Color.LIGHT_GRAY);
		g.drawOval(cx-tolBS+ox, cy-tolBS+oy, 2*tolBS, 2*tolBS);
		g.setColor(Color.DARK_GRAY);
		g.drawOval(cx-tolAS+ox, cy-tolAS+oy, 2*tolAS, 2*tolAS);
		g.setColor(Color.RED);
		g.drawOval(cx-CURSOR_SIZE+ox, cy-CURSOR_SIZE+oy, 2*CURSOR_SIZE, 2*CURSOR_SIZE);
		
		//border
		g.setColor(Color.BLACK);
		g.drawRect(ox, oy, s, s);
	}

	private boolean setFromMouse(Point m) {
		int w = getWidth();
		int h = getHeight();
		int s = Math.min(w, h);
		int ox = (w-s) / 2;
		int oy = (h-s) / 2;
		float cb = (m.x-ox)/(float)s - 0.5f;
		float cr = (m.y-oy)/(float)s - 0.5f;
		if (cb>=-0.5 && cb<=0.5 && cr>=-0.5 && cr<=0.5) {
			currentCb = cb;
			currentCr = cr;
			repaint();
			return true;
		} else {
			return false;
		}
	}
	
	private void fireChangeEvent() {
		firePropertyChange(PROP_COLOR_CHANGED, new float[]{oldCb, oldCr}, new float[]{currentCb, currentCr});
	}
	
	private void fireDragEvent() {
		firePropertyChange(PROP_COLOR_DRAGGING, null, new float[]{currentCb, currentCr});
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (setFromMouse(e.getPoint())) {
			oldCb = currentCb;
			oldCr = currentCr;
			dragging = true;
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (setFromMouse(e.getPoint()) && dragging) {
			fireChangeEvent();
		}
		dragging = false;
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
		dragging = false;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if (setFromMouse(e.getPoint())) {
			fireDragEvent();
			dragging = true;
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}
	
	public static void main(String[] args) {
		YCbCrColorPicker picker = new YCbCrColorPicker();
		picker.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName().equals(PROP_COLOR_CHANGED)) {
					System.out.println("color changed, cb="+picker.getCurrentCb()+", cr="+picker.getCurrentCr());
				} else if (evt.getPropertyName().equals(PROP_COLOR_DRAGGING)) {
					System.out.println("color dragging, cb="+picker.getCurrentCb()+", cr="+picker.getCurrentCr());
				}
			}
		});
		
		JFrame frame = new JFrame("ColorPicker Test");
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(picker);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
