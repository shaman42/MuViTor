/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import com.jme3.util.SafeArrayList;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.undo.*;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.Listenable;
import org.shaman.muvitor.filters.Filter;
import org.shaman.muvitor.player.Player;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

/**
 * This class represents an object in the timeline.
 * <p>
 * Properties:
 * <ul>
 * 
 * <li><b>offset</b>: 
 * the offset from time zero when the first frame should be played.<br>
 * This value can be negative, indicating that the playback of this resource
 * starts somewhere in the middle.<br>
 * This property is modified by moving the object in the timeline.
 * </li>
 * 
 * <li><b>start</b>:
 * the number of frames that are dropped before the resource is visible.<br>
 * This value must be non-negative.<br>
 * This property is modified by the left-crop hand in the timeline.
 * </li>
 * 
 * <li><b>duration</b>:
 * The number of frames that are played from the beginning, ignoring start.<br>
 * This property must be positive.<br>
 * This property is modified by the right-crop hand in the timeline.
 * </li>
 * 
 * </ul>
 * 
 * Invariances:
 * <ul>
 * <li><code>offset+start > 0</code> : no frames can be played at a negative time index</li>
 * <li><code>start+duration > 0</code> : the number of frames dropped at the beginning must not exceed the duration</li>
 * <li><code>duration <= resource_length</code> : the playback can only be as long as the underlying resource</li>
 * </ul>
 * 
 * Consequences:
 * <ul>
 * <li>The first frame has index <code>start</code> and is played at frame <code>offset+start</code></li>
 * <li>The last frame has index <code>duration-1</code> and is played at frame <code>offset+duration-1</code></li>
 * </ul>
 * 
 * @author Sebastian Weiss
 */
public abstract class TimelineObject implements Cloneable, Listenable {
	private static final Logger LOG = Logger.getLogger(TimelineObject.class.getName());	
	
	protected transient PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	@Element
	protected boolean enabled = true;
	public static final String PROP_ENABLED = "enabled";
	
	@Element
	protected FrameTime start = new FrameTime();
	public static final String PROP_START = "start";
	
	@Element
	protected FrameTime duration = new FrameTime();
	public static final String PROP_DURATION = "duration";
	
	@Element
	protected FrameTime offset = new FrameTime();
	public static final String PROP_OFFSET = "offset";
	
	@Element(required = false)
	protected boolean lockOffset = false;
	public static final String PROP_LOCK_OFFSET = "lockOffset";
	
	protected UndoableEditSupport undoSupport;
	protected TimelineChannel channel;
	
	/**
	 * Storage for the player
	 */
	public final HashMap<String, Object> playerProperties = new HashMap<>();
	
	@ElementList(required = false)
	protected ArrayList<TimelineObject> children = new ArrayList<>();
	public static final String PROP_CHILD_ADDED = "child+";
	public static final String PROP_CHILD_REMOVED = "child-";
	public static final String PROP_CHILD_MODIFIED = "childMod";
	
	@Element(required = false)
	protected TimelineObject parent;
	public static final String PROP_PARENT_CHANGED = "parent";
	
	@ElementList(required = false)
	protected SafeArrayList<Filter> filters = new SafeArrayList<>(Filter.class);
	public static final String PROP_FILTER_LIST_CHANGED = "filterList";
	public static final String PROP_FILTER_MODIFIED = "filterMod";

	/**
	 * Initializes this timeline object.
	 * This is called when the timeline object is added to a timeline channel.
	 * @param undoSupport the undo support
	 * @param channel the parent timeline channel
	 */
	public void init(UndoableEditSupport undoSupport, TimelineChannel channel) {
		this.undoSupport = undoSupport;
		this.channel = channel;
	}
	
	/**
	 * Before the timeline channels are updated+rendered over the node system,
	 * the timeline channels and objects are traversed multiple times
	 * with the following step semantics.
	 * This is used to prefetch the contents and compute the filters.
	 */
	public static enum PreUpdateStep {
		FETCH_CONTENT,
		APPLY_FILTERS
	}
	/**
	 * Performs a pre-update step. This is used to prefetch the contents
	 * and compute the filters.
	 * This is only called for active objects.
	 * @param player the player
	 * @param step the current step
	 */
	public void performPreUpdateStep(Player player, PreUpdateStep step) {};

	public boolean isActive(FrameTime time) {
		if (!isEnabled()) {
			return false;
		}
		if (channel!=null && !channel.isVisible()) {
			return false;
		}
		FrameTime end = offset.add(duration);
		FrameTime realStart = offset.add(start);
		if (time.compareTo(realStart)<0 || time.compareTo(end)>=0) {
			return false;
		}
		return true;
	}
	
	/**
	 * Add PropertyChangeListener.
	 *
	 * @param listener
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * Remove PropertyChangeListener.
	 *
	 * @param listener
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	@Override
	public TimelineObject clone() {
		try {
			TimelineObject obj = (TimelineObject) super.clone();
			//reset fields
			obj.channel = null;
			obj.undoSupport = null;
			obj.parent = null;
			obj.propertyChangeSupport = new PropertyChangeSupport(obj);
			obj.filters = new SafeArrayList<>(Filter.class);
			for (Filter f : filters) {
				obj.filters.add(f.cloneForParent(obj));
			}
			return obj;
		} catch (CloneNotSupportedException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public TimelineChannel getChannel() {
		return channel;
	}
	
	/**
	 * Get the value of enabled
	 *
	 * @return the value of enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabledNoUndo(final boolean newEnabled) {
		final boolean oldEnabled = this.enabled;
		this.enabled = newEnabled;
		propertyChangeSupport.firePropertyChange(PROP_ENABLED, oldEnabled, newEnabled);
		if (channel != null && this instanceof ResourceTimelineObject) channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
	}
	
	/**
	 * Set the value of enabled
	 *
	 * @param newEnabled new value of enabled
	 */
	public void setEnabled(final boolean newEnabled) {
		final boolean oldEnabled = this.enabled;
		this.enabled = newEnabled;
		propertyChangeSupport.firePropertyChange(PROP_ENABLED, oldEnabled, newEnabled);
		if (channel != null && this instanceof ResourceTimelineObject) channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
		if (!Objects.equals(oldEnabled, newEnabled) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					enabled = oldEnabled;
					propertyChangeSupport.firePropertyChange(PROP_ENABLED, newEnabled, oldEnabled);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					enabled = newEnabled;
					propertyChangeSupport.firePropertyChange(PROP_ENABLED, oldEnabled, newEnabled);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}
			
			});
		}
	}

	public ArrayList<TimelineObject> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<TimelineObject> children) {
		this.children = children;
	}
	
	public void addChild(TimelineObject child) {
		children.add(child);
		propertyChangeSupport.firePropertyChange(PROP_CHILD_ADDED, null, child);
	}
	
	public void removeChild(TimelineObject child) {
		children.remove(child);
		propertyChangeSupport.firePropertyChange(PROP_CHILD_REMOVED, child, null);
	}
	
	public void fireChildChanged(TimelineObject child) {
		propertyChangeSupport.firePropertyChange(PROP_CHILD_MODIFIED, null, child);
	}

	public TimelineObject getParent() {
		return parent;
	}

	public void setParent(TimelineObject parent) {
		TimelineObject oldParent = this.parent;
		this.parent = parent;
		propertyChangeSupport.firePropertyChange(PROP_PARENT_CHANGED, oldParent, parent);
	}

	/**
	 * Get the duration in msec
	 *
	 * @return the value of duration
	 */
	public FrameTime getDuration() {
		return duration;
	}

	/**
	 * Sets the duration without triggering undo/redo events.
	 * This is used in the timeline while editing the duration.
	 * Note: the new duration can be modified to enforce that it stays within the
	 * bounds. Therefore, the actual new duration is returned
	 * @param newDuration the new duration
	 * @return the new duration after bound tests
	 */
	public FrameTime setDurationNoUndo(FrameTime newDuration) {
		final FrameTime oldDuration = this.duration;
		this.duration = newDuration;
		propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_DURATION, oldDuration, newDuration);
		if (channel != null && this instanceof ResourceTimelineObject) {
			channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
		}
		return newDuration;
	}
	
	/**
	 * Set the duration in msec
	 *
	 * @param newDuration new value of duration
	 */
	public void setDuration(FrameTime newDuration) {
		final FrameTime oldDuration = this.duration;
		newDuration = setDurationNoUndo(newDuration);
		fireDurationUndoRedo(oldDuration, newDuration);
	}

	/**
	 * Fires the undo-redo event explicitly. This is needed if the changes where
	 * made with {@link #setDurationNoUndo(org.shaman.muvitor.FrameTime) }.
	 * @param oldDuration
	 * @param newDuration 
	 */
	public void fireDurationUndoRedo(FrameTime oldDuration, FrameTime newDuration) {
		if (!Objects.equals(oldDuration, newDuration) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {
				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					duration = oldDuration;
					propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_DURATION, newDuration, oldDuration);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					duration = newDuration;
					propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_DURATION, oldDuration, newDuration);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}
			});
		}
	}
	
	/**
	 * Get the start time in frames from the parent object
	 *
	 * @return the value of start
	 */
	public FrameTime getStart() {
		return start;
	}
	
	/**
	 * Sets the start time without triggering undo-redo events.
	 * This is needed for interactive editing in the timeline.
	 * Because of bound tests, the new start time might be altered.
	 * The actual new start is returned.
	 * @param newStart
	 * @return 
	 */
	public FrameTime setStartNoRedo(FrameTime newStart) {
		final FrameTime oldStart = this.start;
		this.start = newStart;
		propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_START, oldStart, newStart);
		if (channel != null && this instanceof ResourceTimelineObject) {
			channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
		}
		return this.start;
	}
	
	/**
	 * Set the start time
	 *
	 * @param newStart new value of start
	 */
	public void setStart(FrameTime newStart) {
		final FrameTime oldStart = this.start;
		newStart = setStartNoRedo(newStart);
		fireStartUndoRedo(oldStart, newStart);
	}
	
	public void fireStartUndoRedo(FrameTime oldStart, FrameTime newStart) {
		if (!Objects.equals(oldStart, newStart) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {
				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					start = oldStart;
					propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_START, newStart, oldStart);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					start = newStart;
					propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_START, oldStart, newStart);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}
			});
		}
	}

	/**
	 * Get the offset
	 *
	 * @return the value of offset
	 */
	public FrameTime getOffset() {
		return offset;
	}

	/**
	 * Sets the offset time without triggering undo-redo events.
	 * This is needed for interactive editing in the timeline.
	 * Because of bound tests, the new offset time might be altered.
	 * The actual new start is returned.
	 * @param newOffset 
	 * @return 
	 */
	public FrameTime setOffsetNoRedo(FrameTime newOffset) {
		if (isLockOffset()) return this.offset;
		final FrameTime oldOffset = this.offset;
		this.offset = newOffset;
		propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_OFFSET, oldOffset, newOffset);
		if (channel != null && this instanceof ResourceTimelineObject) {
			channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
		}
		return this.start;
	}
	
	/**
	 * Set the offset
	 *
	 * @param newOffset new value of duration
	 */
	public void setOffset(FrameTime newOffset) {
		final FrameTime oldOffset = this.offset;
		newOffset = setOffsetNoRedo(newOffset);
		fireOffsetUndoRedo(oldOffset, newOffset);
	}

	public void fireOffsetUndoRedo(FrameTime oldOffset, FrameTime newOffset) {
		if (!Objects.equals(oldOffset, newOffset) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {
				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					offset = oldOffset;
					propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_OFFSET, newOffset, oldOffset);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					offset = newOffset;
					propertyChangeSupport.firePropertyChange(ResourceTimelineObject.PROP_OFFSET, oldOffset, newOffset);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}
			});
		}
	}

	public boolean isLockOffset() {
		return lockOffset;
	}

	public void setLockOffset(final boolean newLockOffset) {
		final boolean oldLockOffset = this.enabled;
		this.lockOffset = newLockOffset;
		propertyChangeSupport.firePropertyChange(PROP_LOCK_OFFSET, oldLockOffset, newLockOffset);
		if (channel != null && this instanceof ResourceTimelineObject) channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
		if (!Objects.equals(oldLockOffset, newLockOffset) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					lockOffset = oldLockOffset;
					propertyChangeSupport.firePropertyChange(PROP_LOCK_OFFSET, newLockOffset, oldLockOffset);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					lockOffset = newLockOffset;
					propertyChangeSupport.firePropertyChange(PROP_LOCK_OFFSET, oldLockOffset, newLockOffset);
					if (channel != null && TimelineObject.this instanceof ResourceTimelineObject) 
						channel.fireTimelineObjectChanged((ResourceTimelineObject) TimelineObject.this);
				}
			
			});
		}
	}
	
	/**
	 * @return an array of filters.
	 */
	public Filter[] getFilterArray() {
		return filters.getArray();
	}
	
	/**
	 * Returns the SafeArrayList instance that is mutable.
	 * Modify the list here and then trigger
	 * {@link #fireFilterListChanged(org.shaman.muvitor.filters.Filter[]) }.
	 * @return 
	 */
	public SafeArrayList<Filter> getFilterListToModify() {
		return filters;
	}
	
	/**
	 * Fires an undo-event that the filter list has changed.
	 * The old contents are passed as parameter, the new contents are expected
	 * to be in the list returned by {@link #getFilterListToModify() }.
	 * @param oldContents 
	 */
	public void fireFilterListChanged(final Filter[] oldContents) {
		final Filter[] newContents = filters.getArray();
		propertyChangeSupport.firePropertyChange(PROP_FILTER_LIST_CHANGED, oldContents, newContents);
		if (channel != null && this instanceof ResourceTimelineObject) {
			channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
		}
		if (!Objects.equals(oldContents, newContents) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					filters.clear();
					filters.addAll(Arrays.asList(oldContents));
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					filters.clear();
					filters.addAll(Arrays.asList(newContents));
				}
			
			});
		}
	}
	
	/**
	 * Notifies the parent that a filter has changed and that it should
	 * redraw / revalidate.
	 * @param filter 
	 */
	public void fireFilterModified(final Filter filter) {
		propertyChangeSupport.firePropertyChange(PROP_FILTER_MODIFIED, null, filter);
		if (channel != null && this instanceof ResourceTimelineObject) {
			channel.fireTimelineObjectChanged((ResourceTimelineObject) this);
		}
	}
}
