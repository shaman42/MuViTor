/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.shape.Quad;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;
import org.shaman.muvitor.resources.Resource;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.filters.Filter;
import org.shaman.muvitor.player.Player;
import org.simpleframework.xml.Element;

/**
 *
 * @author Sebastian Weiss
 * @param <R> the type of resource
 */
public abstract class ResourceTimelineObject<R extends Resource> extends TimelineObject {
	private static final Logger LOG = Logger.getLogger(ResourceTimelineObject.class.getName());
	
	@Element
	protected R resource;
	public static final String PROP_RESOURCE = "resource";
	
	//for playback
	private Player player;
	protected Node objectNode;
	
	public ResourceTimelineObject() {
	}

	public ResourceTimelineObject(R resource) {
		setResource(resource);
	}

	@Override
	public ResourceTimelineObject clone() {
		ResourceTimelineObject obj = (ResourceTimelineObject) super.clone();
		//reset playback details
		obj.player = null;
		obj.objectNode = null;
		return obj;
	}
	
	public Node initPlayer(Player player) {
		this.player = player;
		this.objectNode = new Node(resource.getName());
		this.objectNode.addControl(new AbstractControl() {
			@Override
			protected void controlUpdate(float tpf) {
				ResourceTimelineObject.this.update(player, player.getTime(), tpf);
			}

			@Override
			protected void controlRender(RenderManager arg0, ViewPort arg1) {
			}
		});
		for (Filter f : filters) {
			f.initialize(this, player, undoSupport);
		}
		return this.objectNode;
	}
	public Node getNode() {
		return objectNode;
	}
	
	protected abstract void update(Player player, FrameTime time, float tpf);
	
	/**
	 * Get the value of resource
	 *
	 * @return the value of resource
	 */
	public R getResource() {
		return resource;
	}

	/**
	 * Set the value of resource
	 *
	 * @param newResource new value of resource
	 */
	public void setResource(final R newResource) {
		if (newResource == resource) return;
		if (resource instanceof Resource.AudioProvider && this instanceof ImageTimelineObject) {
			if (((ImageTimelineObject) this).isPlayAudio()) {
				((Resource.AudioProvider) newResource).requestAudio();
			}
		}
		final R oldResource = this.resource;
		this.resource = newResource;
		propertyChangeSupport.firePropertyChange(PROP_RESOURCE, oldResource, newResource);
		if (!Objects.equals(oldResource, newResource) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					resource = oldResource;
					propertyChangeSupport.firePropertyChange(PROP_RESOURCE, newResource, oldResource);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					resource = newResource;
					propertyChangeSupport.firePropertyChange(PROP_RESOURCE, oldResource, newResource);
				}
			
			});
		}
	}
	
	//overwrite setters to ensure invariances

	@Override
	public FrameTime setOffsetNoRedo(FrameTime newOffset) {
		if (isLockOffset()) return this.offset;
		//ensure newOffset+start > 0
		int frames = newOffset.toFrames() + start.toFrames();
		if (frames < 0) {
			newOffset = newOffset.add(-frames);
		}
		return super.setOffsetNoRedo(newOffset);
	}

	@Override
	public FrameTime setStartNoRedo(FrameTime newStart) {
		//ensure newStart>=0
		int frames = newStart.toFrames();
		if (frames < 0) {
			newStart = new FrameTime(0, 0, newStart.getFramesPerSecond());
		}
		//ensure newStart < duration
		if (frames >= duration.toFrames()) {
			newStart = FrameTime.fromFrames(duration.toFrames()-1, newStart.getFramesPerSecond());
		}
		//ensure offset+newStart > 0
		frames = newStart.toFrames() + offset.toFrames();
		if (frames < 0) {
			newStart = newStart.add(-frames);
		}
		return super.setStartNoRedo(newStart);
	}

	@Override
	public FrameTime setDurationNoUndo(FrameTime newDuration) {
		//ensure newDuration>0
		int frames = newDuration.toFrames();
		if (frames <= 0) {
			newDuration = new FrameTime(0, 1, newDuration.getFramesPerSecond());
		}
		//ensure start < newDuration
		if (frames <= start.toFrames()) {
			newDuration = FrameTime.fromFrames(start.toFrames()+1, newDuration.getFramesPerSecond());
		}
		//ensure newDuration<=length
		frames = newDuration.toFrames();
		if (frames > resource.getDurationInFrames() && resource.getDurationInFrames()>0) {
			newDuration = FrameTime.fromFrames(resource.getDurationInFrames(), newDuration.getFramesPerSecond());
		}
		return super.setDurationNoUndo(newDuration);
	}
	
	/**
	 * This method allows the timeline object to overwrite which actual frame
	 * is displayed at the given frame in the playback.
	 * <p>
	 * Examples:<br>
	 * A regular video just returns frame.<br>
	 * An image always returns 0, because no change happens (no duplicate caching)<br>
	 * A video that is linked against an audio stream ignores this.offset and
	 * computes the offset by itself and returns the modified version here.
	 * @param frame
	 * @return 
	 */
	public int convertFrame(int frame) {
		return frame;
	}
	
	//helper functions to create the playback
	
	protected AudioNode createAudioNode(Resource.AudioProvider res) {
		AudioNode audioNode = new AudioNode(res.getAudioData(), res.getAudioKey());
		audioNode.setPositional(false);
		return audioNode;
	}
	
	protected void updateAudioNode(AudioNode audioNode, Player player, FrameTime time) {
		//check for stopping
		FrameTime end = offset.add(duration);
		if (audioNode.getStatus()==AudioSource.Status.Playing
				&& (!player.isPlaying() || time.compareTo(end)>0 
				|| !channel.isVisible() || !isEnabled())) {
			audioNode.stop();
			LOG.log(Level.INFO, "stop playback of {0}", resource.getName());
			return;
		}
		//check for starting
		if (audioNode.getStatus()==AudioSource.Status.Stopped
				&& player.isPlaying() && channel.isVisible() && isEnabled()) {
			FrameTime realStart = start.add(offset);
			if (time.compareTo(realStart)>=0 && time.compareTo(end)<0) {
				//we are in the bounds, seek audio and play
				int offsetInFrames = time.toFrames() - offset.toFrames();
				float timeOffset = offsetInFrames / (float)time.getFramesPerSecond();
				audioNode.setTimeOffset(timeOffset);
				audioNode.play();
				LOG.log(Level.INFO, "start playback of {0} at {1} sec", new Object[]{resource.getName(), timeOffset});
			}
		}
	}
	
	protected Geometry createImageDisplay(Resource.ImageProvider res) {
		//create material
		Material mat = new Material(player.getAssetManager(), "org/shaman/muvitor/shaders/Image.j3md");
		mat.setTexture("ColorMap", res.getFrame(0, true));
		mat.getAdditionalRenderState().setFaceCullMode(RenderState.FaceCullMode.Off);
		mat.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
		//create geometry
		Geometry geom = new Geometry(res.getName(), new Quad(1, 1));
		geom.setMaterial(mat);
//		geom.setQueueBucket(RenderQueue.Bucket.Transparent);
		geom.setCullHint(Spatial.CullHint.Always);
		//done
		return geom;
	}
	
	protected void updateImageBounds(Geometry image, int posX, int posY, 
			int width, int height, float aspectRatio, boolean keepAspectRatio) {
		//compute coords
		int x = posX;
		int y = posY;
		int w = width;
		int h = height;
		if (keepAspectRatio) {
			//w,h defines a rect, place the image centered inside it
			float aspect = w / (float) h;
			if (aspect > aspectRatio) {
				// w is greater
				w = (int) (h * aspectRatio);
				x += (width - w) / 2;
			} else if (aspect < aspectRatio) {
				// h is greater
				h = (int) (w / aspectRatio);
				y += (height - h) / 2;
			}
		}
		//transform the image
		image.setLocalTranslation(x-0.5f, y-0.5f, 0);
		image.setLocalScale(w+1, h+1, 1);
	}
	
	private int getFrameIndex(FrameTime time) {
		return time.toFrames() - offset.toFrames();
	}
	
	protected void prefetchImageContent(Resource.ImageProvider res, FrameTime time, boolean thumbnail) {
		int frame = getFrameIndex(time);
		res.prefetchFrame(frame, thumbnail);
	}
	
	protected void applyImageFilters(Geometry image, Resource.ImageProvider res,
			Player player, FrameTime time, boolean thumbnail) {
		int frame = getFrameIndex(time);
		//get frame (should be prefetched)
		Texture2D tex = res.getFrame(frame, thumbnail);
		//call filters
		tex = player.getFilterExecutor().executeFilters(tex, filters);
		//send to material
		image.getMaterial().setTexture("ColorMap", tex);
	}

	@Override
	public String toString() {
		return "Timeline: " + resource.getName();
	}
}
