/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import org.shaman.muvitor.FrameTime;
import org.simpleframework.xml.Element;

/**
 * A class to manage and store starting time + duration.
 * The comparator compares the start times
 * @author Sebastian
 */
@Deprecated
public final class StartDuration implements Cloneable, Comparable<StartDuration> {

	@Element
	private final FrameTime start;
	@Element
	private final FrameTime duration;

	public StartDuration(FrameTime start, FrameTime duration) {
		this.start = start;
		this.duration = duration;
	}

	public FrameTime getStart() {
		return start;
	}

	public FrameTime getDuration() {
		return duration;
	}
	
	public StartDuration clone() {
		return new StartDuration(start, duration);
	}
	
	@Override
	public int compareTo(StartDuration t) {
		return start.compareTo(t.start);
	}
	
}
