/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource;
import com.jme3.scene.Node;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.resources.AudioResource;


public class AudioTimelineObject extends ResourceTimelineObject<AudioResource> {
	private static final Logger LOG = Logger.getLogger(AudioTimelineObject.class.getName());

	private AudioNode audioNode;
	
	public AudioTimelineObject() {
	}

	public AudioTimelineObject(AudioResource resource) {
		super(resource);
	}

	@Override
	public Node initPlayer(Player player) {
		Node node = super.initPlayer(player);
		audioNode = createAudioNode(resource);
		node.attachChild(audioNode);
		return node;
	}

	@Override
	protected void update(Player player, FrameTime time, float tpf) {
		updateAudioNode(audioNode, player, time);
	}
}
