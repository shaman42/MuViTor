/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import java.awt.Color;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Objects;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.Listenable;
import org.shaman.muvitor.Project;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * A marker in the timeline.
 * It acts as a visual indicator for the user for different stages of the project,
 * it acts as snap point and as start and end point in the exporter.
 * @author Sebastian
 */
public class Marker implements Comparable<Marker>, Listenable{
	
	private Project project;
	private UndoableEditSupport undoSupport;
	private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	@Element
	private FrameTime time;
	public static final String PROP_TIME = "time";
	
	@Element
	private Color color = Color.BLUE;
	public static final String PROP_COLOR = "color";
	
	@Element(required = false)
	private String displayedCharacter = "";
	public static final String PROP_DISPLAYEDCHARACTER = "displayedCharacter";
	
	@Element(required = false)
	private String tooltip = "";
	public static final String PROP_TOOLTIP = "tooltip";

	public Marker() {
	}

	public Marker(FrameTime time) {
		this.time = time;
		assert(time != null);
	}
	
	public void init(Project project, UndoableEditSupport undoSupport) {
		this.project = project;
		this.undoSupport = undoSupport;
	}

	public Project getProject() {
		return project;
	}

	/**
	 * Get the time of this marker
	 *
	 * @return the value of time
	 */
	public FrameTime getTime() {
		return time;
	}

	public void setTimeNoUndo(FrameTime newTime) {
		assert(newTime != null);
		FrameTime oldTime = this.time;
		this.time = newTime;
		propertyChangeSupport.firePropertyChange(PROP_TIME, oldTime, newTime);
		if (project != null) project.fireMarkerChanged(this);
	}
	
	/**
	 * Set the value of time
	 *
	 * @param newTime new value of time
	 */
	public void setTime(FrameTime newTime) {
		assert(newTime != null);
		FrameTime oldTime = this.time;
		setTimeNoUndo(newTime);
		fireTimeChanged(oldTime, newTime);
	}
	public void fireTimeChanged(FrameTime oldTime, FrameTime newTime) {
		propertyChangeSupport.firePropertyChange(PROP_TIME, oldTime, newTime);
		if (!Objects.equals(oldTime, newTime) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					time = oldTime;
					propertyChangeSupport.firePropertyChange(PROP_TIME, newTime, oldTime);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					time = newTime;
					propertyChangeSupport.firePropertyChange(PROP_TIME, oldTime, newTime);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}
			
			});
		}
	}

	/**
	 * Get the background color of this marker
	 *
	 * @return the value of color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Set the background color of this marker
	 *
	 * @param newColor new value of color
	 */
	public void setColor(Color newColor) {
		Color oldColor = this.color;
		this.color = newColor;
		propertyChangeSupport.firePropertyChange(PROP_COLOR, oldColor, newColor);
		if (project != null) project.fireMarkerChanged(this);
		if (!Objects.equals(oldColor, newColor) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					color = oldColor;
					propertyChangeSupport.firePropertyChange(PROP_COLOR, newColor, oldColor);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					color = newColor;
					propertyChangeSupport.firePropertyChange(PROP_COLOR, oldColor, newColor);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}
			
			});
		}
	}

	/**
	 * Get the string that is displayed inside the marker
	 *
	 * @return the value of displayedCharacter
	 */
	public String getDisplayedCharacter() {
		return displayedCharacter;
	}

	/**
	 * Set the string that is displayed inside the marker.
	 * It will be trimmed to contain at most one character.
	 *
	 * @param newDisplayedCharacter new value of displayedCharacter
	 */
	public void setDisplayedCharacter(String newDisplayedCharacter) {
		String newDisplayedCharacter2 = newDisplayedCharacter.isEmpty()
				? " "
				: newDisplayedCharacter.substring(0, 1);
		String oldDisplayedCharacter = this.displayedCharacter;
		this.displayedCharacter = newDisplayedCharacter2;
		propertyChangeSupport.firePropertyChange(PROP_DISPLAYEDCHARACTER, oldDisplayedCharacter, newDisplayedCharacter2);
		if (project != null) project.fireMarkerChanged(this);
		if (!oldDisplayedCharacter.equals(newDisplayedCharacter2) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					displayedCharacter = oldDisplayedCharacter;
					propertyChangeSupport.firePropertyChange(PROP_DISPLAYEDCHARACTER, newDisplayedCharacter2, oldDisplayedCharacter);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					displayedCharacter = newDisplayedCharacter2;
					propertyChangeSupport.firePropertyChange(PROP_DISPLAYEDCHARACTER, oldDisplayedCharacter, newDisplayedCharacter2);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}
			
			});
		}
	}

	/**
	 * Get the tooltip
	 *
	 * @return the value of tooltip
	 */
	public String getTooltip() {
		return tooltip;
	}

	/**
	 * Set the tooltip
	 *
	 * @param newTooltip new value of tooltip
	 */
	public void setTooltip(String newTooltip) {
		String oldTooltip = this.tooltip;
		this.tooltip = newTooltip;
		propertyChangeSupport.firePropertyChange(PROP_TOOLTIP, oldTooltip, newTooltip);
		if (project != null) project.fireMarkerChanged(this);
		if (!Objects.equals(oldTooltip, newTooltip) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					tooltip = oldTooltip;
					propertyChangeSupport.firePropertyChange(PROP_TOOLTIP, newTooltip, oldTooltip);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					tooltip = newTooltip;
					propertyChangeSupport.firePropertyChange(PROP_TOOLTIP, oldTooltip, newTooltip);
					if (project != null) project.fireMarkerChanged(Marker.this);
				}
			
			});
		}
	}
	
	/**
	 * Add PropertyChangeListener.
	 *
	 * @param listener
	 */
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}

	/**
	 * Remove PropertyChangeListener.
	 *
	 * @param listener
	 */
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	
	@Override
	public int compareTo(Marker t) {
		return this.time.compareTo(t.time);
	}
	
}
