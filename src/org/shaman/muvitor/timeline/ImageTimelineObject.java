/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import com.jme3.audio.AudioNode;
import com.jme3.audio.AudioSource;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Matrix4f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.util.TempVars;
import java.awt.Color;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.PresetSaveable;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.resources.Resource;
import org.simpleframework.xml.Element;

/**
 * Timeline object for both images and videos
 * @author Sebastian Weiss
 * @param <T> 
 */
public class ImageTimelineObject<T extends Resource & Resource.ImageProvider> 
		extends ResourceTimelineObject<T> implements PresetSaveable<ImageTimelineObject.CopyableSettings>{

	private static final Logger LOG = Logger.getLogger(ImageTimelineObject.class.getName());

	/**
	 * The settings that can be copied or saved as presets
	 */
	public static class CopyableSettings implements Cloneable {
		@Element public int x;
		@Element public int y;
		@Element(required = false) public int z;
		@Element public int width;
		@Element public int height;
		@Element public boolean keepAspectRatio = true;
		@Element public float cropLeft;
		@Element public float cropRight;
		@Element public float cropTop;
		@Element public float cropBottom = 0;
		@Element public float scalingFactor = 1;
		@Element public float scalingAnchorX = 0.5f;
		@Element public float scalingAnchorY = 0.5f;
		@Element public int border = 0;
		@Element public Color borderColor = new Color(0, 0, 0);
		@Element public int fadeInLength = 0;
		@Element public int fadeOutLength = 0;
                @Element(required = false) public int fadeInOffset = 0;
		@Element(required = false) public int fadeOutOffset = 0;
                @Element(required = false) public Color fadeColor = new Color(0, 0, 0);
                @Element(required = false) public float fadeAlpha = 0; //1=blend to fadeColor, 0=blend to Background
                @Element(required = false) public boolean flipX = false;
                @Element(required = false) public boolean flipY = false;

		@Override
		public CopyableSettings clone(){
			try {
				CopyableSettings s = (CopyableSettings) super.clone();
				s.borderColor = new Color(s.borderColor.getRed(), s.borderColor.getGreen(), s.borderColor.getBlue(), s.borderColor.getAlpha());
				return s;
			} catch (CloneNotSupportedException ex) {
				throw new RuntimeException(ex);
			}
		}

		@Override
		public int hashCode() {
			int hash = 3;
			hash = 71 * hash + this.x;
			hash = 71 * hash + this.y;
			hash = 71 * hash + this.z;
			hash = 71 * hash + this.width;
			hash = 71 * hash + this.height;
			hash = 71 * hash + (this.keepAspectRatio ? 1 : 0);
			hash = 71 * hash + Float.floatToIntBits(this.cropLeft);
			hash = 71 * hash + Float.floatToIntBits(this.cropRight);
			hash = 71 * hash + Float.floatToIntBits(this.cropTop);
			hash = 71 * hash + Float.floatToIntBits(this.cropBottom);
			hash = 71 * hash + Float.floatToIntBits(this.scalingFactor);
			hash = 71 * hash + Float.floatToIntBits(this.scalingAnchorX);
			hash = 71 * hash + Float.floatToIntBits(this.scalingAnchorY);
			hash = 71 * hash + this.border;
			hash = 71 * hash + Objects.hashCode(this.borderColor);
			hash = 71 * hash + this.fadeInLength;
			hash = 71 * hash + this.fadeOutLength;
                        hash = 71 * hash + this.fadeInOffset;
			hash = 71 * hash + this.fadeOutOffset;
                        hash = 71 * hash + Objects.hashCode(this.fadeColor);
                        hash = 71 * hash + Float.floatToIntBits(this.fadeAlpha);
                        hash = 71 * hash + (this.flipX?1:0);
                        hash = 71 * hash + (this.flipY?1:0);
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final CopyableSettings other = (CopyableSettings) obj;
			if (this.x != other.x) {
				return false;
			}
			if (this.y != other.y) {
				return false;
			}
			if (this.z != other.z) {
				return false;
			}
			if (this.width != other.width) {
				return false;
			}
			if (this.height != other.height) {
				return false;
			}
			if (this.keepAspectRatio != other.keepAspectRatio) {
				return false;
			}
			if (Float.floatToIntBits(this.cropLeft) != Float.floatToIntBits(other.cropLeft)) {
				return false;
			}
			if (Float.floatToIntBits(this.cropRight) != Float.floatToIntBits(other.cropRight)) {
				return false;
			}
			if (Float.floatToIntBits(this.cropTop) != Float.floatToIntBits(other.cropTop)) {
				return false;
			}
			if (Float.floatToIntBits(this.cropBottom) != Float.floatToIntBits(other.cropBottom)) {
				return false;
			}
			if (Float.floatToIntBits(this.scalingFactor) != Float.floatToIntBits(other.scalingFactor)) {
				return false;
			}
			if (Float.floatToIntBits(this.scalingAnchorX) != Float.floatToIntBits(other.scalingAnchorX)) {
				return false;
			}
			if (Float.floatToIntBits(this.scalingAnchorY) != Float.floatToIntBits(other.scalingAnchorY)) {
				return false;
			}
			if (this.border != other.border) {
				return false;
			}
			if (!Objects.equals(this.borderColor, other.borderColor)) {
				return false;
			}
			if (this.fadeInLength != other.fadeInLength) {
				return false;
			}
			if (this.fadeOutLength != other.fadeOutLength) {
				return false;
			}
                        if (this.fadeInOffset != other.fadeInOffset) {
				return false;
			}
			if (this.fadeOutOffset != other.fadeOutOffset) {
				return false;
			}
                        if (!Objects.equals(this.fadeColor, other.fadeColor)) {
				return false;
			}
                        if (Float.floatToIntBits(this.fadeAlpha) != Float.floatToIntBits(other.fadeAlpha)) {
				return false;
			}
                        if (this.flipX != other.flipX) {
				return false;
			}
                        if (this.flipY != other.flipY) {
				return false;
			}
			return true;
		}
		
	}
	
	@Element
	private float aspect;
	@Element
	private boolean playAudio;
	
	@Element 
	private CopyableSettings settings;
	
	private boolean scalingChanged = true;
	private Matrix4f scaleMatrix = new Matrix4f();
	
	public static final String PROP_SETTINGS = "settings";
	public static final String PROP_X = "x";
	public static final String PROP_Y = "y";
	public static final String PROP_Z = "z";
	public static final String PROP_WIDTH = "width";
	public static final String PROP_HEIGHT = "height";
	public static final String PROP_KEEP_ASPECT_RATIO = "aspect";
	public static final String PROP_PLAY_AUDIO = "playAudio";
	public static final String PROP_CROP_LEFT = "cropLeft";
	public static final String PROP_CROP_RIGHT = "cropRight";
	public static final String PROP_CROP_TOP = "cropTop";
	public static final String PROP_CROP_BOTTOM = "cropBottom";
	public static final String PROP_SCALING_FACTOR = "scalingFactor";
	public static final String PROP_SCALING_ANCHOR_X = "scalingAnchorX";
	public static final String PROP_SCALING_ANCHOR_Y = "scalingAnchorY";
	public static final String PROP_BORDER = "border";
	public static final String PROP_BORDER_COLOR = "borderColor";
	public static final String PROP_FADE_IN_LENGTH = "fadeIn";
	public static final String PROP_FADE_OUT_LENGTH = "fadeIn";
        public static final String PROP_FADE_IN_OFFSET = "fadeInOffset";
	public static final String PROP_FADE_OUT_OFFSET = "fadeOutOffset";
        public static final String PROP_FADE_COLOR = "fadeColor";
	public static final String PROP_FADE_ALPHA = "fadeAlpha";
        public static final String PROP_FLIP_X = "flipX";
	public static final String PROP_FLIP_Y = "flipY";
	
	//playback
	private boolean boundsChanged;
	private Geometry imageGeom;
	private AudioNode audioNode;
	
	/**
	 * Serialization only
	 */
	public ImageTimelineObject() {
	}

	public ImageTimelineObject(T resource) {
		super(resource);
		settings = new CopyableSettings();
		if (resource instanceof Resource.AudioProvider) {
			playAudio = true;
		}
	}

	@Override
	public Node initPlayer(Player player) {
		Node node = super.initPlayer(player);
		imageGeom = createImageDisplay(resource);
		node.attachChild(imageGeom);
		boundsChanged = true;
		return node;
	}

	private boolean cachedActive = true;
	@Override
	public void performPreUpdateStep(Player player, PreUpdateStep step) {
		if (imageGeom == null) return; //not initialized yet
		switch (step) {
			case FETCH_CONTENT:
				cachedActive = isActive(player.getTime());
				if (cachedActive) {
					prefetchImageContent(resource, start, enabled);
				}
				break;
			case APPLY_FILTERS:
				if (cachedActive) {
					applyImageFilters(imageGeom, resource, player, player.getTime(), !player.isUseHighResFrames());
				}
				break;
		}
	}
	
	@Override
	protected void update(Player player, FrameTime time, float tpf) {
		updateImage(player, time, tpf);
		updateAudio(player, time, tpf);
	}
	protected void updateImage(Player player, FrameTime time, float tpf) {
		if (!cachedActive) {
			imageGeom.setCullHint(Spatial.CullHint.Always);
			return;
		}
		imageGeom.setCullHint(Spatial.CullHint.Inherit);
		if (boundsChanged) {
			updateImageBounds(imageGeom, settings.x, settings.y, settings.width, settings.height, aspect, settings.keepAspectRatio);
			boundsChanged = false;
		}
		imageGeom.setLocalTranslation(imageGeom.getLocalTranslation().x, imageGeom.getLocalTranslation().y, settings.z);
		//cropping
		imageGeom.getMaterial().setFloat("CropLeft", settings.cropLeft);
		imageGeom.getMaterial().setFloat("CropRight", settings.cropRight);
		imageGeom.getMaterial().setFloat("CropTop", settings.cropTop);
		imageGeom.getMaterial().setFloat("CropBottom", settings.cropBottom);
		//effects
		imageGeom.getMaterial().setFloat("BorderV", settings.border / (imageGeom.getLocalScale().y));
		imageGeom.getMaterial().setFloat("BorderH", settings.border / (imageGeom.getLocalScale().x));
		imageGeom.getMaterial().setColor("BorderColor", 
				new ColorRGBA(settings.borderColor.getRed()/256f, settings.borderColor.getGreen()/256f, settings.borderColor.getBlue()/256f, 1f));
		float alpha = 1;
		if (settings.fadeInLength > 0) {
			alpha = Math.max(0, Math.min(alpha, (time.toFrames() - offset.add(start).toFrames() - settings.fadeInOffset) / (float) settings.fadeInLength));
		}
		if (settings.fadeOutLength > 0) {
			alpha = Math.max(0, Math.min(alpha, (offset.add(duration).toFrames() - settings.fadeOutOffset - time.toFrames()) / (float) settings.fadeOutLength));
		}
		imageGeom.getMaterial().setFloat("BlendAlpha", alpha);
                imageGeom.getMaterial().setColor("BlendColor", 
                        new ColorRGBA(settings.fadeColor.getRed()/256f, settings.fadeColor.getGreen()/256f, settings.fadeColor.getBlue()/256f, settings.fadeAlpha));
                //flip
                imageGeom.getMaterial().setBoolean("FlipX", settings.flipX);
                imageGeom.getMaterial().setBoolean("FlipY", settings.flipY);
		//scaling
		if (scalingChanged) {
			TempVars vars = TempVars.get();
			
			Matrix4f mat1 = vars.tempMat4;
			mat1.set(Matrix4f.IDENTITY);
			scaleMatrix.set(Matrix4f.IDENTITY);
			mat1.setScale(1/settings.scalingFactor, 1/settings.scalingFactor, 1);
			scaleMatrix.setTranslation(settings.scalingAnchorX, settings.scalingAnchorY, 0);
			scaleMatrix.multLocal(mat1);
			mat1.set(Matrix4f.IDENTITY);
			mat1.setTranslation(-settings.scalingAnchorX, -settings.scalingAnchorY, 0);
			scaleMatrix.multLocal(mat1);
			
			vars.release();
			scalingChanged = false;
//			LOG.log(Level.INFO, "Image {0}: scaling changed\n{1}", new Object[]{getResource().getName(), scaleMatrix});
		}
		imageGeom.getMaterial().setMatrix4("TexScaleMatrix", scaleMatrix);
		
	}
	protected void updateAudio(Player player, FrameTime time, float tpf) {
		if (audioNode == null && resource instanceof Resource.AudioProvider) {
			if (((Resource.AudioProvider) resource).isAudioLoaded()) {
				audioNode = createAudioNode((Resource.AudioProvider) resource);
				objectNode.attachChild(audioNode);
			}
		}
		if (audioNode != null) {
			if (playAudio) {
				updateAudioNode(audioNode, player, time);
			} else if (audioNode.getStatus()==AudioSource.Status.Playing) {
				audioNode.stop();
			}
		}
	}

	@Override
	public ResourceTimelineObject clone() {
		ImageTimelineObject obj = (ImageTimelineObject) super.clone();
		obj.scaleMatrix = obj.scaleMatrix.clone();
		obj.settings = obj.settings.clone();
		return obj;
	}
	
	/**
	 * Returns the x position
	 * @return 
	 */
	public int getX() {
		return settings.x;
	}

	public void setXNoUndo(final int newX) {
		final int oldX = this.settings.x;
		this.settings.x = newX;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_X, oldX, newX);
	}
	
	/**
	 * Sets the x position
	 * @param newX 
	 */
	public void setX(final int newX) {
		final int oldX = settings.x;
		settings.x = newX;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_X, oldX, newX);
		if (!Objects.equals(oldX, newX) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.x = oldX;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_X, newX, oldX);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.x = newX;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_Y, oldX, newX);
				}
			
			});
		}
	}
	
	/**
	 * Returns the x position
	 * @return 
	 */
	public int getZ() {
		return settings.z;
	}
	
	/**
	 * Sets the x position
	 * @param newX 
	 */
	public void setZ(final int newZ) {
		final int oldZ = settings.z;
		settings.z = newZ;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_Z, oldZ, newZ);
		if (!Objects.equals(oldZ, newZ) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.z = oldZ;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_Z, newZ, oldZ);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.z = newZ;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_Z, oldZ, newZ);
				}
			
			});
		}
	}
	
	/**
	 * Returns the y position
	 * @return 
	 */
	public int getY() {
		return settings.y;
	}
	
	public void setYNoUndo(final int newY) {
		final int oldY = settings.y;
		settings.y = newY;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_Y, oldY, newY);
	}

	/**
	 * Sets the y position
	 * @param y
	 */
	public void setY(final int newY) {
		final int oldY = settings.y;
		settings.y = newY;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_Y, oldY, newY);
		if (!Objects.equals(oldY, newY) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.y = oldY;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_Y, newY, oldY);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.y = newY;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_Y, oldY, newY);
				}
			
			});
		}
	}
	
	/**
	 * Returns the width
	 * @return 
	 */
	public int getWidth() {
		return settings.width;
	}
	
	public void setWidthNoUndo(final int newWidth) {
		final int oldWidth = settings.width;
		settings.width = newWidth;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_WIDTH, oldWidth, newWidth);
	}

	/**
	 * Sets the width
	 * @param newWidth 
	 */
	public void setWidth(final int newWidth) {
		final int oldWidth = settings.width;
		settings.width = newWidth;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_WIDTH, oldWidth, newWidth);
		if (!Objects.equals(oldWidth, newWidth) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.width = oldWidth;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_WIDTH, newWidth, oldWidth);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.width = newWidth;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_WIDTH, oldWidth, newWidth);
				}
			
			});
		}
	}
	
	/**
	 * Returns the height
	 * @return 
	 */
	public int getHeight() {
		return settings.height;
	}

	public void setHeightNoUndo(final int newHeight) {
		final int oldHeight = settings.height;
		settings.height = newHeight;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_HEIGHT, oldHeight, newHeight);
	}
	
	/**
	 * Sets the height
	 * @param newHeight 
	 */
	public void setHeight(final int newHeight) {
		final int oldHeight = settings.height;
		settings.height = newHeight;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_HEIGHT, oldHeight, newHeight);
		if (!Objects.equals(oldHeight, newHeight) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.height = oldHeight;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_HEIGHT, newHeight, oldHeight);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.height = newHeight;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_HEIGHT, oldHeight, newHeight);
				}
			
			});
		}
	}

	/**
	 * @return {@code true} if the aspect ration should be preserved
	 */
	public boolean isKeepAspectRatio() {
		return settings.keepAspectRatio;
	}

	/**
	 * Sets if the aspect ration should be preserved on resizing
	 * @param newAspect 
	 */
	public void setKeepAspectRatio(final boolean newAspect) {
		final boolean oldAspect = settings.keepAspectRatio;
		settings.keepAspectRatio = newAspect;
		boundsChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_KEEP_ASPECT_RATIO, oldAspect, newAspect);
		if (!Objects.equals(oldAspect, newAspect) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.keepAspectRatio = oldAspect;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_KEEP_ASPECT_RATIO, newAspect, oldAspect);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.keepAspectRatio = newAspect;
					boundsChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_KEEP_ASPECT_RATIO, oldAspect, newAspect);
				}
			
			});
		}
	}

	public float getAspect() {
		return aspect;
	}

	public void setAspect(float apsect) {
		this.aspect = apsect;
	}

	public boolean isPlayAudio() {
		return playAudio;
	}

	public void setPlayAudio(boolean newPlayAudio) {
		if (newPlayAudio && resource!=null && resource instanceof Resource.AudioProvider) {
			((Resource.AudioProvider) resource).requestAudio();
		}
		this.playAudio = newPlayAudio;
		final boolean oldPlayAudio = settings.keepAspectRatio;
		this.playAudio = newPlayAudio;
		propertyChangeSupport.firePropertyChange(PROP_PLAY_AUDIO, oldPlayAudio, newPlayAudio);
		if (!Objects.equals(oldPlayAudio, newPlayAudio) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					playAudio = oldPlayAudio;
					propertyChangeSupport.firePropertyChange(PROP_PLAY_AUDIO, newPlayAudio, oldPlayAudio);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					playAudio = newPlayAudio;
					propertyChangeSupport.firePropertyChange(PROP_PLAY_AUDIO, oldPlayAudio, newPlayAudio);
				}
			
			});
		}
	}
	
	
	public float getCropLeft() {
		return settings.cropLeft;
	}
	public void setCropLeftNoUndo(float crop) {
		final float oldCropLeft = settings.cropLeft;
		if (crop<0 || crop>1) {
			LOG.log(Level.WARNING, "left crop is out of bounds: {0}", crop);
		}
		final float newCropLeft = Math.max(0, Math.min(1, crop));
		settings.cropLeft = newCropLeft;
		propertyChangeSupport.firePropertyChange(PROP_CROP_LEFT, oldCropLeft, newCropLeft);
	}
	public void setCropLeft(float crop) {
		final float oldCropLeft = settings.cropLeft;
		final float newCropLeft = Math.max(0, Math.min(1, crop));
		settings.cropLeft = newCropLeft;
		propertyChangeSupport.firePropertyChange(PROP_CROP_LEFT, oldCropLeft, newCropLeft);
		if (!Objects.equals(oldCropLeft, newCropLeft) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.cropLeft = oldCropLeft;
					propertyChangeSupport.firePropertyChange(PROP_CROP_LEFT, newCropLeft, oldCropLeft);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.cropLeft = newCropLeft;
					propertyChangeSupport.firePropertyChange(PROP_CROP_LEFT, oldCropLeft, newCropLeft);
				}
			
			});
		}
	}
	
	public float getCropRight() {
		return settings.cropRight;
	}
	public void setCropRightNoUndo(float crop) {
		final float oldCropRight = settings.cropRight;
		if (crop<0 || crop>1) {
			LOG.log(Level.WARNING, "right crop is out of bounds: {0}", crop);
		}
		final float newCropRight = Math.max(0, Math.min(1, crop));
		settings.cropRight = newCropRight;
		propertyChangeSupport.firePropertyChange(PROP_CROP_RIGHT, oldCropRight, newCropRight);
	}
	public void setCropRight(float crop) {
		final float oldCropRight = settings.cropRight;
		final float newCropRight = Math.max(0, Math.min(1, crop));
		settings.cropRight = newCropRight;
		propertyChangeSupport.firePropertyChange(PROP_CROP_RIGHT, oldCropRight, newCropRight);
		if (!Objects.equals(oldCropRight, newCropRight) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.cropRight = oldCropRight;
					propertyChangeSupport.firePropertyChange(PROP_CROP_RIGHT, newCropRight, oldCropRight);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.cropRight = newCropRight;
					propertyChangeSupport.firePropertyChange(PROP_CROP_RIGHT, oldCropRight, newCropRight);
				}
			
			});
		}
	}
	
	public float getCropTop() {
		return settings.cropTop;
	}
	public void setCropTopNoUndo(float crop) {
		final float oldCropTop = settings.cropTop;
		if (crop<0 || crop>1) {
			LOG.log(Level.WARNING, "top crop is out of bounds: {0}", crop);
		}
		final float newCropTop = Math.max(0, Math.min(1, crop));
		settings.cropTop = newCropTop;
		propertyChangeSupport.firePropertyChange(PROP_CROP_TOP, oldCropTop, newCropTop);
	}
	public void setCropTop(float crop) {
		final float oldCropTop = settings.cropTop;
		final float newCropTop = Math.max(0, Math.min(1, crop));
		settings.cropTop = newCropTop;
		propertyChangeSupport.firePropertyChange(PROP_CROP_TOP, oldCropTop, newCropTop);
		if (!Objects.equals(oldCropTop, newCropTop) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.cropTop = oldCropTop;
					propertyChangeSupport.firePropertyChange(PROP_CROP_TOP, newCropTop, oldCropTop);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.cropTop = newCropTop;
					propertyChangeSupport.firePropertyChange(PROP_CROP_TOP, oldCropTop, newCropTop);
				}
			
			});
		}
	}
	
	public float getCropBottom() {
		return settings.cropBottom;
	}
	public void setCropBottomNoUndo(float crop) {
		final float oldCropBottom = settings.cropBottom;
		if (crop<0 || crop>1) {
			LOG.log(Level.WARNING, "bottom crop is out of bounds: {0}", crop);
		}
		final float newCropBottom = Math.max(0, Math.min(1, crop));
		settings.cropBottom = newCropBottom;
		propertyChangeSupport.firePropertyChange(PROP_CROP_BOTTOM, oldCropBottom, newCropBottom);
	}
	public void setCropBottom(float crop) {
		final float oldCropBottom = settings.cropBottom;
		final float newCropBottom = Math.max(0, Math.min(1, crop));
		settings.cropBottom = newCropBottom;
		propertyChangeSupport.firePropertyChange(PROP_CROP_BOTTOM, oldCropBottom, newCropBottom);
		if (!Objects.equals(oldCropBottom, newCropBottom) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.cropBottom = oldCropBottom;
					propertyChangeSupport.firePropertyChange(PROP_CROP_BOTTOM, newCropBottom, oldCropBottom);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.cropBottom = newCropBottom;
					propertyChangeSupport.firePropertyChange(PROP_CROP_BOTTOM, oldCropBottom, newCropBottom);
				}
			
			});
		}
	}
	
	public float getScalingFactor() {
		return settings.scalingFactor;
	}
	public void setScalingFactorNoUndo(float value) {
		final float oldValue = settings.scalingFactor;
		final float newValue = Math.max(1, value);
		settings.scalingFactor = newValue;
		scalingChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_SCALING_FACTOR, oldValue, newValue);
	}
	public void fireScalingFactorUndoRedo(float oldValue, float newValue) {
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.scalingFactor = oldValue;
					scalingChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_SCALING_FACTOR, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.scalingFactor = newValue;
					scalingChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_SCALING_FACTOR, oldValue, newValue);
				}
			
			});
		}
	}
	public void setScalingFactor(float value) {
		final float oldValue = settings.scalingFactor;
		final float newValue = Math.max(1, value);
		setScalingFactorNoUndo(value);
		fireScalingFactorUndoRedo(oldValue, newValue);
	}
	
	public float getScalingAnchorX() {
		return settings.scalingAnchorX;
	}
	public void setScalingAnchorXNoUndo(float value) {
		final float oldValue = settings.scalingFactor;
		final float newValue = Math.max(0, Math.min(1, value));
		settings.scalingAnchorX = newValue;
		scalingChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_SCALING_ANCHOR_X, oldValue, newValue);
	}
	public void fireScalingAnchorXUndoRedo(float oldValue, float newValue) {
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.scalingAnchorX = oldValue;
					scalingChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_SCALING_ANCHOR_X, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.scalingAnchorX = newValue;
					scalingChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_SCALING_ANCHOR_X, oldValue, newValue);
				}
			
			});
		}
	}
	public void setScalingAnchorX(float value) {
		final float oldValue = settings.scalingFactor;
		final float newValue = Math.max(0, Math.min(1, value));
		setScalingAnchorXNoUndo(newValue);
		fireScalingAnchorXUndoRedo(oldValue, newValue);
	}
	
	public float getScalingAnchorY() {
		return settings.scalingAnchorY;
	}
	public void setScalingAnchorYNoUndo(float value) {
		final float oldValue = settings.scalingFactor;
		final float newValue = Math.max(0, Math.min(1, value));
		settings.scalingAnchorY = newValue;
		scalingChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_SCALING_ANCHOR_Y, oldValue, newValue);
	}
	public void fireScalingAnchorYUndoRedo(float oldValue, float newValue) {
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.scalingAnchorY = oldValue;
					scalingChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_SCALING_ANCHOR_Y, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.scalingAnchorY = newValue;
					scalingChanged = true;
					propertyChangeSupport.firePropertyChange(PROP_SCALING_ANCHOR_Y, oldValue, newValue);
				}
			
			});
		}
	}
	public void setScalingAnchorY(float value) {
		final float oldValue = settings.scalingFactor;
		final float newValue = Math.max(0, Math.min(1, value));
		setScalingAnchorYNoUndo(value);
		fireScalingAnchorYUndoRedo(oldValue, newValue);
	}

	public int getBorder() {
		return settings.border;
	}
	public void setBorder(int value) {
		final int oldValue = settings.border;
		final int newValue = Math.max(0, value);
		settings.border = newValue;
		propertyChangeSupport.firePropertyChange(PROP_BORDER, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.border = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_BORDER, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.border = newValue;
					propertyChangeSupport.firePropertyChange(PROP_BORDER, oldValue, newValue);
				}
			
			});
		}
	}
	
	public Color getBorderColor() {
		return settings.borderColor;
	}
	public void setBorderColor(Color value) {
		final Color oldValue = settings.borderColor;
		final Color newValue = value;
		settings.borderColor = newValue;
		propertyChangeSupport.firePropertyChange(PROP_BORDER_COLOR, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.borderColor = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_BORDER_COLOR, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.borderColor = newValue;
					propertyChangeSupport.firePropertyChange(PROP_BORDER_COLOR, oldValue, newValue);
				}
			
			});
		}
	}
	
	public int getFadeInLength() {
		return settings.fadeInLength;
	}
	public void setFadeInLength(int value) {
		final int oldValue = settings.fadeInLength;
		final int newValue = Math.max(0, value);
		settings.fadeInLength = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FADE_IN_LENGTH, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.fadeInLength = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_IN_LENGTH, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.fadeInLength = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_IN_LENGTH, oldValue, newValue);
				}
			
			});
		}
	}
	
	public int getFadeOutLength() {
		return settings.fadeOutLength;
	}
	public void setFadeOutLength(int value) {
		final int oldValue = settings.fadeOutLength;
		final int newValue = Math.max(0, value);
		settings.fadeOutLength = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FADE_OUT_LENGTH, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.fadeOutLength = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_OUT_LENGTH, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.fadeOutLength = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_OUT_LENGTH, oldValue, newValue);
				}
			
			});
		}
	}
        
        public int getFadeInOffset() {
		return settings.fadeInOffset;
	}
	public void setFadeInOffset(int value) {
		final int oldValue = settings.fadeInOffset;
		final int newValue = Math.max(0, value);
		settings.fadeInOffset = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FADE_IN_OFFSET, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.fadeInOffset = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_IN_OFFSET, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.fadeInOffset = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_IN_OFFSET, oldValue, newValue);
				}
			
			});
		}
	}
	
	public int getFadeOutOffset() {
		return settings.fadeOutOffset;
	}
	public void setFadeOutOffset(int value) {
		final int oldValue = settings.fadeOutOffset;
		final int newValue = Math.max(0, value);
		settings.fadeOutOffset = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FADE_OUT_OFFSET, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.fadeOutOffset = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_OUT_OFFSET, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.fadeOutOffset = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_OUT_OFFSET, oldValue, newValue);
				}
			
			});
		}
	}
        
        public Color getFadeColor() {
		return settings.fadeColor;
	}
	public void setFadeColor(Color value) {
		final Color oldValue = settings.fadeColor;
		final Color newValue = value;
		settings.fadeColor = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FADE_COLOR, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.fadeColor = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_COLOR, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.fadeColor = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_COLOR, oldValue, newValue);
				}
			
			});
		}
	}
	
        public float getFadeAlpha() {
		return settings.fadeAlpha;
	}
	public void setFadeAlpha(float value) {
		final float oldValue = settings.fadeAlpha;
		final float newValue = Math.max(0, Math.min(value, 1));
		settings.fadeAlpha = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FADE_ALPHA, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.fadeAlpha = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_ALPHA, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.fadeAlpha = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FADE_ALPHA, oldValue, newValue);
				}
			
			});
		}
	}
        
        public boolean getFlipX() {
		return settings.flipX;
	}
	public void setFlipX(boolean value) {
		final boolean oldValue = settings.flipX;
		final boolean newValue = value;
		settings.flipX = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FLIP_X, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.flipX = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FLIP_X, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.flipX = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FLIP_X, oldValue, newValue);
				}
			
			});
		}
	}
        
        public boolean getFlipY() {
		return settings.flipY;
	}
	public void setFlipY(boolean value) {
		final boolean oldValue = settings.flipY;
		final boolean newValue = value;
		settings.flipY = newValue;
		propertyChangeSupport.firePropertyChange(PROP_FLIP_Y, oldValue, newValue);
		if (!Objects.equals(oldValue, newValue) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings.flipY = oldValue;
					propertyChangeSupport.firePropertyChange(PROP_FLIP_Y, newValue, oldValue);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings.flipY = newValue;
					propertyChangeSupport.firePropertyChange(PROP_FLIP_Y, oldValue, newValue);
				}
			
			});
		}
	}
        
	@Override
	public void loadPresetSettings(CopyableSettings s) {
		CopyableSettings newSettings = s.clone();
		CopyableSettings oldSettings = this.settings.clone();
		this.settings = newSettings.clone();
		scalingChanged = true;
		propertyChangeSupport.firePropertyChange(PROP_SETTINGS, oldSettings, newSettings);
		if (undoSupport!=null && !Objects.equals(newSettings, oldSettings)) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					settings = oldSettings.clone();
					propertyChangeSupport.firePropertyChange(PROP_SETTINGS, newSettings, oldSettings);
					scalingChanged = true;
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					settings = newSettings.clone();
					propertyChangeSupport.firePropertyChange(PROP_SETTINGS, oldSettings, newSettings);
					scalingChanged = true;
				}
			
			});
		}
	}

	@Override
	public CopyableSettings savePresetSettings() {
		return settings.clone();
	}

	@Override
	public Class<?> presetClass() {
		return CopyableSettings.class;
	}
}
