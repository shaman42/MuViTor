/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import java.beans.*;

/**
 *
 * @author Sebastian Weiss
 */
public class ImageTimelineObjectBeanInfo extends SimpleBeanInfo {

    // Bean descriptor//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( org.shaman.muvitor.timeline.ImageTimelineObject.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor
    // Here you can add code for customizing the BeanDescriptor.

        return beanDescriptor;     }//GEN-LAST:BeanDescriptor


    // Property identifiers//GEN-FIRST:Properties
    private static final int PROPERTY_border = 0;
    private static final int PROPERTY_borderColor = 1;
    private static final int PROPERTY_cropBottom = 2;
    private static final int PROPERTY_cropLeft = 3;
    private static final int PROPERTY_cropRight = 4;
    private static final int PROPERTY_cropTop = 5;
    private static final int PROPERTY_duration = 6;
    private static final int PROPERTY_enabled = 7;
    private static final int PROPERTY_fadeAlpha = 8;
    private static final int PROPERTY_fadeColor = 9;
    private static final int PROPERTY_fadeInLength = 10;
    private static final int PROPERTY_fadeInOffset = 11;
    private static final int PROPERTY_fadeOutLength = 12;
    private static final int PROPERTY_fadeOutOffset = 13;
    private static final int PROPERTY_flipX = 14;
    private static final int PROPERTY_flipY = 15;
    private static final int PROPERTY_height = 16;
    private static final int PROPERTY_keepAspectRatio = 17;
    private static final int PROPERTY_lockOffset = 18;
    private static final int PROPERTY_offset = 19;
    private static final int PROPERTY_playAudio = 20;
    private static final int PROPERTY_resource = 21;
    private static final int PROPERTY_scalingAnchorX = 22;
    private static final int PROPERTY_scalingAnchorY = 23;
    private static final int PROPERTY_scalingFactor = 24;
    private static final int PROPERTY_start = 25;
    private static final int PROPERTY_width = 26;
    private static final int PROPERTY_x = 27;
    private static final int PROPERTY_y = 28;
    private static final int PROPERTY_z = 29;

    // Property array 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[30];
    
        try {
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getBorder", "setBorder" ); // NOI18N
            properties[PROPERTY_border].setShortDescription ( "An extra border in pixels" );
            properties[PROPERTY_borderColor] = new PropertyDescriptor ( "borderColor", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getBorderColor", "setBorderColor" ); // NOI18N
            properties[PROPERTY_cropBottom] = new PropertyDescriptor ( "cropBottom", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getCropBottom", "setCropBottom" ); // NOI18N
            properties[PROPERTY_cropLeft] = new PropertyDescriptor ( "cropLeft", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getCropLeft", "setCropLeft" ); // NOI18N
            properties[PROPERTY_cropRight] = new PropertyDescriptor ( "cropRight", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getCropRight", "setCropRight" ); // NOI18N
            properties[PROPERTY_cropTop] = new PropertyDescriptor ( "cropTop", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getCropTop", "setCropTop" ); // NOI18N
            properties[PROPERTY_duration] = new PropertyDescriptor ( "duration", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getDuration", "setDuration" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", org.shaman.muvitor.timeline.ImageTimelineObject.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_fadeAlpha] = new PropertyDescriptor ( "fadeAlpha", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFadeAlpha", "setFadeAlpha" ); // NOI18N
            properties[PROPERTY_fadeColor] = new PropertyDescriptor ( "fadeColor", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFadeColor", "setFadeColor" ); // NOI18N
            properties[PROPERTY_fadeInLength] = new PropertyDescriptor ( "fadeInLength", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFadeInLength", "setFadeInLength" ); // NOI18N
            properties[PROPERTY_fadeInOffset] = new PropertyDescriptor ( "fadeInOffset", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFadeInOffset", "setFadeInOffset" ); // NOI18N
            properties[PROPERTY_fadeOutLength] = new PropertyDescriptor ( "fadeOutLength", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFadeOutLength", "setFadeOutLength" ); // NOI18N
            properties[PROPERTY_fadeOutOffset] = new PropertyDescriptor ( "fadeOutOffset", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFadeOutOffset", "setFadeOutOffset" ); // NOI18N
            properties[PROPERTY_flipX] = new PropertyDescriptor ( "flipX", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFlipX", "setFlipX" ); // NOI18N
            properties[PROPERTY_flipY] = new PropertyDescriptor ( "flipY", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getFlipY", "setFlipY" ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getHeight", "setHeight" ); // NOI18N
            properties[PROPERTY_keepAspectRatio] = new PropertyDescriptor ( "keepAspectRatio", org.shaman.muvitor.timeline.ImageTimelineObject.class, "isKeepAspectRatio", "setKeepAspectRatio" ); // NOI18N
            properties[PROPERTY_lockOffset] = new PropertyDescriptor ( "lockOffset", org.shaman.muvitor.timeline.ImageTimelineObject.class, "isLockOffset", "setLockOffset" ); // NOI18N
            properties[PROPERTY_offset] = new PropertyDescriptor ( "offset", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getOffset", "setOffset" ); // NOI18N
            properties[PROPERTY_playAudio] = new PropertyDescriptor ( "playAudio", org.shaman.muvitor.timeline.ImageTimelineObject.class, "isPlayAudio", "setPlayAudio" ); // NOI18N
            properties[PROPERTY_resource] = new PropertyDescriptor ( "resource", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getResource", "setResource" ); // NOI18N
            properties[PROPERTY_scalingAnchorX] = new PropertyDescriptor ( "scalingAnchorX", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getScalingAnchorX", "setScalingAnchorX" ); // NOI18N
            properties[PROPERTY_scalingAnchorY] = new PropertyDescriptor ( "scalingAnchorY", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getScalingAnchorY", "setScalingAnchorY" ); // NOI18N
            properties[PROPERTY_scalingFactor] = new PropertyDescriptor ( "scalingFactor", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getScalingFactor", "setScalingFactor" ); // NOI18N
            properties[PROPERTY_start] = new PropertyDescriptor ( "start", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getStart", "setStart" ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getWidth", "setWidth" ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getX", "setX" ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getY", "setY" ); // NOI18N
            properties[PROPERTY_z] = new PropertyDescriptor ( "z", org.shaman.muvitor.timeline.ImageTimelineObject.class, "getZ", "setZ" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties
    // Here you can add code for customizing the properties array.

        return properties;     }//GEN-LAST:Properties

    // EventSet identifiers//GEN-FIRST:Events
    private static final int EVENT_propertyChangeListener = 0;

    // EventSet array
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[1];
    
        try {
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( org.shaman.muvitor.timeline.ImageTimelineObject.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events
    // Here you can add code for customizing the event sets array.

        return eventSets;     }//GEN-LAST:Events

    // Method identifiers//GEN-FIRST:Methods

    // Method array 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[0];//GEN-HEADEREND:Methods
    // Here you can add code for customizing the methods array.

        return methods;     }//GEN-LAST:Methods

    private static java.awt.Image iconColor16 = null;//GEN-BEGIN:IconsDef
    private static java.awt.Image iconColor32 = null;
    private static java.awt.Image iconMono16 = null;
    private static java.awt.Image iconMono32 = null;//GEN-END:IconsDef
    private static String iconNameC16 = null;//GEN-BEGIN:Icons
    private static String iconNameC32 = null;
    private static String iconNameM16 = null;
    private static String iconNameM32 = null;//GEN-END:Icons

    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx


//GEN-FIRST:Superclass
    // Here you can add code for customizing the Superclass BeanInfo.

//GEN-LAST:Superclass
	/**
	 * Gets the bean's <code>BeanDescriptor</code>s.
	 *
	 * @return BeanDescriptor describing the editable properties of this bean.
	 * May return null if the information should be obtained by automatic
	 * analysis.
	 */
	@Override
	public BeanDescriptor getBeanDescriptor() {
		return getBdescriptor();
	}

	/**
	 * Gets the bean's <code>PropertyDescriptor</code>s.
	 *
	 * @return An array of PropertyDescriptors describing the editable
	 * properties supported by this bean. May return null if the information
	 * should be obtained by automatic analysis.
	 * <p>
	 * If a property is indexed, then its entry in the result array will belong
	 * to the IndexedPropertyDescriptor subclass of PropertyDescriptor. A client
	 * of getPropertyDescriptors can use "instanceof" to check if a given
	 * PropertyDescriptor is an IndexedPropertyDescriptor.
	 */
	@Override
	public PropertyDescriptor[] getPropertyDescriptors() {
		return getPdescriptor();
	}

	/**
	 * Gets the bean's <code>EventSetDescriptor</code>s.
	 *
	 * @return An array of EventSetDescriptors describing the kinds of events
	 * fired by this bean. May return null if the information should be obtained
	 * by automatic analysis.
	 */
	@Override
	public EventSetDescriptor[] getEventSetDescriptors() {
		return getEdescriptor();
	}

	/**
	 * Gets the bean's <code>MethodDescriptor</code>s.
	 *
	 * @return An array of MethodDescriptors describing the methods implemented
	 * by this bean. May return null if the information should be obtained by
	 * automatic analysis.
	 */
	@Override
	public MethodDescriptor[] getMethodDescriptors() {
		return getMdescriptor();
	}

	/**
	 * A bean may have a "default" property that is the property that will
	 * mostly commonly be initially chosen for update by human's who are
	 * customizing the bean.
	 *
	 * @return Index of default property in the PropertyDescriptor array
	 * returned by getPropertyDescriptors.
	 * <P>
	 * Returns -1 if there is no default property.
	 */
	@Override
	public int getDefaultPropertyIndex() {
		return defaultPropertyIndex;
	}

	/**
	 * A bean may have a "default" event that is the event that will mostly
	 * commonly be used by human's when using the bean.
	 *
	 * @return Index of default event in the EventSetDescriptor array returned
	 * by getEventSetDescriptors.
	 * <P>
	 * Returns -1 if there is no default event.
	 */
	@Override
	public int getDefaultEventIndex() {
		return defaultEventIndex;
	}

	/**
	 * This method returns an image object that can be used to represent the
	 * bean in toolboxes, toolbars, etc. Icon images will typically be GIFs, but
	 * may in future include other formats.
	 * <p>
	 * Beans aren't required to provide icons and may return null from this
	 * method.
	 * <p>
	 * There are four possible flavors of icons (16x16 color, 32x32 color, 16x16
	 * mono, 32x32 mono). If a bean choses to only support a single icon we
	 * recommend supporting 16x16 color.
	 * <p>
	 * We recommend that icons have a "transparent" background so they can be
	 * rendered onto an existing background.
	 *
	 * @param iconKind The kind of icon requested. This should be one of the
	 * constant values ICON_COLOR_16x16, ICON_COLOR_32x32, ICON_MONO_16x16, or
	 * ICON_MONO_32x32.
	 * @return An image object representing the requested icon. May return null
	 * if no suitable icon is available.
	 */
	@Override
	public java.awt.Image getIcon(int iconKind) {
		switch (iconKind) {
			case ICON_COLOR_16x16:
				if (iconNameC16 == null) {
					return null;
				} else {
					if (iconColor16 == null) {
						iconColor16 = loadImage(iconNameC16);
					}
					return iconColor16;
				}
			case ICON_COLOR_32x32:
				if (iconNameC32 == null) {
					return null;
				} else {
					if (iconColor32 == null) {
						iconColor32 = loadImage(iconNameC32);
					}
					return iconColor32;
				}
			case ICON_MONO_16x16:
				if (iconNameM16 == null) {
					return null;
				} else {
					if (iconMono16 == null) {
						iconMono16 = loadImage(iconNameM16);
					}
					return iconMono16;
				}
			case ICON_MONO_32x32:
				if (iconNameM32 == null) {
					return null;
				} else {
					if (iconMono32 == null) {
						iconMono32 = loadImage(iconNameM32);
					}
					return iconMono32;
				}
			default:
				return null;
		}
	}
	
}
