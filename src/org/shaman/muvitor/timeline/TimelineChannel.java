/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.timeline;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Objects;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.Listenable;
import org.shaman.muvitor.Project;
import org.shaman.muvitor.player.Player;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;

/**
 * All timeline channels are executed in parallel, each contain multiple timeline objects
 * that are executed sequentially.
 * @author Sebastian
 */
public class TimelineChannel implements Listenable {

	/**
	 * The display state of this channel
	 */
	public static enum State {
		/**
		 * switched on.
		 */
		ON,
		/**
		 * switched off / hidden
		 */
		OFF,
		/**
		 * Solo: only channels with 'solo' are visible
		 */
		SOLO
	}
	
	/**
	 * Specifies what should be drawn in the timeline.
	 */
	public static enum DisplayState {
		/**
		 * The resource name
		 */
		TEXT,
		/**
		 * The audio waveform if available
		 */
		AUDIO,
		/**
		 * video frames if available
		 */
		VIDEO
	}
	
	@Element
	private State state = State.ON;
	public static final String PROP_STATE = "state";
	private boolean visible;
	
	@Element
	private DisplayState displayState = DisplayState.TEXT;
	public static final String PROP_DISPLAY_STATE = "displayState";
	
	@ElementList
	private ArrayList<ResourceTimelineObject> objects;
	public static final String PROP_OBJECTS = "objx";
	public static final String PROP_OBJECT_ADDED = "obj+";
	public static final String PROP_OBJECT_REMOVED = "obj-";
	public static final String PROP_OBJECT_EDITED = "objEdit";
	
	private Project project;
	
	private final PropertyChangeSupport propertyChangeSupport;
	private UndoableEditSupport undoSupport;
	
	//for playback
	private Player player;
	private Node channelNode;

	public TimelineChannel() {
		objects = new ArrayList<>();
		propertyChangeSupport = new PropertyChangeSupport(this);
	}
	
	public void init(Project project, UndoableEditSupport undoSupport) {
		this.project = project;
		this.undoSupport = undoSupport;
		for (ResourceTimelineObject r : objects) {
			r.init(undoSupport, this);
		}
		updateState();
	}
	
	public Node initPlayer(Player player) {
		this.player = player;
		this.channelNode = new Node("channel");
		for (ResourceTimelineObject obj : objects) {
			channelNode.attachChild(obj.initPlayer(player));
		}
		updateStateInternal();
		return channelNode;
	}
	public Node getChannelNode() {
		return channelNode;
	}
	
	@Override
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.addPropertyChangeListener(listener);
	}
	
	@Override
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		propertyChangeSupport.removePropertyChangeListener(listener);
	}

	/**
	 * @return the enclosing project
	 */
	public Project getProject() {
		return project;
	}

	public State getState() {
		return state;
	}

	/**
	 * Returns true if this channel is visible.
	 * This is the case if it set to 'SOLO' or to 'ON' and no other channel is 'SOLO'
	 * @return if it is visible and all children should be displayed
	 */
	public boolean isVisible() {
		return visible;
	}
	
	private boolean hasSoloMembers() {
		if (project == null) return state==State.SOLO;
		for (TimelineChannel c : project.getTimelineChannels()) {
			if (c.getState() == State.SOLO) return true;
		}
		return false;
	}
	
	/**
	 * Triggers an update for all timeline channels
	 */
	private void updateState() {
		if (project != null) {
			for (TimelineChannel c : project.getTimelineChannels()) {
				c.updateStateInternal();
			}
		} else {
			updateStateInternal();
		}
	}
	/**
	 * updates the state for this channel
	 */
	private void updateStateInternal() {
		if (channelNode == null) return;
		switch (state) {
			case OFF: 
				visible = false;
				break;
			case SOLO: 
				visible = true;
				break;
			case ON:
				visible = !hasSoloMembers();
				break;
		}
		if (visible) {
			channelNode.setCullHint(Spatial.CullHint.Inherit);
		} else {
			channelNode.setCullHint(Spatial.CullHint.Always);
		}
	}

	public void setState(final State newState) {
		final State oldState = newState;
		this.state = newState;
		updateState();
		propertyChangeSupport.firePropertyChange(PROP_STATE, oldState, newState);
		if (!Objects.equals(oldState, newState) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					state = oldState;
					updateState();
					propertyChangeSupport.firePropertyChange(PROP_STATE, newState, oldState);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					state = newState;
					updateState();
					propertyChangeSupport.firePropertyChange(PROP_STATE, oldState, newState);
				}
			
			});
		}
	}

	public DisplayState getDisplayState() {
		return displayState;
	}

	public void setDisplayState(final DisplayState newDisplayState) {
		final DisplayState oldDisplayState = this.displayState;
		this.displayState = newDisplayState;
		propertyChangeSupport.firePropertyChange(PROP_DISPLAY_STATE, oldDisplayState, newDisplayState);
		if (!Objects.equals(oldDisplayState, newDisplayState) && undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit() {

				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					displayState = oldDisplayState;
					propertyChangeSupport.firePropertyChange(PROP_DISPLAY_STATE, newDisplayState, oldDisplayState);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					displayState = newDisplayState;
					propertyChangeSupport.firePropertyChange(PROP_DISPLAY_STATE, oldDisplayState, newDisplayState);
				}
			
			});
		}
	}

	public ArrayList<ResourceTimelineObject> getObjects() {
		return objects;
	}

	public void setObjects(ArrayList<ResourceTimelineObject> objects) {
		this.objects = objects;
	}
	
	private void internalAdd(ResourceTimelineObject obj) {
		if (player != null) {
			player.enqueue(() -> {
				channelNode.attachChild(obj.initPlayer(player));
			});
		}
	}
	private void internalRemove(ResourceTimelineObject obj) {
		player.enqueue(() -> {
			channelNode.detachChild(obj.getNode());
		});
	}
	public void addTimelineObject(final ResourceTimelineObject obj) {
		objects.add(obj);
		sortTimelineObjects();
		obj.init(undoSupport, this);
		propertyChangeSupport.firePropertyChange(PROP_OBJECT_ADDED, null, obj);
		propertyChangeSupport.firePropertyChange(PROP_OBJECTS, null, obj);
		if (project != null) project.fireTimelineChannelChanged(TimelineChannel.this);
		internalAdd(obj);
		if (undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit(){
				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					objects.remove(obj);
					propertyChangeSupport.firePropertyChange(PROP_OBJECT_REMOVED, obj, null);
					propertyChangeSupport.firePropertyChange(PROP_OBJECTS, obj, null);
					if (project != null) project.fireTimelineChannelChanged(TimelineChannel.this);
					internalRemove(obj);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					objects.add(obj);
					sortTimelineObjects();
					propertyChangeSupport.firePropertyChange(PROP_OBJECT_ADDED, null, obj);
					propertyChangeSupport.firePropertyChange(PROP_OBJECTS, null, obj);
					if (project != null) project.fireTimelineChannelChanged(TimelineChannel.this);
					internalAdd(obj);
				}
			
			});
		}
	}
	
	/**
	 * Sorts the timeline objects so that they are increasing in time.
	 */
	public void sortTimelineObjects() {
		objects.sort((ResourceTimelineObject r1, ResourceTimelineObject r2) -> {
			FrameTime s1 = r1.getOffset().add(r1.getStart());
			FrameTime s2 = r2.getOffset().add(r2.getStart());
			return s1.compareTo(s2);
		});
	}
	
	public boolean removeTimelineObject(final ResourceTimelineObject obj) {
		final int index = objects.indexOf(obj);
		if (index < 0) return false;
		objects.remove(obj);
		propertyChangeSupport.firePropertyChange(PROP_OBJECT_REMOVED, obj, null);
					propertyChangeSupport.firePropertyChange(PROP_OBJECTS, obj, null);
		if (project != null) project.fireTimelineChannelChanged(TimelineChannel.this);
		internalRemove(obj);
		if (undoSupport != null) {
			undoSupport.postEdit(new AbstractUndoableEdit(){
				@Override
				public void undo() throws CannotUndoException {
					super.undo();
					objects.add(index, obj);
					propertyChangeSupport.firePropertyChange(PROP_OBJECT_ADDED, null, obj);
					propertyChangeSupport.firePropertyChange(PROP_OBJECTS, null, obj);
					if (project != null) project.fireTimelineChannelChanged(TimelineChannel.this);
					internalAdd(obj);
				}

				@Override
				public void redo() throws CannotRedoException {
					super.redo();
					objects.remove(obj);
					propertyChangeSupport.firePropertyChange(PROP_OBJECT_REMOVED, obj, null);
					propertyChangeSupport.firePropertyChange(PROP_OBJECTS, obj, null);
					if (project != null) project.fireTimelineChannelChanged(TimelineChannel.this);
					internalRemove(obj);
				}
			
			});
		}
		return true;
	}
	
	public void fireTimelineObjectChanged(ResourceTimelineObject obj) {
		propertyChangeSupport.firePropertyChange(PROP_OBJECT_EDITED, null, obj);
		propertyChangeSupport.firePropertyChange(PROP_OBJECTS, null, obj);
		if (project != null) project.fireTimelineChannelChanged(TimelineChannel.this);
	}
	
	/**
	 * Computes the total length of this channel
	 * @return the total length
	 */
	public FrameTime computeTotalLength() {
		FrameTime length = new FrameTime(project.getFramerate());
		for (TimelineObject obj : objects) {
			length = length.max(obj.getOffset().add(obj.getDuration()));
		}
		return length;
	}
}
