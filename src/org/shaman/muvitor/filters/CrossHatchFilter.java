/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.filters;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.texture.Texture2D;
import java.awt.Image;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.undo.UndoableEditSupport;
import org.openide.util.lookup.ServiceProvider;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.resources.Resource;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineObject;
import org.simpleframework.xml.Element;

@Deprecated
public class CrossHatchFilter extends AbstractFilter {
	
	private static final Image ICON;
	static {
		try {
			ICON = ImageIO.read(CrossHatchFilter.class.getResource("empty.png"));
		} catch (IOException ex) {
			throw new Error("unable to load icon", ex);
		}
	}

	private Material material;
	
	private @Element ColorRGBA lineColor = ColorRGBA.Black.clone();
    private @Element ColorRGBA paperColor = ColorRGBA.White.clone();
    private @Element float colorInfluenceLine = 0.8f;
    private @Element float colorInfluencePaper = 0.1f;
    private @Element float fillValue = 0.9f;
    private @Element float luminance1 = 0.9f;
    private @Element float luminance2 = 0.7f;
    private @Element float luminance3 = 0.5f;
    private @Element float luminance4 = 0.3f;
    private @Element float luminance5 = 0.0f;
    private @Element float lineThickness = 1.0f;
    private @Element float lineDistance = 4.0f;

	public CrossHatchFilter() {
	}

	@Override
	public void initialize(TimelineObject parent, Player player, UndoableEditSupport undoSupport) {
		super.initialize(parent, player, undoSupport);
		
		material = new Material(assetManager, "org/shaman/muvitor/filters/CrossHatch.j3md");
        material.setColor("LineColor", lineColor);
        material.setColor("PaperColor", paperColor);

        material.setFloat("ColorInfluenceLine", colorInfluenceLine);
        material.setFloat("ColorInfluencePaper", colorInfluencePaper);

        material.setFloat("FillValue", fillValue);

        material.setFloat("Luminance1", luminance1);
        material.setFloat("Luminance2", luminance2);
        material.setFloat("Luminance3", luminance3);
        material.setFloat("Luminance4", luminance4);
        material.setFloat("Luminance5", luminance5);

        material.setFloat("LineThickness", lineThickness);
        material.setFloat("LineDistance", lineDistance);
	}
	
	@Override
	public Filter cloneForParent(TimelineObject parent) {
		CrossHatchFilter f = new CrossHatchFilter();
		f.colorInfluenceLine = colorInfluenceLine;
		f.colorInfluencePaper = colorInfluencePaper;
		f.fillValue = fillValue;
		f.lineColor = lineColor.clone();
		f.lineDistance = lineDistance;
		f.lineThickness = lineThickness;
		f.luminance1 = luminance1;
		f.luminance2 = luminance2;
		f.luminance3 = luminance3;
		f.luminance4 = luminance4;
		f.luminance5 = luminance5;
		f.paperColor = paperColor.clone();
		return f;
	}

	@Override
	public List<? extends PreFilterPass> getPreFilterPasses(FrameTime time) {
		return Collections.singletonList(new PreFilterPass() {

			@Override
			public Material getMaterial(Texture2D input) {
				material.setTexture("Texture", input);
				return material;
			}
		});
	}

	@Override
	public Image getIcon() {
		return ICON;
	}

	@Override
	public Object savePresetSettings() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void loadPresetSettings(Object settings) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Class presetClass() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@ServiceProvider(service = FilterFactory.class)
	public static class Factory implements FilterFactory {

		@Override
		public String getName() {
			return "Image/Cross Hatch";
		}

		@Override
		public boolean isApplicable(ResourceTimelineObject<Resource> obj) {
			return obj instanceof ImageTimelineObject;
		}

		@Override
		public Filter createFilter(ResourceTimelineObject<Resource> obj) {
			return new CrossHatchFilter();
		}

		@Override
		public Image getIcon() {
			return ICON;
		}
		
	}
}
