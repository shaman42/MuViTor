/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.filters;

import java.awt.Image;
import org.shaman.muvitor.resources.Resource;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineObject;

/**
 * factory for filters
 * @author Sebastian Weiss
 */
public interface FilterFactory {
	/**
	 * The name of the filter, separate paths by '/'.
	 * @return 
	 */
	String getName();
	
	/**
	 * Checks if the filter can be applied on the specified object
	 * @param obj the object
	 * @return {@code true} if applicable and it should show up in the UI
	 */
	boolean isApplicable(ResourceTimelineObject<Resource> obj);
	
	/**
	 * Creates the filter and sets the parent to the specified object
	 * @param obj the parent resource object
	 * @return the filter
	 */
	Filter createFilter(ResourceTimelineObject<Resource> obj);
	
	/**
	 * A 24x24 sized icon
	 * @return 
	 */
	Image getIcon();
}
