/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.filters;

import com.jme3.material.Material;
import com.jme3.texture.Texture2D;
import java.awt.Image;
import java.util.List;
import javax.swing.undo.UndoableEditSupport;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.timeline.TimelineObject;

/**
 * Interface for pre- and post-filters.
 * Pre-filters are applied before the image is sent to scaling, cropping,
 * and placing into the final image.
 * Post-filters modify the color after scaling.
 * @author Sebastian Weiss
 */
public interface Filter {
	
	/**
	 * Initializes this filter
	 * @param parent
	 * @param player 
	 * @param undoSupport 
	 */
	void initialize(TimelineObject parent, Player player, UndoableEditSupport undoSupport);
	
	/**
	 * Clones this filter for the new parent object
	 * @param parent the new parent
	 * @return a deep clone of this filter
	 */
	Filter cloneForParent(TimelineObject parent);
	
	/**
	 * A single pre-filter pass.
	 * Multiple passes can be chained in one single filter.
	 */
	static interface PreFilterPass {
		Material getMaterial(Texture2D input);
	}
	
	/**
	 * Returns the filter passes that are executed to preprocess the current
	 * frame before it is drawn on the screen.
	 * @param time the current time
	 * @return a list of the filter passes
	 */
	List<? extends PreFilterPass> getPreFilterPasses(FrameTime time);
	
	/**
	 * The Icon of the filter of size 24x24.
	 * @return the icon
	 */
	Image getIcon();
	
	/**
	 * @return The parent timeline object
	 */
	TimelineObject getParent();
}
