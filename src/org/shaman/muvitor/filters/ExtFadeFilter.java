/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.filters;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.texture.Texture2D;
import java.awt.Image;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.undo.UndoableEditSupport;
import org.openide.util.lookup.ServiceProvider;
import org.shaman.muvitor.FrameTime;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.resources.Resource;
import org.shaman.muvitor.timeline.ImageTimelineObject;
import org.shaman.muvitor.timeline.ResourceTimelineObject;
import org.shaman.muvitor.timeline.TimelineObject;
import org.simpleframework.xml.Element;

public class ExtFadeFilter extends AbstractFilter<ExtFadeFilter.CopyableSettings> {
	
	private static final Image ICON;
	static {
		try {
			ICON = ImageIO.read(ExtFadeFilter.class.getResource("empty.png"));
		} catch (IOException ex) {
			throw new Error("unable to load icon", ex);
		}
	}

	private Material material;
	private CopyableSettings settings;

	public static class CopyableSettings implements Cloneable
	{
		public @Element ColorRGBA backgroundColor = ColorRGBA.Black.clone();
		public @Element int fadeInOffset = 0;
		public @Element int fadeInLength = 0;

		@Override
		public CopyableSettings clone() {
			try {
				CopyableSettings s = (CopyableSettings) super.clone();
				s.backgroundColor = s.backgroundColor.clone();
				return s;
			} catch (CloneNotSupportedException ex) {
				throw new Error(ex);
			}
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 13 * hash + Objects.hashCode(this.backgroundColor);
			hash = 13 * hash + this.fadeInOffset;
			hash = 13 * hash + this.fadeInLength;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final CopyableSettings other = (CopyableSettings) obj;
			if (this.fadeInOffset != other.fadeInOffset) {
				return false;
			}
			if (this.fadeInLength != other.fadeInLength) {
				return false;
			}
			if (!Objects.equals(this.backgroundColor, other.backgroundColor)) {
				return false;
			}
			return true;
		}
		
	}

	public ExtFadeFilter() {
		settings = new CopyableSettings();
	}

	@Override
	public void initialize(TimelineObject parent, Player player, UndoableEditSupport undoSupport) {
		super.initialize(parent, player, undoSupport);
		
		material = new Material(assetManager, "org/shaman/muvitor/filters/ExtFade.j3md");
        material.setColor("BackgroundColor", settings.backgroundColor);
        material.setFloat("Blending", 0.0f);
	}
	
	
	
	@Override
	public Filter cloneForParent(TimelineObject parent) {
		ExtFadeFilter f = new ExtFadeFilter();
		f.settings = settings.clone();
		return f;
	}
	
	@Override
	public CopyableSettings savePresetSettings() {
		return settings.clone();
	}

	@Override
	public void loadPresetSettings(CopyableSettings settings) {
		this.settings = settings.clone();
	}

	@Override
	public Class<?> presetClass() {
		return CopyableSettings.class;
	}
	

	@Override
	public List<? extends PreFilterPass> getPreFilterPasses(FrameTime time) {
		return Collections.singletonList(new PreFilterPass() {

			@Override
			public Material getMaterial(Texture2D input) {
				material.setTexture("Texture", input);
				return material;
			}
		});
	}

	@Override
	public Image getIcon() {
		return ICON;
	}

	@ServiceProvider(service = FilterFactory.class)
	public static class Factory implements FilterFactory {

		@Override
		public String getName() {
			return "Image/Extended Fade";
		}

		@Override
		public boolean isApplicable(ResourceTimelineObject<Resource> obj) {
			return obj instanceof ImageTimelineObject;
		}

		@Override
		public Filter createFilter(ResourceTimelineObject<Resource> obj) {
			return new ExtFadeFilter();
		}

		@Override
		public Image getIcon() {
			return ICON;
		}
		
	}
}
