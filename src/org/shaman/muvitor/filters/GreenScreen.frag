#import "Common/ShaderLib/GLSLCompat.glsllib"
#import "Common/ShaderLib/MultiSample.glsllib"

uniform COLORTEXTURE m_Texture;
varying vec2 texCoord;
 
uniform float m_KeyCb;
uniform float m_KeyCr;
uniform float m_TolA;
uniform float m_TolB;
uniform vec2 m_PixelSize;
uniform vec4 m_Mask;

float rgb2y(float r, float g, float b) {
	return 0.299 * r + 0.587 * g + 0.114 * b;
}
float rgb2cb(float r, float g, float b) {
	return -0.168736 * r - 0.331264 * g + 0.5 * b;
}
float rgb2cr(float r, float g, float b) {
	return 0.5 * r - 0.418688 * g - 0.081312 * b;
}

float ycc2r(float y, float cb, float cr) {
	return y - 1.21889e-6*cb + 1.402*cr;
}
float ycc2g(float y, float cb, float cr) {
	return y - 0.344136*cb - 0.714136*cr;
}
float ycc2b(float y, float cb, float cr) {
	return y + 1.772 * cb + 4.06298e-7 * cr;
}
 
void main() {
    vec4 texVal = getColor(m_Texture, texCoord);
    float cb = rgb2cb(texVal.r, texVal.g, texVal.b);
	float cr = rgb2cr(texVal.r, texVal.g, texVal.b);
 
	float factor = 0.0;
#ifdef SMOOTH_ALPHA
	// smooth alpha into the unmasked region
	factor += texVal.a * 0.3333;
	factor += getColor(m_Texture, texCoord + vec2(-m_PixelSize.x, -m_PixelSize.y)).a * 0.0556;
	factor += getColor(m_Texture, texCoord + vec2(-m_PixelSize.x, +m_PixelSize.y)).a * 0.0556;
	factor += getColor(m_Texture, texCoord + vec2(+m_PixelSize.x, -m_PixelSize.y)).a * 0.0556;
	factor += getColor(m_Texture, texCoord + vec2(+m_PixelSize.x, +m_PixelSize.y)).a * 0.0556;
	factor += getColor(m_Texture, texCoord + vec2(-m_PixelSize.x, 0)).a * 0.1111;
	factor += getColor(m_Texture, texCoord + vec2(0, -m_PixelSize.y)).a * 0.1111;
	factor += getColor(m_Texture, texCoord + vec2(+m_PixelSize.x, 0)).a * 0.1111;
	factor += getColor(m_Texture, texCoord + vec2(0, +m_PixelSize.y)).a * 0.1111;
	factor = min(factor, texVal.a);
#else
	// compute alpha value from YCbCr difference
	float temp = sqrt((m_KeyCb - cb) * (m_KeyCb - cb) + (m_KeyCr - cr) * (m_KeyCr - cr));
	if (temp < m_TolA) {
		factor = 0.0;
	} else if (temp < m_TolB) {
		factor = ((temp - m_TolA) / (m_TolB - m_TolA));
	} else {
		factor = 1.0;
	}
#ifdef USE_EXTRA_MASK
	if (m_Mask.w > 0.0
		&& (texCoord.x-m_Mask.x)*(texCoord.x-m_Mask.x)/m_Mask.z + (texCoord.y-m_Mask.y)*(texCoord.y-m_Mask.y)/m_Mask.w < 1.0) {
		factor = 1.0;
	}
#endif
#endif

#ifdef DESPILL
	float y2 = rgb2y(texVal.r, texVal.g, texVal.b);
	vec3 maskColor;
	maskColor.r = ycc2r(y2, m_KeyCb, m_KeyCr);
	maskColor.g = ycc2g(y2, m_KeyCb, m_KeyCr);
	maskColor.b = ycc2b(y2, m_KeyCb, m_KeyCr);
	texVal.rgb = texVal.rgb - maskColor.rgb * (1.0-factor);
#endif

	gl_FragColor = vec4(texVal.rgb, factor);
}