/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.filters;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.Renderer;
import com.jme3.texture.FrameBuffer;
import com.jme3.texture.Image;
import com.jme3.texture.Texture2D;
import com.jme3.ui.Picture;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.shaman.muvitor.FrameTime;

/**
 * Executes a filter list in a pre-render step.
 * @author Sebastian Weiss
 */
public class FilterExecutor {

	private static class FramebufferKey {
		private final int width;
		private final int height;
		private final int index;

		private FramebufferKey(int width, int height, int index) {
			this.width = width;
			this.height = height;
			this.index = index;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 59 * hash + this.width;
			hash = 59 * hash + this.height;
			hash = 59 * hash + this.index;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final FramebufferKey other = (FramebufferKey) obj;
			if (this.width != other.width) {
				return false;
			}
			if (this.height != other.height) {
				return false;
			}
			if (this.index != other.index) {
				return false;
			}
			return true;
		}
		
	}
	
	private static class FramebufferPair {
		private final int width;
		private final int height;
		private FrameBuffer[] buffers;
		private Texture2D[] textures;

		private FramebufferPair(int width, int height) {
			this.width = width;
			this.height = height;
			this.textures = new Texture2D[2];
			this.buffers = new FrameBuffer[2];
			for (int i=0; i<2; ++i) {
				textures[i] = new Texture2D(width, height, Image.Format.BGRA8);
				buffers[i] = new FrameBuffer(width, height, 1);
				buffers[i].setColorTexture(textures[i]);
			}
		}
	}
	private final HashMap<FramebufferKey, FramebufferPair> bufferMap;
	private final RenderManager renderManager;
	private final ArrayList<Filter.PreFilterPass> passes;
	private final Camera camera;
	private final Picture fsQuad;
	private int currentIndex = 0;
	private FrameTime currentFrame;
	private boolean enabled;
	
	public FilterExecutor(RenderManager renderManager) {
		this.bufferMap = new HashMap<>();
		this.renderManager = renderManager;
		this.passes = new ArrayList<>();
		this.camera = new Camera(512, 512);
		camera.setViewPort(0, 1, 0, 1);
		camera.setParallelProjection(true);
		fsQuad = new Picture("filter full screen quad");
        fsQuad.setWidth(1);
        fsQuad.setHeight(1);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	private FramebufferPair getBufferPair(int width, int height, int index) {
		FramebufferKey key = new FramebufferKey(width, height, index);
		FramebufferPair pair = bufferMap.get(key);
		if (pair == null) {
			pair = new FramebufferPair(width, height);
			bufferMap.put(key, pair);
		}
		return pair;
	}
	
	public void clearBuffers() {
		bufferMap.clear();
	}
	
	public void newFrame(FrameTime time) {
		currentFrame = time;
		currentIndex = 0;
	}
	
	/**
	 * Executes a filter chain
	 * @param in the input texture
	 * @param filters the filter chain
	 * @return the output texture
	 */
	public Texture2D executeFilters(Texture2D in, List<? extends Filter> filters) {
		if (!isEnabled()) {
			return in;
		}
		//1. flatten list of passes
		passes.clear();
		for (Filter f : filters) {
			passes.addAll(f.getPreFilterPasses(currentFrame));
		}
		//2. early out
		if (passes.isEmpty()) {
			return in;
		}
		//3. process filters
		int i = 0;
		Texture2D input = in;
		FramebufferPair buffers = getBufferPair(in.getImage().getWidth(), in.getImage().getHeight(), currentIndex++);
		Renderer renderer = renderManager.getRenderer();
		camera.resize(in.getImage().getWidth(), in.getImage().getHeight(), false);
		renderManager.setCamera(camera, false);
		renderer.setBackgroundColor(ColorRGBA.BlackNoAlpha);
		
		for (Filter.PreFilterPass pass : passes) {
			FrameBuffer buf = buffers.buffers[i];
			renderer.setFrameBuffer(buf);
			renderer.clearBuffers(true, true, true);
			
			Material mat = pass.getMaterial(input);
			if (mat.getAdditionalRenderState().isDepthWrite()) {
                mat.getAdditionalRenderState().setDepthTest(false);
                mat.getAdditionalRenderState().setDepthWrite(false);
            }
			
			fsQuad.setMaterial(mat);
			fsQuad.updateGeometricState();
			
			renderManager.renderGeometry(fsQuad);
			
			input = buffers.textures[i];
			i = 1 - i;
		}
		
		//done
		return input;
	}
	
}
