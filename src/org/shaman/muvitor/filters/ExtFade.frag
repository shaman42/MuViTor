#import "Common/ShaderLib/GLSLCompat.glsllib"
#import "Common/ShaderLib/MultiSample.glsllib"

uniform COLORTEXTURE m_Texture;
varying vec2 texCoord;
 
uniform vec4 m_BackgroundColor;
uniform float m_Blending;
 
void main() {
    vec4 texVal = getColor(m_Texture, texCoord);
    gl_FragColor = mix(m_BackgroundColor, texVal, m_Blending);
}