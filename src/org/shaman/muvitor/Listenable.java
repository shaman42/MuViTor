/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import java.beans.PropertyChangeListener;

/**
 * The interface for all objects that allow {@link PropertyChangeListener} to
 * be registered.
 * The property panel registers a listener on the focused object if this interface
 * is implemented. This allows the property panel to react on state changes
 * of the focused object
 * @author Sebastian
 */
public interface Listenable {
	
	void addPropertyChangeListener(PropertyChangeListener listener);
	void removePropertyChangeListener(PropertyChangeListener listener);
	
}
