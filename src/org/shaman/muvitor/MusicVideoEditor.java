/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.shaman.muvitor.player.Player;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEdit;
import javax.swing.undo.UndoableEditSupport;
import org.apache.commons.io.FilenameUtils;
import org.jdesktop.swingx.JXMultiSplitPane;
import org.jdesktop.swingx.MultiSplitLayout;
import org.shaman.muvitor.layout.LayoutManagerPanel;
import org.shaman.muvitor.player.VideoTools;
import org.simpleframework.xml.Serializer;

/**
 *
 * @author Sebastian Weiss
 */
public class MusicVideoEditor extends JFrame {

	private static final Logger LOG = Logger.getLogger(MusicVideoEditor.class.getName());
	public static final String TITLE = "Music Video Editor";
	public static final String PROJECT_FILE_EXTENSION = ".xml";
	
	public static MusicVideoEditor MAIN_FRAME;

	private static String SETTINGS_MAIN_FRAME = "MainFrameSettings";
	private static String SETTINGS_TIMELINE_FRAME = "TimelineFrameSettings";
	private static String SETTINGS_SHOW_TIMELINE_FRAME = "ShowTimelineFrame";
	
	//project
	private File projectFile;
	private Project project;
	private boolean changed;
	private Player player;
	
	//UI
	private JToolBar toolBar;
	private JMenuBar menuBar;
	private PropertyPanel propertyPanel;
	private ResourcePanel resourcePanel;
	private TimelinePanel timelinePanel;
	private MainPanel mainPanel;
	private LayoutManagerPanel layoutManagerPanel;
	
	private JXMultiSplitPane sp;
	private JPanel leftPanel;
	private JTabbedPane rightPanel;
	private JPanel centerPanel;
	private JPanel bottomPanel;
	private JFrame timelineFrame;
	private boolean showTimelineInExtraFrame = false;
	
	Action timelineExtraFrameAction;
	private Action newProjectAction;
	private Action loadProjectAction;
	private Action saveProjectAction;
	private Action saveAsProjectAction;
	private Action saveCopyAsProjectAction;
	private Action undoAction;
	private Action redoAction;
	private Action exportProjectAction;
	
	//copy/paste of settings
	private Object currentSettings;
	private Action copySettingsAction;
	private Action pasteSettingsAction;
	
	private UndoManager undoManager;
	private UndoableEditSupport undoSupport;
	
	public MusicVideoEditor() throws HeadlessException {
		setTitle(TITLE);
		MAIN_FRAME = this;
		getContentPane().setLayout(new BorderLayout());
		
		undoManager = new UndoManager();
		undoSupport = new UndoableEditSupport();
		undoSupport.addUndoableEditListener(new UndoableEditListener() {
			@Override
			public void undoableEditHappened(UndoableEditEvent uee) {
				UndoableEdit edit = uee.getEdit();
				undoManager.addEdit(edit);
				refreshUndoRedoUI();
				enableSave();
			}
		});
		
		createToolBarAndMenuBar();
		setJMenuBar(menuBar);
		getContentPane().add(toolBar, BorderLayout.NORTH);
		
		leftPanel = createResourceView();
		JPanel right1Panel = createPropertyView();
		JPanel right2Panel = createLayoutManagerView();
		centerPanel = createMainView();
		bottomPanel = createTimelineView();
		
		rightPanel = new JTabbedPane();
		rightPanel.addTab("Properties", right1Panel);
		rightPanel.addTab("Layout", right2Panel);
		
		showTimelineInExtraFrame = Settings.getBoolean(SETTINGS_SHOW_TIMELINE_FRAME, false);
		setupContentPane();
		
		Selections.getInstance().addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				if (evt.getPropertyName() == Selections.PROP_FOCUSED_OBJECT) {
					checkFocussedObject(evt.getNewValue());
				}
			}
		});
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent we) {
				Settings.flush();
			}
		});
	}
	private void setupContentPane() {
		LOG.info("setup content pane: showTimelineInExtraFrame="+showTimelineInExtraFrame);
		Settings.setBoolean(SETTINGS_SHOW_TIMELINE_FRAME, showTimelineInExtraFrame);
		timelineExtraFrameAction.putValue(Action.SELECTED_KEY, showTimelineInExtraFrame);
		if (sp != null) {
			getContentPane().remove(sp);
		}
		if (showTimelineInExtraFrame) {
			String layoutDef = "(ROW weight=1.0 left (LEAF weight=1.0 name=center) right)";
			sp = new JXMultiSplitPane();
			MultiSplitLayout.Node root = MultiSplitLayout.parseModel(layoutDef);
			sp.getMultiSplitLayout().setModel(root);
			sp.add(leftPanel, "left");
			sp.add(rightPanel, "right");
			sp.add(centerPanel, "center");
			sp.setContinuousLayout(true);
			getContentPane().add(sp, BorderLayout.CENTER);
			pack();
			repaint();
			
			timelineFrame = new JFrame("Timeline");
			timelineFrame.getContentPane().add(bottomPanel);
			timelineFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			timelineFrame.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					showTimelineInExtraFrame = false;
					setupContentPane();
				}
			});
			timelineFrame.pack();
			setupPersistentFrame(timelineFrame, SETTINGS_TIMELINE_FRAME);
			timelineFrame.setVisible(true);
		} else {
			if (timelineFrame != null) {
				timelineFrame.getContentPane().removeAll();
				timelineFrame.dispose();
				timelineFrame = null;
			}
			
			String layoutDef = "(COLUMN (ROW weight=1.0 left (LEAF weight=1.0 name=center) right) bottom)";
			sp = new JXMultiSplitPane();
			MultiSplitLayout.Node root = MultiSplitLayout.parseModel(layoutDef);
			sp.getMultiSplitLayout().setModel(root);
			sp.add(leftPanel, "left");
			sp.add(rightPanel, "right");
			sp.add(centerPanel, "center");
			sp.add(bottomPanel, "bottom");
			sp.setContinuousLayout(true);
			getContentPane().add(sp, BorderLayout.CENTER);
			
			pack();
			repaint();
		}
	}
	private void createToolBarAndMenuBar() {
		toolBar = new JToolBar();
		toolBar.setFloatable(false);
		menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		menuBar.add(fileMenu);
		
		JMenu editMenu = new JMenu("Edit");
		editMenu.setMnemonic(KeyEvent.VK_E);
		menuBar.add(editMenu);
		
		//create actions
		
		ClassLoader cl = MusicVideoEditor.class.getClassLoader();
		newProjectAction = new AbstractAction("New", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/new16.png"))) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				newProject();
			}
		};
		newProjectAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/new24.png")));
		
		loadProjectAction = new AbstractAction("Load", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/open16.png"))) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				loadProject();
			}
		};
		loadProjectAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/open24.png")));
		
		saveProjectAction = new AbstractAction("Save", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/save16.png"))) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				saveProject();
			}
		};
		saveProjectAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/save24.png")));
		saveProjectAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		saveProjectAction.setEnabled(false);
		
		saveAsProjectAction = new AbstractAction("Save As", null) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				saveAs();
			}
		};
		saveAsProjectAction.setEnabled(false);
		
		saveCopyAsProjectAction = new AbstractAction("Save Copy As", null) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				saveCopyAs();
			}
		};
		saveCopyAsProjectAction.setEnabled(false);
		
		undoAction = new AbstractAction("Undo", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/undo16.gif"))) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				undo();
			}
		};
		undoAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/undo24.gif")));
		undoAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
		undoAction.setEnabled(false);
		
		redoAction = new AbstractAction("Redo", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/redo16.gif"))) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				redo();
			}
		};
		redoAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/redo24.gif")));
		redoAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));
		redoAction.setEnabled(false);
		
		exportProjectAction = new AbstractAction("Export", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/export16.png"))) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				exportProject();
			}
		};
		exportProjectAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/export24.png")));
		exportProjectAction.setEnabled(false);
		
		Action exitAction = new AbstractAction("Exit") {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispatchEvent(new WindowEvent(MusicVideoEditor.this, WindowEvent.WINDOW_CLOSING));
			}
		};
		
		timelineExtraFrameAction = new AbstractAction("Show Timeline in extra frame", null) {
			@Override
			public void actionPerformed(ActionEvent ae) {
				boolean selected = (boolean) getValue(Action.SELECTED_KEY);
				MusicVideoEditor.MAIN_FRAME.setShowTimelineInExtraFrame(selected);
			}
		};
		timelineExtraFrameAction.putValue(Action.SELECTED_KEY, isShowTimelineInExtraFrame());
		
		copySettingsAction = new AbstractAction("Copy Settings", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/copy16.png"))) {
			@Override
			public void actionPerformed(ActionEvent e) {
				performCopy();
			}
		};
		copySettingsAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/copy24.png")));
		copySettingsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		copySettingsAction.setEnabled(false);
		
		pasteSettingsAction = new AbstractAction("Copy Settings", new ImageIcon(cl.getResource("org/shaman/muvitor/icons/paste16.png"))) {
			@Override
			public void actionPerformed(ActionEvent e) {
				performPaste();
			}
		};
		pasteSettingsAction.putValue(Action.LARGE_ICON_KEY, new ImageIcon(cl.getResource("org/shaman/muvitor/icons/paste24.png")));
		pasteSettingsAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
		pasteSettingsAction.setEnabled(false);
		
		//menu layout
		fileMenu.add(newProjectAction);
		fileMenu.add(loadProjectAction);
		fileMenu.add(saveProjectAction);
		fileMenu.add(saveAsProjectAction);
		fileMenu.add(saveCopyAsProjectAction);
		fileMenu.addSeparator();
		fileMenu.add(exportProjectAction);
		fileMenu.addSeparator();
		fileMenu.add(exitAction);
		editMenu.add(undoAction);
		editMenu.add(redoAction);
		editMenu.addSeparator();
		editMenu.add(copySettingsAction);
		editMenu.add(pasteSettingsAction);
		
		//toolbar layout
		toolBar.add(newProjectAction);
		toolBar.add(loadProjectAction);
		toolBar.add(saveProjectAction);
		toolBar.addSeparator();
		toolBar.add(copySettingsAction);
		toolBar.add(pasteSettingsAction);
		toolBar.addSeparator();
		toolBar.add(undoAction);
		toolBar.add(redoAction);
		toolBar.addSeparator();
		toolBar.add(exportProjectAction);
	}
	private JPanel createResourceView() {
		resourcePanel = new ResourcePanel();
		resourcePanel.setUndoSupport(undoSupport);
		return resourcePanel;
	}
	private JPanel createPropertyView() {
		propertyPanel = new PropertyPanel();
		propertyPanel.setUndoSupport(undoSupport);
		return propertyPanel;
	}
	private JPanel createMainView() {
		mainPanel = new MainPanel();
		mainPanel.setUndoSupport(undoSupport);
		mainPanel.initMenu(menuBar, toolBar);
		return mainPanel;
	}
	private JPanel createTimelineView() {
		timelinePanel = new TimelinePanel();
		timelinePanel.setUndoSupport(undoSupport);
		timelinePanel.initMenu(menuBar, toolBar);
		return timelinePanel;
	}
	private JPanel createLayoutManagerView() {
		layoutManagerPanel = new LayoutManagerPanel();
		layoutManagerPanel.setUndoSupport(undoSupport);
		layoutManagerPanel.initMenu(menuBar, toolBar);
		return layoutManagerPanel;
	}

	public void setShowTimelineInExtraFrame(boolean showTimelineInExtraFrame) {
		this.showTimelineInExtraFrame = showTimelineInExtraFrame;
		setupContentPane();
	}

	public boolean isShowTimelineInExtraFrame() {
		return showTimelineInExtraFrame;
	}
	
	private void closeProject() {
		//close player
		if (player != null) {
			player.stop();
			player.destroy();
			player = null;
		}
		
		//disable buttons
		saveProjectAction.setEnabled(false);
		exportProjectAction.setEnabled(false);
		
		//clear undo redo
		undoManager.discardAllEdits();
		refreshUndoRedoUI();
		
		LOG.info("old project closed");
	}
	
	private void newProject() {
		LOG.info("create new project");
		project = NewProjectDialog.showDialog(this);
		if (project != null) {
			closeProject();
			LOG.info("new project created");
			project.setVersion(1);
			projectFile =  project.getFolder();
			project.setFolder(project.getFolder().getParentFile());
//			project.setFolder(new File(project.getFolder(), project.getName()));
			saveProjectAction.setEnabled(true);
			exportProjectAction.setEnabled(true);
			projectLoaded();
			changed = true;
			saveProject();
		}
	}
//	private boolean isProjectDir(File dir, boolean dialog) {
//		File f = new File(dir, PROJECT_FILE_NAME);
//		if (f.exists()) {
//			return true;
//		}
//		if (dialog) {
//			JOptionPane.showMessageDialog(this, "The selected folder does not contain a project");
//		}
//		return false;
//	}
	private boolean isProjectFile(File dir) {
		if (!dir.isFile()) return false;
		return dir.getName().endsWith(PROJECT_FILE_EXTENSION);
	}
	private void loadProject() {
		LOG.info("load project");
		JFileChooser fc = new JFileChooser(Settings.getLastDirectory());
		fc.setAcceptAllFileFilterUsed(false);
		fc.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File file) {
				if (file.isDirectory()) return true;
				return file.getName().endsWith(PROJECT_FILE_EXTENSION);
			}

			@Override
			public String getDescription() {
				return "Project (.xml)";
			}
		});
		int ret = fc.showOpenDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			if (!isProjectFile(fc.getSelectedFile())) {
				return;
			}
			Settings.setLastDirectory(fc.getCurrentDirectory());
			loadProject(fc.getSelectedFile());
		}
	}
	private void loadProject(File file) {
		closeProject();
		Serializer serializer = SerializationUtils.createSerializer();
		try {
			projectFile = file;
			project = serializer.read(Project.class, file);
			project.setFolder(file.getParentFile());
			projectLoaded();
			LOG.info("project file loaded");
		} catch (Exception ex) {
			LOG.log(Level.SEVERE, null, ex);
		}
	}
	/**
	 * initialize project
	 */
	private void projectLoaded() {
		project.init(undoSupport);
		setTitle(projectFile.getAbsolutePath() + " - " + TITLE);
		project.setTime(new FrameTime(project.getFramerate()));
		exportProjectAction.setEnabled(true);
		
		//create dialog
		JDialog dialog = new JDialog(MusicVideoEditor.this, "Loading", true);
		JLabel message = new JLabel("Load project");
		message.setPreferredSize(new Dimension(300, 50));
		JProgressBar bar = new JProgressBar(0, 1000);
		dialog.setLayout(new BorderLayout());
		dialog.add(message, BorderLayout.NORTH);
		dialog.add(bar, BorderLayout.CENTER);
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setResizable(false);
		dialog.pack();
		dialog.setLocationRelativeTo(null);
		
		ProjectLoadingWorker worker = new ProjectLoadingWorker(dialog, bar, message);
		worker.execute();
		dialog.setVisible(true);
		
		saveAsProjectAction.setEnabled(true);
		saveCopyAsProjectAction.setEnabled(true);
	}
	private class LoadingItem {
		private float progress;
		private String message;
	}
	private class ProjectLoadingWorker extends SwingWorker<Void, LoadingItem> 
			implements Player.ResourceLoadingCallback {
		private JDialog dialog;
		private JProgressBar bar;
		private JLabel label;

		public ProjectLoadingWorker(JDialog dialog, JProgressBar bar, JLabel label) {
			this.dialog = dialog;
			this.bar = bar;
			this.label = label;
		}
		
		@Override
		protected Void doInBackground() throws Exception {
			//init player
			player = new Player(project, undoSupport);
			player.loadResources(this);
			return null;
		}

		@Override
		protected void done() {
			//set project
			resourcePanel.setProject(project);
			timelinePanel.setProject(project);
			mainPanel.setProject(project);
			propertyPanel.setProject(project);
			layoutManagerPanel.setProject(project);
			//set player
			resourcePanel.setPlayer(player);
			timelinePanel.setPlayer(player);
			mainPanel.setPlayer(player);
			layoutManagerPanel.setPlayer(player);
			layoutManagerPanel.setMainPanel(mainPanel);
			//close dialog
			dialog.setVisible(false);
			dialog.dispose();
		}

		@Override
		public void onMessage(String message) {
			label.setText("<html>"+message.replaceAll("\n", "<br>")+"</html>");
		}

		@Override
		public void onProgress(float progress) {
			bar.setValue((int) (progress*1000));
		}
		
	}
	
	
	private void saveProject() {
		if (saveProject(projectFile)) {
			changed = false;
			saveProjectAction.setEnabled(false);
		}
	}
	private void saveAs() {
		JFileChooser fc = new JFileChooser(Settings.getLastDirectory());
		fc.setAcceptAllFileFilterUsed(false);
		fc.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File file) {
				if (file.isDirectory()) return true;
				return file.getName().endsWith(PROJECT_FILE_EXTENSION);
			}

			@Override
			public String getDescription() {
				return "Project (.xml)";
			}
		});
		fc.setDialogTitle("Save As ..");
		int ret = fc.showSaveDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			File f = fc.getSelectedFile();
			if (!f.getName().endsWith(PROJECT_FILE_EXTENSION)) {
				f = new File(f.getAbsolutePath()+PROJECT_FILE_EXTENSION);
			}
			Settings.setLastDirectory(fc.getCurrentDirectory());
			if (saveProject(f)) {
				projectFile = f;
				setTitle(projectFile.getAbsolutePath() + " - " + TITLE);
				changed = false;
				saveProjectAction.setEnabled(false);
			}
		}
	}
	private void saveCopyAs() {
		JFileChooser fc = new JFileChooser(Settings.getLastDirectory());
		fc.setAcceptAllFileFilterUsed(false);
		fc.setFileFilter(new FileFilter() {
			@Override
			public boolean accept(File file) {
				if (file.isDirectory()) return true;
				return file.getName().endsWith(PROJECT_FILE_EXTENSION);
			}

			@Override
			public String getDescription() {
				return "Project (.xml)";
			}
		});
		fc.setDialogTitle("Save Copy As ..");
		int ret = fc.showSaveDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			File f = fc.getSelectedFile();
			if (!f.getName().endsWith(PROJECT_FILE_EXTENSION)) {
				f = new File(f.getAbsolutePath()+PROJECT_FILE_EXTENSION);
			}
			Settings.setLastDirectory(fc.getCurrentDirectory());
			saveProject(f);
		}
	}
	/**
	 * Trigger this to save backups
	 */
	public void saveBackup() {
		File f = new File(projectFile.getParent(), FilenameUtils.getBaseName(projectFile.getName())+"_bak"+PROJECT_FILE_EXTENSION);
		saveProject(f);
	}
	private boolean saveProject(File file) {
		LOG.info("save project to "+file);
		Serializer serializer = SerializationUtils.createSerializer();
		if (!project.getFolder().exists()) {
			project.getFolder().mkdir();
		}
		try {
			serializer.write(project, file);
			LOG.info("project saved");
			return true;
		} catch (Exception ex) {
			LOG.log(Level.SEVERE, null, ex);
			JOptionPane.showMessageDialog(this, ex.getLocalizedMessage(), "Unable to save project", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}
	private void exportProject() {
		LOG.info("export project");
		//choose file
		JFileChooser fc = new JFileChooser(Settings.get("EXPORT_FOLDER", Settings.getLastDirectory().getAbsolutePath()));
		fc.setAcceptAllFileFilterUsed(true);
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		FileFilter f = new FileNameExtensionFilter("videos", "avi", "mp4", "mpeg", "mkv");
		fc.addChoosableFileFilter(f);
		fc.setFileFilter(f);
		int ret = fc.showSaveDialog(this);
		if (ret == JFileChooser.APPROVE_OPTION) {
			Settings.set("EXPORT_FOLDER", fc.getCurrentDirectory().getAbsolutePath());
			File target = fc.getSelectedFile();
			LOG.log(Level.INFO, "export to {0}", target);
			
			//choose time
			FrameTime times[] = new FrameTime[2];
			boolean ret2 = StartEndDialog.showDialog(this, project, times);
			FrameTime start = times[0];
			FrameTime end = times[1];
			if (!ret2) {
				return;
			}
			LOG.info("start time: "+start+", end time: "+end);
			
			player.export(target, start, end);
		}
	}
	
	private void undo() {
		undoManager.undo();
		refreshUndoRedoUI();
		enableSave();
	}
	private void redo() {
		undoManager.redo();
		refreshUndoRedoUI();
		enableSave();
	}
	private void refreshUndoRedoUI() {
		undoAction.setEnabled(undoManager.canUndo());
		undoAction.putValue(Action.NAME, undoManager.getUndoPresentationName());
		redoAction.setEnabled(undoManager.canRedo());
		redoAction.putValue(Action.NAME, undoManager.getRedoPresentationName());
	}
	private void enableSave() {
		changed = true;
		saveProjectAction.setEnabled(true);
	}

	
	private void performCopy() {
		currentSettings = ((PresetSaveable) Selections.getInstance().getFocussedObject()).savePresetSettings();
		LOG.info("settings copied from "+Selections.getInstance().getFocussedObject());
	}
	@SuppressWarnings("unchecked")
	private void performPaste() {
		((PresetSaveable) Selections.getInstance().getFocussedObject()).loadPresetSettings(currentSettings);
		LOG.info("settings pasted into "+Selections.getInstance().getFocussedObject());
	}
	private void checkFocussedObject(Object o) {
		if (o instanceof PresetSaveable) {
			copySettingsAction.setEnabled(true);
			if (currentSettings == null) {
				pasteSettingsAction.setEnabled(false);
			} else {
				pasteSettingsAction.setEnabled(currentSettings.getClass() == ((PresetSaveable) o).presetClass());
			}
		}
		else {
			copySettingsAction.setEnabled(false);
			pasteSettingsAction.setEnabled(false);
		} 
	}
	
	/**
	 * Creates a frame that with settings that are persistent across launches.
	 * Settings are: x, y, width, height, maximization
	 * @param title
	 * @param settingsPrefix
	 * @return 
	 */
	private static void setupPersistentFrame(JFrame frame, String settingsPrefix) {
		int x = Settings.getInt(settingsPrefix+"_x", -1);
		int y = Settings.getInt(settingsPrefix+"_y", -1);
		int width = Settings.getInt(settingsPrefix+"_w", -1);
		int height = Settings.getInt(settingsPrefix+"_h", -1);
		boolean maximized = Settings.getBoolean(settingsPrefix+"_max", false);
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice[] gs = ge.getScreenDevices();
		
		if (x<0 || y<0 || width<0 || height<0) {
			width = gs[0].getDefaultConfiguration().getBounds().width;
			height = gs[0].getDefaultConfiguration().getBounds().height;
			frame.setLocation(
				((width / 2) - (frame.getSize().width / 2)) + gs[0].getDefaultConfiguration().getBounds().x, 
				((height / 2) - (frame.getSize().height / 2)) + gs[0].getDefaultConfiguration().getBounds().y
			);
		} else {
			frame.setLocation(x, y);
			frame.setSize(width, height);
		}
		
		if (maximized) {
			frame.setExtendedState(frame.getExtendedState() | JFrame.MAXIMIZED_BOTH);
		}
		
		frame.addWindowStateListener(new WindowStateListener() {
			@Override
			public void windowStateChanged(WindowEvent e) {
				int state = e.getNewState();
				if ((state & JFrame.MAXIMIZED_BOTH) == JFrame.MAXIMIZED_BOTH) {
					Settings.setBoolean(settingsPrefix+"_max", true);
				} else {
					Settings.setBoolean(settingsPrefix+"_max", false);
				}
			}
		});
		frame.addComponentListener(new ComponentAdapter() {

			@Override
			public void componentMoved(ComponentEvent e) {
				Settings.setInt(settingsPrefix+"_x", frame.getX());
				Settings.setInt(settingsPrefix+"_y", frame.getY());
			}

			@Override
			public void componentResized(ComponentEvent e) {
				Settings.setInt(settingsPrefix+"_w", frame.getWidth());
				Settings.setInt(settingsPrefix+"_h", frame.getHeight());
			}
			
		});
	}
	
	
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		MusicVideoEditor muvitor = new MusicVideoEditor();
		muvitor.pack();
		setupPersistentFrame(muvitor, SETTINGS_MAIN_FRAME);
		muvitor.setVisible(true);
//		muvitor.setLocationRelativeTo(null);
		VideoTools.checkFfmpeg(muvitor);
		
//		muvitor.loadProject(new File("C:\\Users\\Sebastian\\Documents\\Java\\SimpleVideoEditorProjects\\Project1"));
	}
	
}
