/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.videoio;

import java.awt.image.BufferedImage;

/**
 * An interface to plug in multiple ways on how video frames are acquired.
 * @author Sebastian
 */
public interface VideoFrameProvider {
	
	/**
	 * @return the width of each frame in pixels
	 */
	int getWidth();
	/**
	 * @return the height of each frame in pixels
	 */
	int getHeight();
	
	/**
	 * @return the total count of frames, -1 if unknown
	 */
	int getFrameCount();
	
	/**
	 * @return the index of the current frame
	 */
	int getCurrentFrameIndex();
	
	/**
	 * Returns the current frame as image.
	 * The passed image might be used or a new image might be created.
	 * Either way, don't write into the returned image (apart from passing it
	 * to this method again).
	 * @param toFill a preallocated image, might be null
	 * @return the current frame, {@code null} if the end is reached
	 */
	BufferedImage getCurrentFrame(BufferedImage toFill);
	
	/**
	 * Moves one step further
	 */
	void advance();
	
	/**
	 * Checks if this provider supports seeking.
	 * If yes, this enables {@link #setCurrentFrameIndex(int) }, however, it
	 * does not ensure that it will always succeed.
	 * @return {@code true} iff seeking is enabled
	 */
	boolean supportsSeeking();
	
	/**
	 * Seeks to the specified frame index, if possible.
	 * @param newIndex the new frame index
	 * @return {@code true} if the seeking was successfull
	 */
	boolean setCurrentFrameIndex(int newIndex);
}
