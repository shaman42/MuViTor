
#if defined(DISCARD_ALPHA)
    uniform float m_AlphaDiscardThreshold;
#endif

uniform sampler2D m_ColorMap;
varying vec2 texCoord;

//cropping
uniform float m_CropLeft;
uniform float m_CropRight;
uniform float m_CropTop;
uniform float m_CropBottom;

//effects
uniform float m_BorderV;
uniform float m_BorderH;
uniform vec4 m_BorderColor;
uniform float m_BlendAlpha;
uniform vec4 m_BlendColor;

//flipping
uniform bool m_FlipX;
uniform bool m_FlipY;

//scaling
uniform mat4 m_TexScaleMatrix;

void main(){
    vec2 texCoordFlipped = vec2(
        m_FlipX ? 1-texCoord.x : texCoord.x,
        m_FlipY ? 1-texCoord.y : texCoord.y);

    vec4 color = texture2D(m_ColorMap, (m_TexScaleMatrix * vec4(texCoordFlipped, 0.0, 1.0)).xy);     

	//cropping
	if (texCoord.x < m_CropLeft
		|| texCoord.x > (1-m_CropRight)
		|| texCoord.y < m_CropBottom
		|| texCoord.y > (1-m_CropTop)) {
		discard;
	}

	//border
	if (texCoord.x-m_CropLeft < m_BorderH
		|| 1-texCoord.x-m_CropRight < m_BorderH
		|| texCoord.y-m_CropBottom < m_BorderV
		|| 1-texCoord.y-m_CropTop < m_BorderV) {
		color = m_BorderColor;
	}

	//pre-border alpha blending
        color.rgb = mix(color.rgb, m_BlendColor.rgb, m_BlendColor.a*(1-m_BlendAlpha));
	color.a *= mix(m_BlendAlpha, 1, m_BlendColor.a);

    #if defined(DISCARD_ALPHA)
        if(color.a < m_AlphaDiscardThreshold){
           discard;
        }
    #endif

    gl_FragColor = color;
}