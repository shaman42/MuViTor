/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import org.simpleframework.xml.Element;

/**
 * Represents a time in the project: seconds + frames.
 * Each instance is immutable.
 * @author Sebastian Weiss
 */
public final class FrameTime implements Comparable<FrameTime>, Cloneable {
	
	//persistent variables
	
	/**
	 * The seconds, not final because of serializable
	 */
	@Element
	private int seconds;
	
	/**
	 * The frame within the second, not final because of serializable
	 * Between 0 (inclusive) and framesPerSecond (exclusive) 
	 */
	@Element
	private int frame;
	
	@Element
	private int framesPerSecond;

	@Deprecated //Marker, this constructor should only be used by the serializer
	public FrameTime() {
	}
	public FrameTime(int seconds, int frame, int framesPerSecond) {
		this.seconds = seconds;
		this.frame = frame;
		this.framesPerSecond = framesPerSecond;
	}
	public FrameTime(FrameTime toClone) {
		seconds = toClone.seconds;
		frame = toClone.frame;
		framesPerSecond = toClone.framesPerSecond;
	}
	public FrameTime(int framesPerSecond) {
		this.framesPerSecond = framesPerSecond;
	}
	
	public static FrameTime fromFrames(int frames, int framesPerSecond) {
		return new FrameTime(framesPerSecond).fromFrames(frames);
	}
	
	/**
	 * Creates a new frame time object and sets it
	 * to the closest possible frame time (round down) 
	 * as specified by the milliseconds from the beginning.
	 * @param milliseconds the milliseconds
	 * @param framesPerSecond the project's frames per second
	 * @return a new frame time instance
	 */
	public static FrameTime fromMillis(int milliseconds, int framesPerSecond) {
		return new FrameTime(framesPerSecond).fromMillis(milliseconds);
	}
	
	private void set(FrameTime other) {
		this.seconds = other.seconds;
		this.frame =other.frame;
		this.framesPerSecond = other.framesPerSecond;
	}

	public int getSeconds() {
		return seconds;
	}

	public int getFrame() {
		return frame;
	}

	public int getFramesPerSecond() {
		return framesPerSecond;
	}

	public void setFramesPerSecond(int framesPerSecond) {
		this.framesPerSecond = framesPerSecond;
	}
	
	/**
	 * Returns the second-part of {@link #seconds}, without minutes
	 * @return the seconds
	 */
	public int getSecond() {
		return seconds % 60;
	}
	
	/**
	 * Returns the minute-part of {@link #seconds}
	 * @return 
	 */
	public int getMinute() {
		return seconds / 60;
	}
	
	/**
	 * Tests if the instance is valid: frames is within the allowed range
	 * @return {@code true} iff it is valid
	 */
	public boolean isValid() {
		return (frame >= 0) && (frame < framesPerSecond);
	}

	/**
	 * Converts this frame time into milliseconds
	 * @return the time in milliseconds
	 */
	public int toMillis() {
		return seconds*1000 + frame * 1000 / framesPerSecond;
	}
	
	/**
	 * Sets this instance to the closest possible frame time (round down) 
	 * as specified by the milliseconds from the beginning.
	 * @param millis 
	 */
	private FrameTime fromMillis(int millis) {
		seconds = millis / 1000;
		frame = (millis % 1000) * framesPerSecond / 1000;
		frame = Math.min(frame, framesPerSecond);
		return this;
	}
	
	/**
	 * Converts it into frames from the beginning
	 * @return the frames from beginning
	 */
	public int toFrames() {
		return frame + seconds * framesPerSecond;
	}
	
	/**
	 * Sets this instance to the frame time from number of frames from the beginning.
	 * @param frames the frames
	 * @return this
	 */
	private FrameTime fromFrames(int frames) {
		seconds = frames / framesPerSecond;
		frame = frames % framesPerSecond;
		while (frame < 0) {
			frame += framesPerSecond;
			seconds --;
		}
		return this;
	}
	
	private FrameTime incrementLocal() {
		frame++;
		if (frame >= framesPerSecond) {
			frame = 0;
			seconds++;
		}
		return this;
	}
	public FrameTime increment() {
		return new FrameTime(this).incrementLocal();
	}
	private FrameTime decrementLocal() {
		if (frame==0 && seconds==0) {
			return this;
		}
		frame--;
		if (frame < 0) {
			frame = framesPerSecond - 1;
			seconds--;
		}
		return this;
	}
	public FrameTime decrement() {
		return new FrameTime(this).decrementLocal();
	}
	
	private void checkCompatibility(FrameTime other) {
		if (other.framesPerSecond != this.framesPerSecond) {
			throw new IllegalArgumentException("frames per second do not match: this="+this.framesPerSecond+", other="+other.framesPerSecond);
		}
	}
	
	/**
	 * Adds the specified frame time to this instance
	 * @param other
	 * @return 
	 */
	private FrameTime addLocal(FrameTime other) {
		checkCompatibility(other);
		seconds += other.seconds;
		frame += other.frame;
		if (frame >= framesPerSecond) {
			frame -= framesPerSecond;
			seconds++;
		}
		if (frame < 0) {
			frame += framesPerSecond;
			seconds--;
		}
		return this;
	}
	/**
	 * Adds the specified frame time to this instance and returns the result
	 * as a new instance.
	 * @param other
	 * @return a new frame time with the result
	 */
	public FrameTime add(FrameTime other) {
		return new FrameTime(this).addLocal(other);
	}
	
	/**
	 * Adds the specified frames to this instance and returns the result
	 * as a new instance.
	 * @param frames the number of frames to add
	 * @return 
	 */
	public FrameTime add(int frames) {
		return FrameTime.fromFrames(this.toFrames() + frames, framesPerSecond);
	}
	
	/**
	 * Subtracts the specified frame time to this instance
	 * @param other
	 * @return 
	 */
	private FrameTime subtractLocal(FrameTime other) {
		checkCompatibility(other);
		seconds -= other.seconds;
		frame -= other.frame;
		if (frame < 0) {
			frame += framesPerSecond;
			seconds--;
		}
		if (frame >= framesPerSecond) {
			frame -= framesPerSecond;
			seconds++;
		}
		return this;
	}
	/**
	 * Subtracts the specified frame time to this instance and returns the result as
	 * a new instance.
	 * @param other
	 * @return 
	 */
	public FrameTime subtract(FrameTime other) {
		return new FrameTime(this).subtractLocal(other);
	}
	
	/**
	 * Computes the maximum of this and other and stores it in this.
	 * @param other
	 * @return 
	 */
	private FrameTime maxLocal(FrameTime other) {
		checkCompatibility(other);
		if (other.seconds > this.seconds) {
			this.seconds = other.seconds;
			this.frame = other.frame;
		} else if (this.seconds > other.seconds) {
			//do nothing
		} else { //this.seconds == other.seconds
			this.frame = Math.max(this.frame, other.frame);
		}
		return this;
	}
	/**
	 * Computes the maximum of this and other and stores it in this
	 * and returns the result as a new instance
	 * @param other
	 * @return 
	 */
	public FrameTime max(FrameTime other) {
		return new FrameTime(this).maxLocal(other);
	}
	
	/**
	 * Computes the minimum of this and other and stores it in this.
	 * @param other
	 * @return 
	 */
	private FrameTime minLocal(FrameTime other) {
		checkCompatibility(other);
		if (other.seconds < this.seconds) {
			this.seconds = other.seconds;
			this.frame = other.frame;
		} else if (this.seconds < other.seconds) {
			//do nothing
		} else { //this.seconds == other.seconds
			this.frame = Math.min(this.frame, other.frame);
		}
		return this;
	}
	/**
	 * Computes the minimum of this and other and stores it in this
	 * and returns the result as a new instance
	 * @param other
	 * @return 
	 */
	public FrameTime min(FrameTime other) {
		return new FrameTime(this).minLocal(other);
	}
	
	@Override
	public int hashCode() {
		int hash = 7;
		hash = 71 * hash + this.seconds;
		hash = 71 * hash + this.frame;
		hash = 71 * hash + this.framesPerSecond;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final FrameTime other = (FrameTime) obj;
		if (this.seconds != other.seconds) {
			return false;
		}
		if (this.frame != other.frame) {
			return false;
		}
		if (this.framesPerSecond != other.framesPerSecond) {
			return false;
		}
		return true;
	}

	@Override
	@SuppressWarnings("CloneDoesntCallSuperClone")
	public FrameTime clone() {
		return new FrameTime(this);
	}

	@Override
	public String toString() {
		return getMinute()+","+getSecond()+":"+getFrame();
	}
        
        public static FrameTime fromString(String s, int framesPerSecond)
        {
            int idx1 = s.indexOf(',');
            int idx2 = s.indexOf(':');
            if (idx1==-1 || idx2==-2 || idx1>idx2) 
                throw new IllegalArgumentException("string can't be parsed");
            String min = s.substring(0, idx1);
            String sec = s.substring(idx1+1, idx2);
            String frame = s.substring(idx2+1);
            return new FrameTime(Integer.parseInt(min)*60+Integer.parseInt(sec),
                    Integer.parseInt(frame), framesPerSecond);
        }

	@Override
	public int compareTo(FrameTime o) {
		if (this.seconds < o.seconds) {
			return -1;
		} else if (this.seconds > o.seconds) {
			return 1;
		} else {
			return Integer.compare(frame, o.frame);
		}
	}
	
	public javax.swing.SpinnerModel getSpinnerModel() {
		return new SpinnerModel(this);
	}
	
	public javax.swing.SpinnerModel getSpinnerModel(FrameTime min, FrameTime max) {
		return new SpinnerModel(this, min, max);
	}
	
	private static class SpinnerModel extends javax.swing.AbstractSpinnerModel {

		private FrameTime time;
		private final FrameTime min;
		private final FrameTime max;
		
		public SpinnerModel() {
			min = null;
			max = null;
		}

		public SpinnerModel(FrameTime time) {
			this.time = time.clone();
			min = null;
			max = null;
		}

		public SpinnerModel(FrameTime time, FrameTime min, FrameTime max) {
			this.time = time.clone();
			this.min = min;
			this.max = max;
		}
	
		@Override
		public Object getValue() {
			return time.clone();
		}

		@Override
		public void setValue(Object value) {
			if (!(value instanceof FrameTime)) return;
			time = (FrameTime) value;
			fireStateChanged();
		}

		@Override
		public Object getNextValue() {
			time.incrementLocal();
			fireStateChanged();
			return time.clone();
		}

		@Override
		public Object getPreviousValue() {
			time.decrementLocal();
			fireStateChanged();
			return time.clone();
		}
		
	}
	
	//TODO
	//private class SpinnerEditor extends javax.swing.JSpi
}
