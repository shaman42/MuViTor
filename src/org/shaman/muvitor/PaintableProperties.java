/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import javax.swing.JComponent;

/**
 * If an object implementing this interface becomes focussed
 * (see {@link Selections#setFocussedObject(java.lang.Object) },
 * then the component returned by {@link #getPropertyComponent() }
 * is displayed instead of the property table.
 * @author Sebastian Weiss
 */
public interface PaintableProperties {
	
	/**
	 * Component to display instead of the default property table.
	 * @return the custom component
	 */
	JComponent getPropertyComponent();
	
}
