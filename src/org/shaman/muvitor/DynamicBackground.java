/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;


import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyEditor;
import java.beans.PropertyEditorManager;
import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.Objects;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.simpleframework.xml.*;

/**
 *
 * @author SebastianWeiß
 */
public class DynamicBackground implements Listenable, Cloneable {
    
    @ElementList private ArrayList<FrameTime> times = new ArrayList<>();
    @ElementList private ArrayList<Color> colors = new ArrayList<>();
    private int framerate;

    public DynamicBackground() {
    }

    void setFramerate(int framerate) {
        this.framerate = framerate;
    }
    
    int getFramerate() {
        return framerate;
    }
    
    @Override
    public DynamicBackground clone(){
        DynamicBackground d = new DynamicBackground();
        for (FrameTime ft : times)
            d.times.add(ft.clone());
        for (Color c : colors)
            d.colors.add(new Color(c.getRGB()));
        d.framerate = framerate;
        return d;
    }
    
    @Override
    public String toString() {
        return "Num control points: " + String.valueOf(times.size());
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.times);
        hash = 37 * hash + Objects.hashCode(this.colors);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DynamicBackground other = (DynamicBackground) obj;
        if (!Objects.equals(this.times, other.times)) {
            return false;
        }
        if (!Objects.equals(this.colors, other.colors)) {
            return false;
        }
        return true;
    }
    
    

    public static final String PROP_COLORS = "colors";

    /**
     * Get the value of colors
     *
     * @return the value of colors
     */
    public ArrayList<Color> getColors() {
        return colors;
    }

    /**
     * Set the value of colors
     *
     * @param colors new value of colors
     */
    public void setColors(ArrayList<Color> colors) {
        ArrayList<Color> oldColors = this.colors;
        this.colors = colors;
        propertyChangeSupport.firePropertyChange(PROP_COLORS, oldColors, colors);
    }

    public static final String PROP_TIMES = "times";

    /**
     * Get the value of times
     *
     * @return the value of times
     */
    public ArrayList<FrameTime> getTimes() {
        return times;
    }

    /**
     * Set the value of times
     *
     * @param times new value of times
     */
    public void setTimes(ArrayList<FrameTime> times) {
        ArrayList<FrameTime> oldTimes = this.times;
        this.times = times;
        propertyChangeSupport.firePropertyChange(PROP_TIMES, oldTimes, times);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    /**
     * Add PropertyChangeListener.
     *
     * @param listener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    /**
     * Remove PropertyChangeListener.
     *
     * @param listener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public static void RegisterEditor()
    {
        PropertyEditorManager.registerEditor(DynamicBackground.class, DynamicBackground.Editor.class);
    }
    
    public static class Editor extends PropertyEditorSupport {
        
        @Override
        public Component getCustomEditor() {
            //return m_editor;
            JButton b = new JButton ("Edit");
            b.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DynamicBackground data = (DynamicBackground) Editor.this.getValue();
                    DynamicBackgroundEditDialog d = new DynamicBackgroundEditDialog(null, true, data);
                    d.setVisible(true);
                    if (d.getReturnStatus() == DynamicBackgroundEditDialog.RET_OK)
                        setValue(d.getData());
                }
            });
            return b;
        }

        @Override
        public boolean supportsCustomEditor() {
            return true;
        }
        
    }
}
