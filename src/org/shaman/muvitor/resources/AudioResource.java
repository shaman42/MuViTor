/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.resources;

import com.jme3.audio.AudioBuffer;
import com.jme3.audio.AudioKey;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.simpleframework.xml.Element;

/**
 *
 * @author Sebastian Weiss
 */
public class AudioResource implements Resource, Resource.AudioProvider {
	private static final Logger LOG = Logger.getLogger(AudioResource.class.getName());
	
	@Element
	private String name;
	
	private AudioBuffer audioData;
	private AudioKey audioKey;
	private AudioWaveformCreator.WaveformFile waveform;
	
	/**
	 * The length in frames
	 */
	private int numFrames = -1;

	public AudioResource() {
	}

	public AudioResource(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void load(ResourceLoader loader) {
		if (isLoaded()) {
			return;
		}
		//load sample
		loader.setMessage(name+":\nloading");
		String path = name;
		audioKey = new AudioKey(path, false);
		audioData = (AudioBuffer) loader.getPlayer().getAssetManager().loadAudio(audioKey);
		numFrames = (int) (audioData.getDuration() * loader.getPlayer().getProject().getFramerate());
		// load waveform
		try {
			File waveformFile = new File(loader.getProjectDirectory().getAbsoluteFile() + 
					File.separator + path + AudioWaveformCreator.FILE_SUFFIX);
			try {
				waveform = AudioWaveformCreator.loadWaveform(waveformFile);
			} catch (FileNotFoundException ex) {
				//generate waveform
				waveform = AudioWaveformCreator.createWaveform(audioData, loader.getPlayer().getProject().getFramerate());
				AudioWaveformCreator.saveWaveform(waveformFile, waveform);
			}
		} catch (IOException | ClassNotFoundException ex) {
			LOG.log(Level.SEVERE, "unable to load audio "+name, ex);
			return;
		}
		loader.setMessage(name+":\nloaded");
	}

	@Override
	public void requestAudio() {
		//do nothing, audio is always loaded
	}

	@Override
	public boolean isAudioLoaded() {
		return true;
	}
	
	@Override
	public boolean isLoaded() {
		return audioData != null;
	}

	@Override
	public void unload() {
		audioData = null;
		audioKey = null;
	}

	@Override
	public AudioBuffer getAudioData() {
		return audioData;
	}

	@Override
	public AudioKey getAudioKey() {
		return audioKey;
	}

	@Override
	public AudioWaveformCreator.WaveformFile getWaveform() {
		return waveform;
	}
	
	@Override
	public int getDurationInFrames() {
		return numFrames;
	}
	
	@Override
	public String toString() {
		return new File(getName()).getName();
	}
}
