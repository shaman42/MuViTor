/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.resources;

import com.jme3.asset.AssetManager;
import com.jme3.audio.AudioBuffer;
import com.jme3.audio.AudioKey;
import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.shaman.muvitor.LRUCache;
import org.shaman.muvitor.Project;
import org.shaman.muvitor.player.Player;
import org.shaman.muvitor.player.VideoTools;
import org.shaman.muvitor.videoio.BufferedHumbleVideoFrameProvider;
import org.simpleframework.xml.Element;

/**
 *
 * @author Sebastian Weiss
 */
public class VideoResource implements Resource, Resource.ImageProvider, Resource.AudioProvider {
	private static final Logger LOG = Logger.getLogger(VideoResource.class.getName());
	private static final boolean FLIP_Y = true;
	
	public static class FrameKey {
		private final VideoResource resource;
		private final int frame;
		public FrameKey(VideoResource resource, int frame) {
			this.resource = resource;
			this.frame = frame;
		}
		@Override
		public int hashCode() {
			int hash = 5;
			hash = 89 * hash + Objects.hashCode(this.resource);
			hash = 89 * hash + this.frame;
			return hash;
		}
		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final FrameKey other = (FrameKey) obj;
			if (!Objects.equals(this.resource, other.resource)) {
				return false;
			}
			if (this.frame != other.frame) {
				return false;
			}
			return true;
		}
	}
	public static final int MAX_CACHE_SIZE = 200; //find value that fits into memory
	/**
	 * Cache for thumbnails
	 */
	public static final LRUCache<FrameKey, Texture2D> VIDEO_CACHE = new LRUCache<>(MAX_CACHE_SIZE);

	@Element
	private String name;
	@Element
	private int framerate;
	@Element
	private int width;
	@Element
	private int height;
	@Element
	private String highResFile;

	private Player player;
	private VideoTools videoTools;
	private int numFrames = -1;
	private float thumbnailScale;
	private AudioBuffer audioData;
	private AudioKey audioKey;
	private AudioWaveformCreator.WaveformFile waveform;
	private BufferedHumbleVideoFrameProvider highResFrameProvider;
	private Texture2D highResTexture;
	private int lastHighResFrame = -1;
	private File storageFolder;
	
	/**
	 * A timeline object requested the audio.
	 * This will trigger the loading
	 */
	private boolean audioDataRequested = false;
	
	private final AWTLoader awtLoader = new AWTLoader();
	private class FrameLoader implements LRUCache.Factory<FrameKey, Texture2D> {
		@Override
		public Texture2D create(FrameKey key) {
			assert (key.resource == VideoResource.this);
			assert (videoTools != null);
			try {
				BufferedImage img = videoTools.getThumbnail(key.frame);
				Texture2D tex = new Texture2D(awtLoader.load(img, FLIP_Y));
				return tex;
			} catch (IOException ex) {
				Logger.getLogger(VideoResource.class.getName()).log(Level.SEVERE, "unable to load thumbnail "+key.frame, ex);
				return null;
			}
		}
	}
	private final FrameLoader factory = new FrameLoader();
	
	public VideoResource() {
	}

	public VideoResource(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	public void setFramerate(int framerate) {
		this.framerate = framerate;
	}

	public int getFramerate() {
		return framerate;
	}

	@Override
	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getHighResFile() {
		return highResFile;
	}

	public void setHighResFile(String highResFile) {
		this.highResFile = highResFile;
	}

	@Override
	public void requestAudio() {
		getAudioData();
		//This triggers either loading or sets the audioDataRequested flag
	}
	
	@Override
	public AudioBuffer getAudioData() {
		if (audioData == null) {
			if (player != null) {
				//load it now
				audioData = (AudioBuffer) player.getAssetManager().loadAudio(audioKey);
				LOG.log(Level.INFO, "{0} loaded", audioKey);
			} else {
				audioDataRequested = true;
			}
		}
		return audioData;
	}

	@Override
	public boolean isAudioLoaded() {
		return audioData!=null;
	}

	@Override
	public AudioKey getAudioKey() {
		return audioKey;
	}

	@Override
	public AudioWaveformCreator.WaveformFile getWaveform() {
		return waveform;
	}
	
	/**
	 * Returns the folder where the thumbnails and audio of this resource is
	 * stored. Use this to store additional resource-related data.
	 * @return the storage folder of this video resource.
	 */
	public File getStorageFolder() {
		return storageFolder;
	}
	
	@Override
	public void load(ResourceLoader loader) {
		if (isLoaded()) {
			return;
		}
		try {
			player = loader.getPlayer();
			Project project = player.getProject();
			//path
			loader.setMessage(name+":\nload audio");
			String basePath = name;
			videoTools = new VideoTools(basePath, loader.getProjectDirectory());
			storageFolder = new File(loader.getProjectDirectory().getAbsoluteFile() + 
					File.separator + videoTools.getBasePath());
                        //0. check if thumbnails are available
                        if (!videoTools.hasThumbnails()) {
                            LOG.log(Level.WARNING, "Thumbnails not found for {0}, extract again", name);
                            videoTools.extractThumbnailsAgain(highResFile, loader.getPlayer().getProject().getFramerate());
                        }
			
			//1. create audio key
			audioKey = new AudioKey(videoTools.getAudioPath(), false);
			if (audioDataRequested) {
				//load the audio
				getAudioData();
			} else {
				LOG.log(Level.INFO, "audio loading skipped for {0}", name);
			}
			// load waveform
			File waveformFile = new File(loader.getProjectDirectory().getAbsoluteFile() + 
					File.separator + videoTools.getAudioPath() + AudioWaveformCreator.FILE_SUFFIX);
			try {
				waveform = AudioWaveformCreator.loadWaveform(waveformFile);
			} catch (FileNotFoundException ex) {
				//generate waveform
				audioData = getAudioData();
				waveform = AudioWaveformCreator.createWaveform(audioData, project.getFramerate());
				AudioWaveformCreator.saveWaveform(waveformFile, waveform);
			}
			
			//2. load thumbnails
			loader.setMessage(name+":\nload video");
			numFrames = videoTools.getFrameCount();
			loader.setMessage(name+":\nloaded");
			
			//3. get thumbnail scale
			BufferedImage firstThumbnail = videoTools.getThumbnail(0);
			thumbnailScale = firstThumbnail.getWidth() / width;
			
			LOG.log(Level.INFO, "video loaded");
		} catch (IOException | ClassNotFoundException ex) {
			LOG.log(Level.SEVERE, "unable to load video "+name, ex);
		}
	}

	@Override
	public boolean isLoaded() {
		return numFrames >= 0;
	}

	@Override
	public void unload() {
		audioData = null;
		audioKey = null;
		videoTools = null;
		numFrames = -1;
	}
	
	@Override
	public int getDurationInFrames() {
		return numFrames;
	}

	@Override
	public float getThumbnailScale() {
		return thumbnailScale;
	}
	
	@Override
	public String toString() {
		return new File(getName()).getName();
	}

	@Override
	public Texture2D getFrame(int index, boolean thumbnail) {
		if (thumbnail) {
			FrameKey key = new FrameKey(this, Math.min(index, numFrames-1));
			return VIDEO_CACHE.getCreate(key, factory);
		} else {
			if (index == lastHighResFrame) {
				//nothing to do
				return highResTexture;
			}
			//load high res image
			if (highResFrameProvider == null) {
				try {
					highResFrameProvider = new BufferedHumbleVideoFrameProvider(highResFile);
				} catch (InterruptedException | IOException ex) {
					Logger.getLogger(VideoResource.class.getName()).log(Level.SEVERE, "unable to create high resolution frame provider", ex);
					return null;
				}
			}
			highResFrameProvider.setCurrentFrameIndex(index);
			BufferedImage img = highResFrameProvider.getCurrentFrame(null);
			//convert to texture
			if (FLIP_Y) {
				//clone image first, because flipping modifies the image
				img = BufferedHumbleVideoFrameProvider.copyImage(img);
			}
			if (highResTexture == null) {
				highResTexture = new Texture2D(awtLoader.load(img, FLIP_Y));
			} else {
				highResTexture.setImage(awtLoader.load(img, FLIP_Y));
			}
			lastHighResFrame = index;
			return highResTexture;
		}
	}
	
	@Override
	public void prefetchFrame(int index, boolean thumbnail) {
		//TODO: implement prefetching
	}
}
