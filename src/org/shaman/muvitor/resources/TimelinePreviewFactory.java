/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.resources;

import com.jme3.texture.Texture2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Level;
import java.util.logging.Logger;
import jme3tools.converters.ImageToAwt;
import org.shaman.muvitor.LRUCache;
import org.shaman.muvitor.TimelineDisplay;

/**
 * a factory that provides preview images for audio and video resources in 
 * the timeline.
 * @author Sebastian
 */
public class TimelinePreviewFactory {
	private static final Logger LOG = Logger.getLogger(TimelinePreviewFactory.class.getName());
	
	private final int rowHeight;
	private final int waveformWidth;
	private final PropertyChangeSupport changeSupport;
	/**
	 * An event with this name is fired if a new frame was loaded.
	 * The new value contains the image. But this is only a dummy value
	 * and should trigger a repaint.
	 */
	public static final String PROP_FRAME_LOADED = "frameloaded";
	
	//loading
	private final ExecutorService threadPool;
	private static class ImageKey {
		Resource.ImageProvider resource;
		int frame;
		private ImageKey(Resource.ImageProvider resource, int frame) {
			this.resource = resource;
			this.frame = frame;
		}
		@Override
		public int hashCode() {
			int hash = 7;
			hash = 17 * hash + Objects.hashCode(this.resource);
			hash = 17 * hash + this.frame;
			return hash;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final ImageKey other = (ImageKey) obj;
			if (this.frame != other.frame) {
				return false;
			}
			if (!Objects.equals(this.resource, other.resource)) {
				return false;
			}
			return true;
		}
	}
	private static class AudioKey {
		Resource.AudioProvider resource;
		float pixelsPerFrame;
		int frame;
		private AudioKey(Resource.AudioProvider resource, float pixelsPerFrame, int frame) {
			this.resource = resource;
			this.pixelsPerFrame = pixelsPerFrame;
			this.frame = frame;
		}

		@Override
		public int hashCode() {
			int hash = 3;
			hash = 29 * hash + Objects.hashCode(this.resource);
			hash = 29 * hash + Float.floatToIntBits(this.pixelsPerFrame);
			hash = 29 * hash + this.frame;
			return hash;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final AudioKey other = (AudioKey) obj;
			if (Float.floatToIntBits(this.pixelsPerFrame) != Float.floatToIntBits(other.pixelsPerFrame)) {
				return false;
			}
			if (this.frame != other.frame) {
				return false;
			}
			if (!Objects.equals(this.resource, other.resource)) {
				return false;
			}
			return true;
		}
		
	}
	private final LRUCache<Object, Object> cache;
	private final Object dummy = new Object();
	private final ImageFactory imageFactory;
	private final AudioFactory audioFactory;
	
	/**
	 * creates a new factory
	 * @param rowHeight the height of the single images, will fit in a timeline channel
	 */
	public TimelinePreviewFactory(int rowHeight) {
		this.rowHeight = rowHeight;
		waveformWidth = (int) TimelineDisplay.MAX_PIXELS_PER_FRAME; //on the finest resolution, an image covers exactly one frame
		
		changeSupport = new PropertyChangeSupport(this);
		
		threadPool = Executors.newSingleThreadExecutor();
		cache = new LRUCache<>(500);
		imageFactory = new ImageFactory();
		audioFactory = new AudioFactory();
	}

	public int getRowHeight() {
		return rowHeight;
	}

	public int getWaveformWidth() {
		return waveformWidth;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener l) {
		changeSupport.addPropertyChangeListener(l);
	}
	public void removePropertyChangeListener(PropertyChangeListener l) {
		changeSupport.removePropertyChangeListener(l);
	}
	
	/**
	 * Retrieves a frame preview.
	 * @param resource the resource
	 * @param frame the frame index from the beginning
	 * @return {@code null} if it is not available or not loaded yet. If loaded,
	 *  returns the scaled preview image
	 */
	public Image getFramePreview(Resource.ImageProvider resource, int frame) {
		ImageKey key = new ImageKey(resource, frame);
		synchronized(cache) {
			Object value = cache.getCreate(key, imageFactory);
			if (value == dummy) {
				//it will be loaded
				return null;
			} else {
				assert (value instanceof Image);
				return (Image) value;
			}
		}
	}
	
	public Image getWaveformPreview(Resource.AudioProvider resource, int frame, float pixelsPerFrame) {
		AudioKey key = new AudioKey(resource, pixelsPerFrame, frame);
		synchronized(cache) {
			Object value = cache.getCreate(key, audioFactory);
			if (value == dummy) {
				//it will be loaded
				return null;
			} else {
				assert (value instanceof Image);
				return (Image) value;
			}
		}
	}
	
	private class ImageFactory implements LRUCache.Factory<Object, Object> {

		@Override
		public Object create(Object key) {
			//launch task
			threadPool.execute(() -> loadImage((ImageKey) key));
			return dummy;
		}
		
		private void loadImage(ImageKey key) {
			Texture2D tex = key.resource.getFrame(key.frame, true);
			BufferedImage img = ImageToAwt.convert(tex.getImage(), false, true, 0);
			AffineTransform at = new AffineTransform();
			double scale = rowHeight / (double) img.getHeight(null) ;
			at.scale(scale, -scale);
			at.translate(0, -img.getHeight());
			AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
			img = scaleOp.filter(img, null);
			synchronized (cache) {
				cache.put(key, img);
				LOG.log(Level.FINE, "preview frame processed: {0} : {1}", new Object[]{key.resource.getName(), key.frame});
			}
			changeSupport.firePropertyChange(PROP_FRAME_LOADED, null, img);
		}
	}
	
	private class AudioFactory implements LRUCache.Factory<Object, Object> {

		@Override
		public Object create(Object key) {
			//launch task
			threadPool.execute(() -> createImage((AudioKey) key));
			return dummy;
		}
		
		private void createImage(AudioKey key) {
			//find layer
			AudioWaveformCreator.WaveformFile file = key.resource.getWaveform();
			AudioWaveformCreator.WaveformZoomLayer layer = null;
			for (AudioWaveformCreator.WaveformZoomLayer l : file.getLayers()) {
				if (l.getPixelsPerFrame() == key.pixelsPerFrame) {
					layer = l;
					break;
				}
			}
			if (layer == null) {
				LOG.log(Level.SEVERE, "no layer found that matches the pixel/frame ratio {0}", key.pixelsPerFrame);
				return;
			}
			//find pixel entry
			int offX = (int) (key.frame * layer.getPixelsPerFrame());
			int length = Math.min(layer.getValues().length - offX, waveformWidth);
			//create image
			BufferedImage image = new BufferedImage(Math.max(1,length), rowHeight, BufferedImage.TYPE_BYTE_GRAY);
			byte[] data = ((DataBufferByte)image.getRaster().getDataBuffer()).getData();
			for (int x=0; x<length; ++x) {
				int value = layer.getValues()[x + offX] & 0xff;
				value = (int) (value / 255.0 * rowHeight);
				for (int y=0; y<rowHeight; ++y) {
					data[x + y*length] = (byte) (((rowHeight-y-1)>value) ? 200 : 100);
				}
			}
			synchronized (cache) {
				cache.put(key, image);
				LOG.log(Level.FINE, "waveform processed: {0} : {1},{2}", new Object[]{key.resource.getName(), key.frame, key.pixelsPerFrame});
			}
			changeSupport.firePropertyChange(PROP_FRAME_LOADED, null, image);
		}
	}
}
