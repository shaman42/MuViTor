/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.resources;

import com.jme3.audio.AudioBuffer;
import com.jme3.audio.AudioKey;
import com.jme3.texture.Texture2D;
import java.awt.image.BufferedImage;
import java.io.File;
import org.shaman.muvitor.player.Player;

/**
 *
 * @author Sebastian Weiss
 */
public interface Resource {
	
	String getName();
	void setName(String name);
	
	void load(ResourceLoader loader);
	boolean isLoaded();
	void unload();
	
	int getDurationInFrames();
	
	public static interface ResourceLoader {
		File getProjectDirectory();
		void setMessage(String message);
		Player getPlayer();
	}
	
	public static interface AudioProvider extends Resource {
		/**´
		 * @return The audio data
		 */
		AudioBuffer getAudioData();
		
		AudioKey getAudioKey();
		
		AudioWaveformCreator.WaveformFile getWaveform();
		
		/**
		 * Because audio is not always needed (e.g. for videos with disabled audio),
		 * they are only loaded if requested
		 */
		void requestAudio();
		boolean isAudioLoaded();
	}
	
	public static interface ImageProvider extends Resource {
		/**
		 * Retrieves the frame with the specified index from the beginning
		 * of the resource
		 * @param index the frame index
		 * @param thumbnail {@code true} if only a preview thumbnail should be returned
		 * @return the frame
		 */
		Texture2D getFrame(int index, boolean thumbnail);
		/**
		 * Notifies the provider that the image with the specified index
		 * is retrieved next by {@link #getFrame(int, boolean) }.
		 * @param index the frame index
		 * @param thumbnail {@code true} if only a preview thumbnail should be returned
		 */
		void prefetchFrame(int index, boolean thumbnail);
		/**
		 * Returns the scale of the thumbnail
		 * @return 
		 */
		float getThumbnailScale();
		
		int getWidth();
		int getHeight();
	}
}
