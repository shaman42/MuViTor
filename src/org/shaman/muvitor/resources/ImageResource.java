/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.resources;

import com.jme3.texture.Texture2D;
import com.jme3.texture.plugins.AWTLoader;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.shaman.muvitor.player.VideoTools;
import org.simpleframework.xml.Element;

/**
 *
 * @author Sebastian Weiss
 */
public class ImageResource implements Resource, Resource.ImageProvider {
	private static final boolean FLIP_Y = true;
	
	@Element
	private String name;

	private Texture2D image;
	
	public ImageResource() {
	}

	public ImageResource(String name) {
		this.name = name;
	}
	

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void load(ResourceLoader loader) {
		if (isLoaded()) {
			return;
		}
		try {
			//load images
			loader.setMessage(name+":\nloading");
			String path = loader.getProjectDirectory().getAbsolutePath()+"\\"+name;
			BufferedImage awtImage = ImageIO.read(new File(path));
			awtImage = VideoTools.ensureFormat(awtImage, BufferedImage.TYPE_INT_ARGB);
			//convert to textures
			AWTLoader awtLoader = new AWTLoader();
			image = new Texture2D(awtLoader.load(awtImage, FLIP_Y));
			image.setAnisotropicFilter(16);
			loader.setMessage(name+":\nloaded");
		} catch (IOException ex) {
			Logger.getLogger(AudioResource.class.getName()).log(Level.SEVERE, null, ex);
			loader.setMessage(ex.getMessage());
		}
	}

	@Override
	public boolean isLoaded() {
		return image != null;
	}

	@Override
	public void unload() {
		image = null;
	}

	public Texture2D getImage() {
		return image;
	}
	
	@Override
	public String toString() {
		return new File(getName()).getName();
	}

	@Override
	public Texture2D getFrame(int index, boolean thumbnail) {
		return this.image;
	}

	@Override
	public float getThumbnailScale() {
		return 1;
	}

	@Override
	public int getWidth() {
		return this.image.getImage().getWidth();
	}

	@Override
	public int getHeight() {
		return this.image.getImage().getHeight();
	}

	@Override
	public int getDurationInFrames() {
		return -1; //marker for an arbitrary length
	}

	@Override
	public void prefetchFrame(int index, boolean thumbnail) {
		//No-Op, image is always loaded
	}

}
