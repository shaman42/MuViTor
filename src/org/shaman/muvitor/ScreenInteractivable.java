/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor;

import java.awt.Graphics2D;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;

/**
 * Focussed objects that implement this interface receive click events
 * from the main screen and are allowed to paint on the screen
 * @author Sebastian Weiss
 */
public interface ScreenInteractivable 
		extends MouseWheelListener, MouseListener, MouseMotionListener {
	
	void drawOnScreen(Graphics2D g);
}
