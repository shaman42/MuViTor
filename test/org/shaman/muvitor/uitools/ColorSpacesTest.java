/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.shaman.muvitor.uitools;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sebastian Weiss
 */
public class ColorSpacesTest {
	
	private static final float STEP = 1f/32f;
	private static final float DELTA = 1f/1024f;
	
	public ColorSpacesTest() {
	}

	@Test
	public void testInvertible() {
		for (float r=0; r<=1; r+=STEP) {
			for (float g=0; g<=1; g+=STEP) {
				for (float b=0; b<=1; b+=STEP) {
					float y = ColorSpaces.rgb2y(r, g, b);
					float cb = ColorSpaces.rgb2cb(r, g, b);
					float cr = ColorSpaces.rgb2cr(r, g, b);
					float r2 = ColorSpaces.ycc2r(y, cb, cr);
					float g2 = ColorSpaces.ycc2g(y, cb, cr);
					float b2 = ColorSpaces.ycc2b(y, cb, cr);
					assertEquals(r, r2, DELTA);
					assertEquals(g, g2, DELTA);
					assertEquals(b, b2, DELTA);
					
				}
			}
		}
	}
	
	@Test
	public void testYCbCrRange() {
		for (float r=0; r<=1; r+=STEP) {
			for (float g=0; g<=1; g+=STEP) {
				for (float b=0; b<=1; b+=STEP) {
					float y = ColorSpaces.rgb2y(r, g, b);
					float cb = ColorSpaces.rgb2cb(r, g, b);
					float cr = ColorSpaces.rgb2cr(r, g, b);
					//System.out.println("r="+r+", g="+g+", b="+b+" -> y="+y+", cb="+cb+", cr="+cr);
					assertTrue(y >= 0-DELTA);
					assertTrue(y <= 1+DELTA);
					assertTrue(cb >= -0.5-DELTA);
					assertTrue(cb <= 0.5+DELTA);
					assertTrue(cr >= -0.5-DELTA);
					assertTrue(cr <= 0.5+DELTA);
				}
			}
		}
	}
}
